import React from 'react'

import BaseMVPView from '../common/base/BaseMVPView'
import ConnectView from '../../utils/ConnectView'
import Presenter from './presenter/AnsweredQuestionnairePresenter'

import Card from '../../components/Card/Card'
import Line from '../../components/Line/Line'
import GenericLoader from '../../components/Loader/GenericLoader'
import GenericButton from '../../components/Button/GenericButton'
import Modal from '../../components/Modal/Modal'

import AnsweredQuestionnaireViewComponent from './components/AnsweredQuestionnaireViewComponent'

class AnsweredQuestionnaireFragment extends BaseMVPView {

  constructor (props) {
    super (props)

    this.state = {
      questionnaireFormModal: true,
      showLoader: false,
      answeredQuestion: [],
      questionsHeaderSettings: {},
      questions: []
    }

    this.showLoaderModal = this.showLoaderModal.bind(this)
  }

  showLoaderModal(showLoader) {
    this.setState({ showLoader })
  }

  componentDidMount() {
    this.props.setSelectedNavigation(5)
    this.presenter.getAnsweredQuestionnaire()
  }

  setAnsweredQuestionnaire(resp) {
    this.setState({ answeredQuestion: resp })
  }

  showQuestionnaireForm(recipientId, action) {
    recipientId != 0 && action ?
      this.presenter.getAnsweredQuestionnaireDetails(recipientId, action)
    :
      this.setState({ questionnaireFormModal: action })
  }

  setAnsweredQuestionnaireDetails(resp) {
    let questionTemp = []

    resp.questions.map((val, key) =>
      questionTemp.push(val)
    )

    this.setState({ questionnaireFormModal: false })
    this.setState({ questions: questionTemp })
    this.setState({ questionsHeaderSettings: resp })
  }

  render () {
    const VIEW_ANSWERED = 'VIEW-ANSWERED'
    const {
      questionnaireFormModal,
      showLoader,
      answeredQuestion,
      questionsHeaderSettings,
      questions
    } = this.state

    return (
      <div>
        {
          showLoader ?
          <Modal width = { 20 } >
            <GenericLoader
              message = { 'Loading Questionnaire' }
              show = { showLoader } />
          </Modal>
        :
        <div>
          <div className = { 'margin-tb-4px' } >
            <label className = { 'font-size-20px' } >
              Answered Question History
            </label>
          </div>
          <Line/>
          {
            !questionnaireFormModal &&
              <div className = { 'margin-top-12px rtl' } >
                <GenericButton
                  className = { 'tabs-button active' }
                  type = { 'button' }
                  onClick = { () => this.showQuestionnaireForm(0, true) }
                  text = { 'Close' } />
              </div>
          }
          <br/>
          {
            questionnaireFormModal ?
            answeredQuestion &&
            answeredQuestion.map((resp, key) =>
              <Card
                key = { key }
                className = { 'margin-bottom-8px' } >
                <div className = { 'grid-global-4fr-3fr-2fr' }
                  onClick = { () => this.showQuestionnaireForm(resp.recipientId, VIEW_ANSWERED) }>
                  <div>
                    <label className = { 'font-size-18px' }>
                      <b>
                       { resp.topMessage }
                      </b>
                    </label>
                    <br/>
                    <label className = { 'font-size-12px' } >
                      Monthly Recurring: { resp.typeDesc }
                    </label>
                  </div>

                  <div>
                    <label className = { 'font-size-14px' } >
                      End Date
                    </label>
                    <br/>
                    <label className = { 'font-size-12px' } >
                      { resp.endDate }
                    </label>
                  </div>

                  <div>
                  </div>

                </div>
              </Card>
            )
            :
            <div>
              <AnsweredQuestionnaireViewComponent
                questions = { questions }
              />
            </div>
          }
          </div>
        }
      </div>
    )
  }
}

export default ConnectView(AnsweredQuestionnaireFragment, Presenter)
