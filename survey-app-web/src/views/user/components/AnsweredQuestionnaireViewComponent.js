import React, { Component } from 'react'

import GenericCard from '../../../components/Card/GenericCard'
import GenericInput from '../../../components/TextBox/GenericInput'
import GenericButton from '../../../components/Button/GenericButton'
import { RadioGroup, RadioButton, ReversedRadioButton } from 'react-radio-buttons'

class AnsweredQuestionnaireViewComponent extends Component {

  constructor (props) {
    super (props)
  }

  render () {

    const {
      questions
    } = this.props

    return (
      <div>

        <GenericCard>
          <div className = { 'grid-global-questionnaire' } >
            <div>
            </div>

              <div className = { 'card-body' } >

                <div>
                  <div className = { 'text-align-center'} >
                    <span className = { 'font-size-18px' } >
                      Questionnaire for engineers
                    </span>
                    <br/>
                    <span className = { 'font-size-14px' } >
                      This questionnaire is intended for newly hired engineers.
                    </span>
                  </div>
                  <br/>
                  <br/>

                  <div className = { 'grid-global font-size-12px' } >
                    <div>
                      <label>
                        Name: Yumeshin Employee 001
                      </label>
                      <br/>
                      <label>
                        Employee No: Y-R-0001
                      </label>
                    </div>
                    <div className = { 'rtl' } >
                      <label>
                        Questionnaire Type: Regular
                      </label>
                      <br/>
                      <span>
                      </span>
                    </div>
                  </div>

                  <br/>
                    <div>
                      {
                        questions.map((question, keyQuestion) =>
                        <div
                          key = { keyQuestion } >
                          <label
                            className = { 'margin-bottom-8px' } >
                            { question.sequenceNo }: { question.question }
                            <br/>
                            <br/>
                          </label>
                            {
                              <div>
                                {
                                question.questionTypeId === 1 ?
                                question.hasImage === 1 ?
                                  <RadioGroup
                                    value = { question.questionnaireRecipientAnswers.answerChoiceId }
                                    horizontal >
                                    {
                                      question.answers.map((answer, ansKey) =>
                                        <ReversedRadioButton
                                          key = { ansKey }
                                          pointColor = { '#ffb901' }
                                          disabled = { answer.answerId === question.questionnaireRecipientAnswers.answerChoiceId ? false : true }
                                          value = { String(answer.answerId) } >
                                          {
                                            <label className = { 'margin-tb-8px' } >
                                              { answer.answer }
                                              <br/>
                                              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAAA+UlEQVRIidXUvUoDQRTF8d+KlY1aieBHG+zyAIK1hYVPYRP1OaxstVQDVlZp/KhUsLEUks5O7MUqwlrsCGoyyy5hCHthqjNz/nPmXoamV4aT1IA8JWAmpXmjAH0MYmI+4TpX9DLDxX89lmBY4/Z3vwxvqyS4wia+Kia4xwIW8TBGHzFfxRaOKgLykHgY0f6Yr+AFb1jGE25wjM8awBFAD+uKafgRzrCEbeyEZ/uoC8jCzfdxjY1xTQqbD/EcQDCPA8xFzqAYrbVg3irbGCCXIYUAe0QX7TJAv4J5rHrYU6TbjQEm/eze0cFrKkBpNeazmx5gFqepIc2uby59gVLzFnnDAAAAAElFTkSuQmCC" />
                                            </label>
                                          }
                                        </ReversedRadioButton>
                                      )
                                    }
                                  </RadioGroup>
                                  :
                                  <RadioGroup
                                    value = { question.questionnaireRecipientAnswers.answerChoiceId }
                                    vertical = { 'true' } >
                                    {
                                      question.answers.map((answer, ansKey) =>
                                        <ReversedRadioButton
                                          key = { ansKey }
                                          pointColor = { '#ffb901' }
                                          disabled = { answer.answerId === question.questionnaireRecipientAnswers.answerChoiceId ? false : true }
                                          value = { String(answer.answerId) } >
                                            { answer.answer }
                                        </ReversedRadioButton>
                                      )
                                    }
                                  </RadioGroup>
                                : question.questionTypeId === 2  &&
                                  <GenericInput
                                    type = { 'textarea' }
                                    disabled = { true }
                                    value = { question.questionnaireRecipientAnswers.answerValue }
                                    />
                                }
                              </div>
                            }
                        <br/>
                        </div>
                        )
                      }
                    </div>
                </div>
              </div>

          </div>
        </GenericCard>
      </div>
    )
  }
}

export default AnsweredQuestionnaireViewComponent
