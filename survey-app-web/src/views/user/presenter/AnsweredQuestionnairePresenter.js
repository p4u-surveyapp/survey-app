import GetAnsweredQuestionnaireInteractor from '../../../domain/interactor/user/GetAnsweredQuestionnaireInteractor'
import GetQuestionnaireFormInteractor from '../../../domain/interactor/user/GetQuestionnaireFormInteractor'

import getQuestionnaireParam from '../../../domain/param/user/GetQuestionnaireParam'

export default class AnsweredQuestionnairePresenter {

  constructor (container) {
    this.getAnsweredQuestionnaireInteractor = new GetAnsweredQuestionnaireInteractor(container.get('SurveyAppClient'))
    this.getQuestionnaireFormInteractor = new GetQuestionnaireFormInteractor(container.get('SurveyAppClient'))
  }

  setView(view) {
    this.view = view
  }

  getAnsweredQuestionnaire() {
    this.getAnsweredQuestionnaireInteractor
        .execute()
        .subscribe(data => {
          this.view.setAnsweredQuestionnaire(data.data)
          this.view.showLoaderModal(false)
        })
  }

  getAnsweredQuestionnaireDetails(recipientId, action) {
    this.view.showLoaderModal(true)
    this.getQuestionnaireFormInteractor
        .execute(getQuestionnaireParam(recipientId, action))
        .subscribe(data => {
          this.view.showLoaderModal(false)
          this.view.setAnsweredQuestionnaireDetails(data.data)
        }
      )
  }

}
