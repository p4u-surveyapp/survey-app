import React, { Component } from 'react'
import Card from '../../../components/Card/Card'
import GenericInput from '../../../components/TextBox/GenericInput'
import GenericButton from '../../../components/Button/GenericButton'
import GenericLoader from '../../../components/Loader/GenericLoader'

import './styles/styles.css'
class LoginComponent extends Component {
  render () {
    const {
      // username,
      // password,
      submit,
      disabled,
      setUsername,
      setPassword
    } = this.props

    return (
      <Card className = { 'login-form-div' }>
        <div>
          <h2 className={ 'header-margin-default' }>
            SURVEY APP
          </h2>
        </div>
        <br/>
        <GenericInput
          type = { 'text' }
          hint = { 'Username' }
          text = { 'Username' }
          onChange = { (e) => setUsername(e.target.value) }
        />
        <GenericInput
          type = { 'password' }
          hint = { 'Password' }
          text = { 'Password' }
          onChange = { (e) => setPassword(e.target.value) }
        />
        <br/>
        {
          disabled ?
          <GenericLoader show = { disabled } />
          :
          <GenericButton
            type = { 'button' }
            onClick={ () => submit() }
            text = { 'Log In' }
            disabled = { disabled }
          />
        }
      </Card>
    )
  }
}
export default LoginComponent
