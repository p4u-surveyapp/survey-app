import LoginInteractor from '../../../domain/interactor/login/LoginInteractor'
import LoginParam from '../../../domain/param/login/LoginParam'

export default class LoginPresenter {
  constructor (container) {
    this.loginInteractor = new LoginInteractor(container.get('SurveyAppClient'))
  }

  setView (view) {
    this.view = view
  }

  login (username, password) {
   this.view.disabledButton(true)
   this.loginInteractor.execute(LoginParam(username, password))
   .subscribe(
     data => {
       this.view.disabledButton(false)
       data &&
        this.view.setLoginBool(data.message)
     },
     error => {
       this.view.disabledButton(false)
     }
   )
 }

}
