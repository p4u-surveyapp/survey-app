import React from 'react';
import LoginComponent from './components/LoginComponent'
import BaseMVPView from '../common/base/BaseMVPView'
import ConnectView from '../../utils/ConnectView'

import Presenter from './presenter/LoginPresenter'

import './styles/loginStyles.css'
class LoginFragment extends BaseMVPView{
  constructor (props) {
    super(props)
    this.state = {
      username : '',
      password: '',
      disabled: false
    }
    this.disabledButton = this.disabledButton.bind(this)
  }

  setUsername(username) {
    this.setState({ username })
  }

  setPassword(password) {
    this.setState({ password })
  }

  disabledButton(bool) {
    this.setState({ disabled: bool })
  }

  submitCredentials() {
    this.presenter.login(this.state.username, this.state.password)
  }

  setLoginBool(bool) {
    this.props.checkIsLogin(bool)
  }

  render () {
    const {
      username,
      password,
      disabled
    } = this.state

    const { checkIsLogin } = this.props

    return (
      <div className = { 'login-container' }>
        <div className = { 'login-grid-column-3' }>
          <div></div>
          <div>
            <LoginComponent
              submit = { () => { this.submitCredentials() } }
              setUsername = { (resp) => this.setUsername(resp) }
              setPassword = { (resp) => this.setPassword(resp) }
              disabled = { disabled }/>
          </div>
        </div>
      </div>
    )
  }
}
export default ConnectView(LoginFragment, Presenter)
