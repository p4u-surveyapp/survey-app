import React from 'react'
import PropTypes from 'prop-types'

import BaseMVPView from '../common/base/BaseMVPView'
import ConnectView from '../../utils/ConnectView'

import Card from '../../components/Card/Card'
import GenericButton from '../../components/Button/GenericButton'
import GenericInput from '../../components/TextBox/GenericInput'
import Line from '../../components/Line/Line'

import Presenter from './presenter/HomePresenter'
import './styles/homeStyle.css'
import EmployeeUserFragment from '../employee/EmployeeUserFragment'

class HomeFragment extends BaseMVPView {
  constructor (props) {
    super(props)
    this.state = {
      greetingsMessage : '',
      employeeName  : '',
      homePreview  : false,
      isRegular: false,
      showForm: false,
      answerArr: []
    }
  }

  componentDidMount () {
    this.props.setSelectedNavigation(1)
  }

  setActiveStatus(isRegular) {
    this.setState({ isRegular })
  }

  showHideForm(value) {
    this.setState({ showForm: value ? false : true })
  }

  render () {
    const {
      homePreview,
      isRegular,
      showForm,
      answerArr
    } = this.state

    const {
      storeWidth,
      history,
      setSelectedNavigation,
      groupId
    } = this.props

    const form = {
      show: {
        display : showForm ? 'block' : 'none'
      }
    }

    const tabs = [
      {
        id: 1,
        title: 'Pending Questionnaire',
        isRegular: false
      },
      {
        id: 2,
        title: 'Tab 2',
        isRegular: true
      }
    ]

    const questionnaire = [
      {
        title: 'Questionnaire for engineers',
        endDate: 'November 7, 2018',
        isMonthlyRecurring: 'Yes'
      },
      {
        title: 'Questionnaire for newly hired employees',
        endDate: 'November 14, 2018',
        isMonthlyRecurring: 'Yes'
      },
      {
        title: 'Monthly Trainee Questionnaire',
        endDate: 'November 21, 2018',
        isMonthlyRecurring: 'Yes'
      },
    ]

    return (
      <div>
        {
          groupId === 2 ?
          <div>
            <div className = { 'grid-global margin-tb-4px' } >
              <label className = { 'font-size-20px' } >
              Dashboard
              </label>
            </div>
            <Line/>
            <br/>
          </div>
          :
            <EmployeeUserFragment/>
        }
      </div>
    )
  }
}

HomeFragment.propTypes = {
  setSelectedNavigation: PropTypes.func,
}

export default ConnectView(HomeFragment, Presenter)
