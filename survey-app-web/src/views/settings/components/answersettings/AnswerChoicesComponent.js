import React, { Component } from 'react'

import Card from '../../../../components/Card/Card'
import Modal from '../../../../components/Modal/Modal'
import Table from './../../../../components/Table/Table'
import Line from '../../../../components/Line/Line'
import GenericButton from '../../../../components/Button/GenericButton'
import GenericInput from '../../../../components/TextBox/GenericInput'

import './styles/answer.css'

class AnswerChoicesComponent extends Component {

  constructor (props) {
    super (props)
    this.checkRequiredFields = this.checkRequiredFields.bind(this)
  }

  checkRequiredFields(
    answer,
    imgUrl,
    rating
  ) {
    if(answer &&
      imgUrl &&
      rating) {
      return false
    } else {
      return true
    }
  }

  render () {
    const {
      isAdd,
      answerGroupId,
      answerChoices,
      showAdd,
      editButton,
      answer,
      answerErrorMessage,
      rating,
      ratingErrorMessage,
      imgUrl,
      imgUrlErrorMessage,
      setAnswer,
      setRating,
      setImgURL,
      submitAnswerChoice
    } = this.props

    return (
      <div className = { 'optional-container' } >
        <div className = { 'grid-global' } >
          <div className = { 'table-name' } >
            <label>
              Answer Choices
            </label>
          </div>
          <div></div>
        </div>
        <Line className = { 'margin-tb-8px' } />
        <span className = { 'font-size-11px label-color' }>* Attaching image requires a Rating.</span>

        <div className = { 'grid-global-columns-x4' }>
          <GenericInput
            defaultValue = { '' }
            text = { 'Answer' }
            value = { answer }
            onChange = { (e) => setAnswer(e.target.value) }
            errorMessage = { answerErrorMessage }
          />
          <GenericInput
          defaultValue = { '' }
          text = { 'Image' }
          value = { imgUrl }
          onChange = { (e) => setImgURL(e.target.value) }
          errorMessage = { imgUrlErrorMessage }
          />
          <GenericInput
            defaultValue = { '' }
            text = { 'Rating' }
            type = { 'number' }
            value = { rating }
            maxLength = { 1 }
            onChange = { (e) => setRating(e.target.value) }
            disabled = { imgUrl ? false : true }
            errorMessage = { ratingErrorMessage }
          />
          <div className = { 'padding-top-18' } >
            <GenericButton
              className = { 'font-size-12px tabs-button active' }
              type = { 'button' }
              onClick = { () => submitAnswerChoice() }
              text = { 'Add Answer Choice' }
              disabled = { this.checkRequiredFields(answer, imgUrl, rating) }
            />
          </div>
        </div>
        {
          answerChoices &&
          answerChoices.length != 0 ?
          answerChoices.map((data, id) =>
            <Card className = { `grid-card margin-bottom-8px ${ answerGroupId == data.answerGroupId && 'card-active' }` } key = { id }>
              <div className = { 'cursor-pointer' } onClick = { () => console.log(data) }>
                <label className = { 'font-size-16px' }>Answer: { data.answer }</label>
                <br/>
                <label className = { 'font-size-11px label-color' }>Rating: { data.rating }</label>
                <br/>
                <label className = { 'font-size-11px label-color' }>Image URL: { data.imgUrl }</label>
              </div>
              <div className = { 'text-align-end' }>
                <span className = { 'action-button action-edit' }
                  onClick = { () => {
                    editButton(data)
                  }
                }/>
              </div>
            </Card>
          )
          :
          <div className = { 'margin-bottom-8px text-align-center' }>
            <label>No record(s)</label>
          </div>
        }
      </div>
    )
  }

}

export default AnswerChoicesComponent
