import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Card from '../../../../components/Card/Card'
import Modal from '../../../../components/Modal/Modal'
import Table from './../../../../components/Table/Table'
import Line from '../../../../components/Line/Line'
import GenericButton from '../../../../components/Button/GenericButton'
import GenericInput from '../../../../components/TextBox/GenericInput'
import GenericDropdown from '../../../../components/Dropdown/GenericDropdown'
import AnswerChoicesComponent from './AnswerChoicesComponent'

import './styles/answer.css'

const propTypes = {
    methodsArray: PropTypes.array,
}

const defaultProps = {
  methodsArray:[{answerMethodId: 1, method: "N/A", createdAt: null}]
}
class AnswerSettingsComponent extends Component {

  constructor (props) {
    super (props)
    this.checkRequiredFields = this.checkRequiredFields.bind(this)
  }

  checkRequiredFields(
    answerGroupCode,
    answerGroupDesc,
    answerGroupMethod
  ) {
    if(answerGroupCode &&
      answerGroupDesc &&
      answerGroupMethod) {
      return false
    } else {
      return true
    }
  }

  render () {
    const {
      settingsArray,
      methodsArray,
      answerGroupId,
      answerGroupCode,
      answerGroupCodeErrorMessage,
      answerGroupDesc,
      answerGroupDescErrorMessage,
      answerGroupMethod,
      answerGroupMethodErrorMessage,
      selectedAnswerGroup,
      selectedAnswerMethod,
      setAnswerGroupCode,
      setAnswerGroupDesc,
      submitAnswerGroup
    } = this.props

    const answerMethodList = methodsArray.map((resp, key) => {
      return {
        id: resp.answerMethodId,
        name: resp.method
      }
    })

    return (
      <div className = { 'optional-container' } >
        <div className = { 'grid-global' } >
          <div className = { 'table-name' } >
            <label>
              Answer Settings
            </label>
          </div>
          <div className = { 'grid-global' } >
            <div></div>
          </div>
        </div>
        <Line  className = { 'margin-tb-8px' } />
        <br/>
        <div className = { 'grid-form-x4' }>
          <GenericInput
            defaultValue = { '' }
            text = { 'Code' }
            value = { answerGroupCode }
            onChange = { (e) => setAnswerGroupCode(e.target.value) }
            errorMessage = { answerGroupCodeErrorMessage }
          />
          <GenericInput
            defaultValue = { '' }
            text = { 'Description' }
            value = { answerGroupDesc }
            onChange = { (e) => setAnswerGroupDesc(e.target.value) }
            errorMessage = { answerGroupDescErrorMessage }
          />
          <GenericDropdown
            text = { 'Method' }
            hint = { 'Select Answer Group Method' }
            value = { answerGroupMethod }
            options = { answerMethodList }
            onChange = { (e) => {
                var index = e.target.selectedIndex
                var optionElement = e.target.childNodes[index]
                var id =  optionElement.getAttribute('id')
                selectedAnswerMethod(id, e.target.value)
              }
            }
            errorMessage = { answerGroupMethodErrorMessage }
          />
          <div className = { 'padding-top-18' } >
            <GenericButton
            className = { 'font-size-12px tabs-button active' }
              type = { 'button' }
              onClick = { () => submitAnswerGroup() }
              text = { 'Add Answer Group' }
              disabled = { this.checkRequiredFields(answerGroupCode, answerGroupDesc, answerGroupMethod) }
            />
          </div>
        </div>
        {
          settingsArray &&
          settingsArray.answerGroups ?
          settingsArray.answerGroups.map((data, id) =>
            <Card className = { `grid-card margin-bottom-8px ${ answerGroupId == data.answerGroupId && 'card-active' }` } key = { id }>
              <div className = { 'cursor-pointer' } onClick = { () =>
                {
                  selectedAnswerGroup(data.answerGroupId, data.answers)
                }
              }>
                <label className = { 'font-size-16px' }>Answer Group Code: {data.groupCode}</label>
                <br/>
                <label className = { 'font-size-11px label-color' }>Answer Group Description: {data.groupDesc}</label>
                <br/>
                <label className = { 'font-size-11px label-color' }>Answer method: {data.method}</label>
              </div>
            </Card>
          )
          :
          <div className = { 'margin-bottom-8px text-align-center' }>
            <label>No record(s)</label>
          </div>
        }
      </div>
    )
  }

}

AnswerSettingsComponent.propTypes = propTypes
AnswerSettingsComponent.defaultProps = defaultProps
export default AnswerSettingsComponent
