import GetAnswerGroupsInteractor from '../../../domain/interactor/answergroups/GetAnswerGroupsInteractor'
import GetAnswerMethodsInteractor from '../../../domain/interactor/answergroups/GetAnswerMethodsInteractor'
import PostAnswerGroupInteractor from '../../../domain/interactor/answergroups/PostAnswerGroupInteractor'
import PostAnswerChoiceInteractor from '../../../domain/interactor/answerchoice/PostAnswerChoiceInteractor'
import PutAnswerChoiceInteractor from '../../../domain/interactor/answerchoice/PutAnswerChoiceInteractor'
import getAnswerGroupsParam from '../../../domain/param/answergroups/GetAnswerGroupsParam'
import postAnswerGroupParam from '../../../domain/param/answergroups/PostAnswerGroupParam'
import postAnswerChoiceParam from '../../../domain/param/answerchoice/PostAnswerChoiceParam'
import putAnswerChoiceParam from '../../../domain/param/answerchoice/PutAnswerChoiceParam'

export default class SettingsPresenter {
  constructor (container) {
    this.getAnswerGroupsInteractor = new GetAnswerGroupsInteractor(container.get('SurveyAppClient'))
    this.getAnswerMethodsInteractor = new GetAnswerMethodsInteractor(container.get('SurveyAppClient'))
    this.postAnswerGroupInteractor = new PostAnswerGroupInteractor(container.get('SurveyAppClient'))
    this.postAnswerChoiceInteractor = new PostAnswerChoiceInteractor(container.get('SurveyAppClient'))
    this.putAnswerChoiceInteractor = new PutAnswerChoiceInteractor(container.get('SurveyAppClient'))
  }

  setView (view) {
    this.view = view
  }

  getAnswerGroups(pageNo, pageSize) {
    this.view.showLoaderModal(true)
    this.getAnswerGroupsInteractor.execute(getAnswerGroupsParam(pageNo, pageSize))
    .do( data => {
      data.data ?
          this.view.setAnswerGroups(data.data)
        :
         data.message &&
         this.view.showErrorModal(true, data.message)
         this.view.showLoaderModal(false)
    },
    error => {
      console.error(error)
    })
    .subscribe( data => {
      data.data ?
          this.view.setAnswerGroups(data.data)
        :
         data.message &&
         this.view.showErrorModal(true, data.message)
         this.view.showLoaderModal(false)
    },
    error => {
      console.error(error)
    })
  }

  getAnswerMethods() {
    this.view.showLoaderModal(true)
    this.getAnswerMethodsInteractor.execute()
    .subscribe( data => {
      data.data ?
          this.view.setAnswerMethods(data.data)
        :
         data.message &&
         this.view.showErrorModal(true, data.message)
         this.view.showLoaderModal(false)
    },
    error => {
      console.error(error)
    })
  }

  submitAnswerGroup(
    answerGroupCode,
    answerGroupDesc,
    answerGroupMethodId
  ) {
    this.view.showLoaderModal(true)
    this.postAnswerGroupInteractor.execute(postAnswerGroupParam(
      answerGroupCode,
      answerGroupDesc,
      answerGroupMethodId
    )).subscribe( data => {
      data.message &&
        this.view.showErrorModal(true, data.message)
        this.getAnswerGroups(1, 10)
        this.view.showLoaderModal(false)
        this.view.resetFields()
    })
  }

  submitAnswerChoice(
    answer,
    rating,
    imgUrl,
    answerGroupId
  ) {
    this.view.showLoaderModal(true)
    this.postAnswerChoiceInteractor.execute(postAnswerChoiceParam(
      answer,
      rating,
      imgUrl,
      answerGroupId
    ))
    .subscribe( data => {
      data.message &&
        this.view.showErrorModal(true, data.message)
        this.getAnswerGroups(1,10)
        this.view.showLoaderModal(false)
        this.view.resetFields()
    })
  }

  editAnswerChoice(
    answer,
    rating,
    imgUrl,
    answerId,
    answerGroupId
  ) {
    this.view.showLoaderModal(true)
    this.putAnswerChoiceInteractor.execute(putAnswerChoiceParam(
      answer,
      rating,
      imgUrl,
      answerId,
      answerGroupId
    ))
    .subscribe( data => {
      data.message &&
        // this.getAnswerGroups(1,10)
        this.view.showErrorModal(true, data.message)
        this.view.resetFields()
        this.view.showLoaderModal(false)
    })
  }

}
