import React from 'react';
import BaseMVPView from '../common/base/BaseMVPView'
import ConnectView from '../../utils/ConnectView'
import GenericCard from '../../components/Card/GenericCard'
import Modal from '../../components/Modal/Modal'
import GenericLoader from '../../components/Loader/GenericLoader'
import GenericButton from '../../components/Button/GenericButton'
import GenericInput from '../../components/TextBox/GenericInput'
import Line from '../../components/Line/Line'

import AnswerSettingsComponent from './components/answersettings/AnswerSettingsComponent'
import AnswerChoicesComponent from './components/answersettings/AnswerChoicesComponent'
import * as Validation from './controller/SettingsController'
import Presenter from './presenter/SettingsPresenter'
import './styles/style.css'
class SettingsFragment extends BaseMVPView {
  constructor (props) {
    super(props)
    this.state = {
      pageNo: 1,
      pageSize: 10,
      isActive: 1,
      answerGroupId: 0,
      answerGroupMethodId: 0,
      answerId: 0,
      settingsArray: [],
      methodsArray: [],
      answerChoices: [],
      answerGroupCode: '',
      answerGroupCodeErrorMessage: '',
      answerGroupDesc: '',
      answerGroupDescErrorMessage: '',
      answerGroupMethod: '',
      answerGroupMethodErrorMessage: '',
      errorMessage: '',
      answer: '',
      answerErrorMessage: '',
      rating: '',
      ratingErrorMessage: '',
      imgUrl: '',
      imgUrlErrorMessage: '',
      isAdd: false,
      isEdit: false,
      showLoader: false,
      showError: false,
      showMethodModal: false
    }
    this.setActiveStatus = this.setActiveStatus.bind(this)
    this.showAdd = this.showAdd.bind(this)
    this.editButton = this.editButton.bind(this)
    this.showMethodModal = this.showMethodModal.bind(this)
    this.selectedAnswerGroup = this.selectedAnswerGroup.bind(this)
    this.selectedAnswerMethod = this.selectedAnswerMethod.bind(this)
    this.showErrorModal = this.showErrorModal.bind(this)
  }

  componentDidMount() {
    this.props.setSelectedNavigation(2)
    this.presenter.getAnswerGroups(this.state.pageNo, this.state.pageSize)
    this.presenter.getAnswerMethods()
  }

  setAnswerGroups(settingsArray) {
      this.setState({ settingsArray })
  }

  setAnswerMethods(methodsArray) {
    this.setState({ methodsArray })
  }

  setActiveStatus(isActive) {
    this.setState({ isActive })
  }

  showAdd(bool) {
    this.setState({ isAdd: bool })
  }

  editButton(value) {
    this.setState({
      answer: value.answer,
      rating: value.rating,
      imgUrl: value.imgUrl,
      answerGroupId: value.answerGroupId,
      answerId: value.answerId,
      isEdit: true
    })
  }

  showMethodModal(bool) {
    this.setState({ methodModal: bool })
  }

  selectedAnswerGroup(answerGroupId, array) {
    this.setState({ answerChoices: array, answerGroupId: answerGroupId })
  }

  selectedAnswerMethod(answerGroupMethodId, answerGroupMethod) {
    Validation.requiredFieldValidator(answerGroupMethodId) ?
    this.setState({ answerGroupMethodId, answerGroupMethod, answerGroupCodeErrorMessage: '' })
    :
    this.setState({ answerGroupMethodId: '', answerGroupMethod: '', answerGroupCodeErrorMessage: '*Required field' })
  }

  setAnswerGroupCode(answerGroupCode) {
    Validation.requiredFieldValidator(answerGroupCode) ?
    this.setState({ answerGroupCode, answerGroupCodeErrorMessage: '' })
    :
    this.setState({ answerGroupCode: '', answerGroupCodeErrorMessage: '*Required field' })
  }

  setAnswerGroupDesc(answerGroupDesc) {
    Validation.requiredFieldValidator(answerGroupDesc) ?
    this.setState({ answerGroupDesc, answerGroupDescErrorMessage: '' })
    :
    this.setState({ answerGroupDesc: '', answerGroupDescErrorMessage: '*Required field' })
  }

  submitAnswerGroup() {
    const {
      answerGroupCode,
      answerGroupDesc,
      answerGroupMethodId
    } = this.state

    this.presenter.submitAnswerGroup(
      answerGroupCode,
      answerGroupDesc,
      answerGroupMethodId
    )
  }

  setAnswer(answer) {
    Validation.requiredFieldValidator(answer) ?
    this.setState({ answer, answerErrorMessage: '' })
    :
    this.setState({ answer: '', answerErrorMessage: '*Required field' })
  }

  setRating(rating) {
    Validation.requiredFieldValidator(rating) ?
    this.setState({ rating, ratingErrorMessage: '' })
    :
    this.setState({ rating: '', ratingErrorMessage: '*Required field' })
  }

  setImgURL(imgUrl) {
    Validation.requiredFieldValidator(imgUrl) ?
    this.setState({ imgUrl, imgUrlErrorMessage: '' })
    :
    this.setState({ imgUrl: '', imgUrlErrorMessage: '*Required field' })
  }

  submitAnswerChoice() {
    const {
      answer,
      rating,
      imgUrl,
      answerGroupId,
      answerId,
      isEdit
    } = this.state

    if (isEdit) {
      this.presenter.editAnswerChoice(
        answer,
        rating,
        imgUrl,
        answerId,
        answerGroupId
      )
    } else {
      this.presenter.submitAnswerChoice(
        answer,
        rating,
        imgUrl,
        answerGroupId
      )
    }
  }

  resetFields() {
    this.setState({
      answerGroupCode: '',
      answerGroupDesc: '',
      answerGroupMethod: '',
      answer: '',
      rating: '',
      imgUrl: '',
      answerGroupId: 0,
      isEdit: false
    })
  }

  showLoaderModal(showLoader) {
    this.setState({ showLoader })
  }

  showErrorModal(bool, message) {
    this.setState({
      errorMessage: message,
      showError: bool
    })
  }

  render () {
    const {
      pageNo,
      pageSize,
      isActive,
      settingsArray,
      methodsArray,
      methodModal,
      answerId,
      answerGroupId,
      answerGroupCode,
      answerGroupCodeErrorMessage,
      answerGroupDesc,
      answerGroupDescErrorMessage,
      answerGroupMethodId,
      answerGroupMethod,
      answerGroupMethodErrorMessage,
      isAdd,
      answerChoices,
      showError,
      showLoader,
      errorMessage,
      answer,
      answerErrorMessage,
      rating,
      ratingErrorMessage,
      imgUrl,
      imgUrlErrorMessage
    } = this.state

    const tabs = [
      {
        id: 1,
        title: 'Answer Settings',
      },
      // {
      //   id: 2,
      //   title: 'Email Template',
      // }
    ]

    return (
      <div>
        {
          showError &&
          <Modal>
            <div className = { 'text-align-center' }>
              <span>{ errorMessage }</span>
              <br/>
              <br/>
              <GenericButton
              text = { 'OK' }
              onClick = { () => {
                this.showErrorModal(false, '')
                }
              }
              />
            </div>
          </Modal>
        }
        {
          showLoader ?
            <div className = { 'padding-top-35' }>
              <GenericLoader show = { showLoader } />
            </div>
          :
            <div>
              <div>
                {
                  tabs.map((resp, key) =>
                    <GenericButton
                      key = { key }
                      text = { resp.title }
                      type = { 'button' }
                      className = { `tabs-button margin-right-8px ${ isActive === resp.id ?' active' : ''}` }
                      onClick = { () => this.setActiveStatus(resp.id) }
                    />
                  )
                }
              </div>
              <br/>
              {
                isActive === 1 ?
                  <div className = { 'grid-global' }>
                    {
                      settingsArray &&
                        <AnswerSettingsComponent
                          settingsArray = { settingsArray }
                          methodsArray = { methodsArray.answerMethods }
                          answerGroupId = { answerGroupId }
                          answerGroupCode = { answerGroupCode }
                          answerGroupCodeErrorMessage = { answerGroupCodeErrorMessage }
                          answerGroupDesc = { answerGroupDesc }
                          answerGroupDescErrorMessage = { answerGroupDescErrorMessage }
                          answerGroupMethod = { answerGroupMethod }
                          answerGroupMethodErrorMessage = { answerGroupMethodErrorMessage }
                          selectedAnswerGroup = { (id, resp) => this.selectedAnswerGroup(id, resp) }
                          selectedAnswerMethod = { (id, method) => this.selectedAnswerMethod(id, method) }
                          setAnswerGroupCode = { (resp) => this.setAnswerGroupCode(resp) }
                          setAnswerGroupDesc = { (resp) => this.setAnswerGroupDesc(resp) }
                          submitAnswerGroup = { () => this.submitAnswerGroup() }
                        />
                    }
                    {
                      answerChoices &&
                        <AnswerChoicesComponent
                          answerChoices = { answerChoices }
                          answer = { answer }
                          answerErrorMessage = { answerErrorMessage }
                          rating = { rating }
                          ratingErrorMessage = { ratingErrorMessage }
                          imgUrl = { imgUrl }
                          imgUrlErrorMessage = { imgUrlErrorMessage }
                          setAnswer = { (resp) => this.setAnswer(resp) }
                          setRating = { (resp) => this.setRating(resp) }
                          setImgURL = { (resp) => this.setImgURL(resp) }
                          submitAnswerChoice = { () => this.submitAnswerChoice() }
                          editButton = { (resp) => this.editButton(resp) }
                        />
                    }
                  </div>
                :
                  isActive === 2 &&
                    <span>Email Template</span>
              }
            </div>
        }

      </div>
    )
  }
}

export default ConnectView(SettingsFragment, Presenter)
