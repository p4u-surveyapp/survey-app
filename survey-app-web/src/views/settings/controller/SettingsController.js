import {
  RequiredValidator
} from '../../../utils/validators'

export function requiredFieldValidator (input) {
  return new RequiredValidator().isValid(input) ? true : false
}
