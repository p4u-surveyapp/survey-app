import React from 'react';
import BaseMVPView from '../common/base/BaseMVPView'
import ConnectView from '../../utils/ConnectView'
import GenericCard from '../../components/Card/GenericCard'
import Modal from '../../components/Modal/Modal'
import GenericButton from '../../components/Button/GenericButton'
import GenericLoader from '../../components/Loader/GenericLoader'

import RegularComponent from './components/regular/RegularComponent'
import OptionalComponent from './components/optional/OptionalComponent'
import Presenter from './presenter/QuestionnaireManagementPresenter'
import './styles/styles.css'
import * as Validation from './controller/QuestionnaireManagementController'

class QuestionnaireManagementFragment extends BaseMVPView {
  constructor (props) {
    super(props)
    this.state = {
      title: '',
      titleErrorMessage: '',
      completeMessage: '',
      completeMessageErrorMessage: '',
      reminderRemainingDays: '',
      reminderRemainingDaysErrorMessage: '',
      startDay: '',
      startDayErrorMessage: '',
      startMonth: '',
      startMonthErrorMessage: '',
      startYear: '',
      startYearErrorMessage: '',
      endDay: '',
      endDayErrorMessage: '',
      endMonth: '',
      endMonthErrorMessage: '',
      endYear: '',
      endYearErrorMessage: '',
      urlLink: '',
      emailTitle: '',
      emailTitleErrorMessage: '',
      sendBy: '',
      sendByErrorMessage: '',
      subject: '',
      subjectErrorMessage: '',
      content: '',
      contentErrorMessage: '',
      targetPersonValue: '',
      targetPersonValueErrorMessage: '',
      errorMessage: '',
      targetNoOfMonths: '',
      operationStatusName: '',
      ansTemplate: '',
      qTypeTemplate: '',
      activeStatus: 0,
      operationStatus: 0,
      pageSize: 10,
      pageSizeRegular: 10,
      isActiveTab: 1,
      questionnaireId: 0,
      totalItems: 0,
      totalItemsRegular: 0,
      lastId: 0,
      lastIdRegular: 0,
      answerTypeModal: false,
      operationStatusModal: false,
      showError: false,
      showLoader: false,
      showYumeshinLoader: false,
      openTargetPersonModal: false,
      isEditMode: false,
      isMoreThanTargetNoOfMonths: 0,
      isMonthlyRecurring: 0,
      questionTypeModal: false,
      answerTypeModal: false,
      isQuestionRequired: false,
      activeLoader: false,
      tableArray: [],
      operationStat: [],
      optionalArray: [],
      questionsArray: [],
      employeesArray: [],
      chunkedArray: [],
      answerMethodArray: [],
      answerGroupArray: [],
      selectedRecipient: [],
      questionTypeArray: [],
      questionHeaderSettings: [],
      questionSetting: [],
      questionnaireIdTemp: 0,
      questionnaireSettingsId: 0,
    }

    this.editButton = this.editButton.bind(this)
    this.editRegularButton = this.editRegularButton.bind(this)
    this.setTitle = this.setTitle.bind(this)
    this.setCompleteMessage = this.setCompleteMessage.bind(this)
    this.setReminderRemainingDays = this.setReminderRemainingDays.bind(this)
    this.toggleMonthlyRecurring = this.toggleMonthlyRecurring.bind(this)
    this.setStatus = this.setStatus.bind(this)
    this.setStartDay = this.setStartDay.bind(this)
    this.setStartMonth = this.setStartMonth.bind(this)
    this.setStartYear = this.setStartYear.bind(this)
    this.setEndDay = this.setEndDay.bind(this)
    this.setEndMonth = this.setEndMonth.bind(this)
    this.setEndYear = this.setEndYear.bind(this)
    this.setUrlLink = this.setUrlLink.bind(this)
    this.setEmailTitle = this.setEmailTitle.bind(this)
    this.setSendBy = this.setSendBy.bind(this)
    this.setSubject = this.setSubject.bind(this)
    this.setContent = this.setContent.bind(this)
    this.showTargetPerson = this.showTargetPerson.bind(this)
    this.selectedTargetPersonValue = this.selectedTargetPersonValue.bind(this)

    this.showQuestionType = this.showQuestionType.bind(this)
    this.showOperationStatus = this.showOperationStatus.bind(this)
    this.setOptionalQuestionnaires = this.setOptionalQuestionnaires.bind(this)
    this.setActiveStatus = this.setActiveStatus.bind(this)
    this.showErrorModal = this.showErrorModal.bind(this)
    this.showLoaderModal = this.showLoaderModal.bind(this)
    this.showYumeshinLoaderModal = this.showYumeshinLoaderModal.bind(this)
    this.showActiveLoader = this.showActiveLoader.bind(this)
    this.submitOptionalQuestionnaire = this.submitOptionalQuestionnaire.bind(this)
    this.selectRecipient = this.selectRecipient.bind(this)
    this.unSelectRecipient = this.unSelectRecipient.bind(this)
    this.resetFields = this.resetFields.bind(this)
    this.addCheckedRecipient = this.addCheckedRecipient.bind(this)
    this.removeCheckedRecipient = this.removeCheckedRecipient.bind(this)
    this.recipientsMapper = this.recipientsMapper.bind(this)
    this.setQuestionnaireActiveStatus = this.setQuestionnaireActiveStatus.bind(this)
    this.setQuestionnaireDeactiveStatus = this.setQuestionnaireDeactiveStatus.bind(this)
    this.setRegularQuestionnaireActiveStatus = this.setRegularQuestionnaireActiveStatus.bind(this)
    this.setRegularQuestionnaireDeactiveStatus = this.setRegularQuestionnaireDeactiveStatus.bind(this)
  }

  componentDidMount() {
    this.props.setSelectedNavigation(3)
    this.presenter.getOperationStatus()
    this.presenter.getQuestionType()
    this.presenter.getAnswerMethod()
    this.presenter.getAnswerGroupTemplate()
    this.state.tableArray.length == 0 &&
    this.presenter.getRegularQuestionnaire(this.state.lastIdRegular, this.state.pageSizeRegular)
    this.presenter.getOptionalQuestionnaires(0, 10)
  }

  setData(data) {
    if(data.startDate) {
      const startDate = data.startDate.split('-')
      const endDate = data.endDate.split('-')
      this.setStartDay(startDate[2])
      this.setStartMonth(startDate[1])
      this.setStartYear(startDate[0])
      this.setEndDay(endDate[2])
      this.setEndMonth(endDate[1])
      this.setEndYear(endDate[0])
    } else {
      this.setStartDay(data.startDay)
      this.setEndDay(data.endDay)
    }

    this.setState({ questionnaireId: data.questionnaireId })
    this.setTitle(data.topMessage)
    this.setCompleteMessage(data.completeMessage)
    this.setReminderRemainingDays(data.reminderRemainingDay)
    this.toggleMonthlyRecurring(data.isMonthlyRecurring)
    this.setStatus(data.activeStatus)
    this.selectedTargetPersonValue(data.targetOption)
    this.setEmailTitle(data.emailSetting.title)
    this.setSendBy(data.emailSetting.sender)
    this.setSubject(data.emailSetting.subject)
    this.setContent(data.emailSetting.content)
    this.setState({ questionHeaderSettings: data.questionHeaderSettings })

    let questionSettingTemp = []
      data.questionHeaderSettings.map((val, key) =>
        {
          this.setTargetNoOfMonths(val.targetNoOfMonths)
          questionSettingTemp = val.questions
          this.state.questionnaireIdTemp = val.questionnaireId
          this.state.questionnaireSettingsIdTemp = val.questionHeaderSettingId
          this.selectedOperationStatus(val.operationStatusDesc, val.operationStatus)
        }
      )
    this.setState({ questionSetting: questionSettingTemp })
  }

  setTitle(title) {
    Validation.requiredFieldValidator(title) ?
    this.setState({ title , titleErrorMessage: ''})
    :
    this.setState({ title: '', titleErrorMessage: '*Required field' })
  }

  setCompleteMessage(completeMessage) {
    Validation.requiredFieldValidator(completeMessage) ?
    this.setState({ completeMessage , completeMessageErrorMessage: ''})
    :
    this.setState({ completeMessage: '', completeMessageErrorMessage: '*Required field' })
  }

  setReminderRemainingDays(reminderRemainingDays) {
    Validation.requiredFieldValidator(reminderRemainingDays) ?
    this.setState({ reminderRemainingDays , reminderRemainingDaysErrorMessage: ''})
    :
    this.setState({ reminderRemainingDays: '', reminderRemainingDaysErrorMessage: '*Required field' })
  }

  setStatus(activeStatus) {
    this.setState({ activeStatus: activeStatus ? 1 : 0 })
  }

  setStartDay(startDay) {
    Validation.requiredFieldValidator(startDay) ?
    this.setState({ startDay , startDayErrorMessage: ''})
    :
    this.setState({ startDay: '', startDayErrorMessage: '*Required field' })
  }

  setStartMonth(startMonth) {
    Validation.requiredFieldValidator(startMonth) ?
    this.setState({ startMonth , startMonthErrorMessage: ''})
    :
    this.setState({ startMonth: '', startMonthErrorMessage: '*Required field' })
  }

  setStartYear(startYear) {
    Validation.requiredFieldValidator(startYear) ?
    this.setState({ startYear , startYearErrorMessage: ''})
    :
    this.setState({ startYear: '', startYearErrorMessage: '*Required field' })
  }

  setEndDay(endDay) {
    Validation.requiredFieldValidator(endDay) ?
    this.setState({ endDay , endDayErrorMessage: ''})
    :
    this.setState({ endDay: '', endDayErrorMessage: '*Required field' })
  }

  setEndMonth(endMonth) {
    Validation.requiredFieldValidator(endMonth) ?
    this.setState({ endMonth , endMonthErrorMessage: ''})
    :
    this.setState({ endMonth: '', endMonthErrorMessage: '*Required field' })
  }

  setEndYear(endYear) {
    Validation.requiredFieldValidator(endYear) ?
    this.setState({ endYear , endYearErrorMessage: ''})
    :
    this.setState({ endYear: '', endYearErrorMessage: '*Required field' })
  }

  setUrlLink(urlLink) {
    this.setState({ urlLink })
  }

  setEmailTitle(emailTitle) {
    Validation.requiredFieldValidator(emailTitle) ?
    this.setState({ emailTitle , emailTitleErrorMessage: ''})
    :
    this.setState({ emailTitle: '', emailTitleErrorMessage: '*Required field' })
  }

  setSendBy(sendBy) {
    Validation.requiredFieldValidator(sendBy) ?
    this.setState({ sendBy , sendByErrorMessage: ''})
    :
    this.setState({ sendBy: '', sendByErrorMessage: '*Required field' })
  }

  setSubject(subject) {
    Validation.requiredFieldValidator(subject) ?
    this.setState({ subject , subjectErrorMessage: ''})
    :
    this.setState({ subject: '', subjectErrorMessage: '*Required field' })
  }

  setContent(content) {
    Validation.requiredFieldValidator(content) ?
    this.setState({ content , contentErrorMessage: ''})
    :
    this.setState({ content: '', contentErrorMessage: '*Required field' })
  }

  showTargetPerson(openTargetPersonModal) {
    this.setState({ openTargetPersonModal })
  }

  selectedTargetPersonValue(targetPersonValue) {
    Validation.requiredFieldValidator(targetPersonValue) ?
    this.setState({ targetPersonValue , targetPersonValueErrorMessage: ''})
    :
    this.setState({ targetPersonValue: '', targetPersonValueErrorMessage: '*Required field' })
    this.showTargetPerson(false)
  }

  setOptionalQuestionnaires(optionalArray) {
    this.setState({
      optionalArray: optionalArray.optionalQuestionnaires,
      totalItems: optionalArray.count,
      lastId: optionalArray.lastId
     })
  }

  setActiveStatus(isActiveTab) {
    this.setState({ isActiveTab })
  }

  setOperationStatus(resp) {
    this.setState({ operationStat: resp })
  }

  toggle12Months(isMoreThanTargetNoOfMonths) {
   this.setState({ isMoreThanTargetNoOfMonths: !isMoreThanTargetNoOfMonths ? 1 : 0})
  }

  showOperationStatus(operationStatusModal) {
   this.setState({ operationStatusModal })
  }

  setTargetNoOfMonths(targetNoOfMonths) {
   this.setState({ targetNoOfMonths})
  }

  showQuestionType(bool) {
   this.setState({ questionTypeModal: bool })
  }

  selectedOperationStatus(result, resultId) {
   this.setState({ operationStatusName: result, operationStatusId: resultId })
   this.showOperationStatus(false)
  }

  toggleQuestionRequired(isQuestionRequired) {
   this.setState({ isQuestionRequired: !isQuestionRequired ? 1 : 0})
  }

  toggleMonthlyRecurring(isMonthlyRecurring) {
    this.setState({ isMonthlyRecurring: isMonthlyRecurring ? 1 : 0})
  }

  selectedQuestionType(result) {
   this.setState({
     qTypeTemplate: result,
     ansTemplate: ''
   })
   this.showQuestionType(false)
  }

  showAnswerType(bool) {
   this.setState({ answerTypeModal: bool })
  }

  selectedAnswerTemplate(result) {
   this.setState({ ansTemplate: result })
   this.showAnswerType(false)
  }

  setQuestionArray(result) {
   this.setState({ questionsArray: result })
  }

  submitRegularQuestionnaire() {
    const {
      title,
      completeMessage,
      reminderRemainingDays,
      isMonthlyRecurring,
      activeStatus,
      startDay,
      endDay,
      content,
      sendBy,
      subject,
      emailTitle,
      targetNoOfMonths,
      isMoreThanTargetNoOfMonths,
      operationStatus,
      questionsArray,
      chunkedArray
    } = this.state
      this.presenter.submitRegularQuestionnaire(
        title,
        completeMessage,
        reminderRemainingDays,
        isMonthlyRecurring,
        activeStatus,
        startDay,
        endDay,
        content,
        sendBy,
        subject,
        emailTitle,
        targetNoOfMonths,
        isMoreThanTargetNoOfMonths,
        operationStatus,
        this.questionsMapper(questionsArray),
        chunkedArray)
  }

  updateRegularQuestionnaire() {
    const {
      questionnaireId,
      title,
      completeMessage,
      reminderRemainingDays,
      isMonthlyRecurring,
      activeStatus,
      startDay,
      endDay,
      content,
      sendBy,
      subject,
      emailTitle
    } = this.state

    this.presenter.updateRegularQuestionnaire(
      questionnaireId,
      title,
      completeMessage,
      reminderRemainingDays,
      isMonthlyRecurring,
      activeStatus,
      startDay,
      endDay,
      content,
      sendBy,
      subject,
      emailTitle)
  }

  submitOptionalQuestionnaire() {
    const {
      content,
      sendBy,
      subject,
      emailTitle,
      activeStatus,
      endDay,
      endMonth,
      endYear,
      reminderRemainingDays,
      startDay,
      startMonth,
      startYear,
      targetPersonValue,
      title,
      completeMessage,
      questionsArray,
      selectedRecipient,
      lastId
    } = this.state
    const startDate = (startYear+'-'+startMonth+'-'+startDay)
    const endDate = (endYear+'-'+endMonth+'-'+endDay)
    this.presenter.submitOptionalQuestionnaire(
      content,
      sendBy,
      subject,
      emailTitle,
      activeStatus,
      endDate,
      reminderRemainingDays,
      startDate,
      targetPersonValue,
      title,
      completeMessage,
      this.questionsMapper(questionsArray),
      this.recipientsMapper(selectedRecipient),
      lastId
    )
  }

  updateOptionalQuestionnaire() {
    const {
      title,
      reminderRemainingDays,
      activeStatus,
      endDay,
      endMonth,
      endYear,
      startDay,
      startMonth,
      startYear,
      urlLink,
      emailTitle,
      sendBy,
      subject,
      content,
      questionnaireId,
      targetPersonValue,
      completeMessage,
      lastId
    } = this.state
    const startDate = (startYear+'-'+startMonth+'-'+startDay)
    const endDate = (endYear+'-'+endMonth+'-'+endDay)
    this.presenter.updateOptionalQuestionnaire(
      title,
      reminderRemainingDays,
      activeStatus,
      startDate,
      endDate,
      urlLink,
      emailTitle,
      sendBy,
      subject,
      content,
      questionnaireId,
      targetPersonValue,
      completeMessage,
      lastId
    )
  }

  questionsMapper(questionsArray) {
    const questionTemp = []
    questionsArray.map((resp, val) =>
      {
        let question = {
          answerGroupId: resp.answerGroupId,
          isRequired: resp.isRequired ? 1 : 0,
          question: resp.question,
          questionTypeId: parseInt(resp.questionTypeId, 10),
          sequenceNo: parseInt(resp.sequenceNo, 10)
        }
        questionTemp.push({ question })
      }
    )
    return questionTemp
  }

  recipientArrayMapper(arr) {
    this.setState({chunkedArray: arr})
  }

  recipientsMapper(selectedRecipient) {
    const recipientTemp = []
    selectedRecipient.map((resp, val) =>
      {
        let recipient = {
          email: resp.email,
          firstName: resp.firstName,
          firstNameKana: resp.firstNameKana,
          groupId: resp.groupId,
          lastName: resp.lastName,
          lastNameKana: resp.lastNameKana,
          loginId: resp.loginId,
          month: resp.month,
          operationStatus: resp.division,
          year: resp.year
        }
        recipientTemp.push({ recipient })
      }
    )
   return recipientTemp
  }

  setRegularQuestionnaire(data) {
    this.setState({
      tableArray: data.regularQuestionnaires,
      totalItemsRegular: data.count,
      lastIdRegular: data.lastId
    })
  }

  showErrorModal(bool, message) {
    this.setState({
      errorMessage: message,
      showError: bool
    })
  }

  showLoaderModal(showLoader) {
    this.setState({ showLoader })
  }

  showYumeshinLoaderModal(showYumeshinLoader) {
    this.setState({ showYumeshinLoader })
  }

  showActiveLoader(activeLoader) {
    this.setState({ activeLoader })
  }

  setQuestionnaireId(result) {
    this.presenter.getQuestionnaireSettings(result)
  }

  editButton(isEditMode, id) {
    this.setState({ isEditMode })
    id &&
    this.presenter.getOptionalQuestionnaireById(id)
  }

  editRegularButton(isEditMode, id) {
    this.setState({ isEditMode })
    id &&
    this.presenter.getRegularQuestionnaireById(id)
  }

  getYumeshinEmployee(operationStatusId, targetNoOfMonths) {
    this.presenter.getYumeshinEmployee(
      operationStatusId,
      targetNoOfMonths
    )
  }

  setYumeshinEmployee(employeesArray) {
    this.setState({ employeesArray })
  }

  setAnswerMethod(resp) {
    this.setState({ answerMethodArray: resp })
  }

  setAnswerGroupTemplate(resp) {
    this.setState({ answerGroupArray: resp })
  }

  addCheckedRecipient(checkedItemsArray) {
    const {
      employeesArray,
      selectedRecipient
    } = this.state

    let tempSelected = [...selectedRecipient]
    let tempEmployee = [...employeesArray]

    checkedItemsArray.map((resp, key) =>
      {
        tempSelected.push(resp)
        tempEmployee.map((element, index) =>
          resp.id === element.id &&
          tempEmployee.splice(index, 1)
        )
      }
    )

    this.setState({ selectedRecipient: tempSelected,
      employeesArray: tempEmployee })
  }

  removeCheckedRecipient(checkedItemsArray) {
    const {
      employeesArray,
      selectedRecipient
    } = this.state

    let tempSelected = [...selectedRecipient]
    let tempEmployee = [...employeesArray]

    checkedItemsArray.map((resp, key) =>
      {
        tempEmployee.unshift(resp)
        tempSelected.map((element, index) =>
          resp.id === element.id &&
          tempSelected.splice(index, 1)
        )
      }
    )

    this.setState({ selectedRecipient: tempSelected,
      employeesArray: tempEmployee })
  }

  selectRecipient(data, key) {
    const {
      employeesArray,
      selectedRecipient
    } = this.state

    let tempSelected = [...selectedRecipient]
    let tempEmployee = [...employeesArray]

    tempSelected.push(data)

    tempEmployee.splice(key, 1)

    this.setState({ selectedRecipient: tempSelected,
      employeesArray: tempEmployee })
  }

  unSelectRecipient(data, key) {
    const {
      employeesArray,
      selectedRecipient
    } = this.state

    let tempSelected = [...selectedRecipient]
    let tempEmployee = [...employeesArray]

    employeesArray.length != 0 ?
      tempEmployee.unshift({
        division: data.division,
        email: data.email,
        firstName: data.firstName,
        firstNameKana: data.firstNameKana,
        groupId: data.groupId,
        id: data.id,
        lastName: data.lastName,
        lastNameKana: data.lastNameKana,
        loginId: data.loginId
      })
    :
      tempEmployee.push({
        division: data.division,
        email: data.email,
        firstName: data.firstName,
        firstNameKana: data.firstNameKana,
        groupId: data.groupId,
        id: data.id,
        lastName: data.lastName,
        lastNameKana: data.lastNameKana,
        loginId: data.loginId
      })

    tempSelected.splice(key, 1)

    this.setState({
      selectedRecipient: tempSelected,
      employeesArray: tempEmployee
    })
  }

  resetFields() {
    this.setState({
      title: '',
      titleErrorMessage: '',
      completeMessage: '',
      completeMessageErrorMessage: '',
      reminderRemainingDays: '',
      reminderRemainingDaysErrorMessage: '',
      activeStatus: 0,
      startDay: '',
      startDayErrorMessage: '',
      startMonth: '',
      startMonthErrorMessage: '',
      startYear: '',
      startYearErrorMessage: '',
      endDay: '',
      endDayErrorMessage: '',
      endMonth: '',
      endMonthErrorMessage: '',
      endYear: '',
      endYearErrorMessage: '',
      urlLink: '',
      emailTitle: '',
      emailTitleErrorMessage: '',
      sendBy: '',
      sendByErrorMessage: '',
      subject: '',
      subjectErrorMessage: '',
      content: '',
      contentErrorMessage: '',
      targetPersonValue: '',
      targetPersonValueErrorMessage: '',
      errorMessage: '',
      targetNoOfMonths: '',
      operationStatusName: '',
      operationStatusId: 0,
      questionnaireId: 0,
      openTargetPersonModal: false,
      isMoreThanTargetNoOfMonths: false,
      isMonthlyRecurring: 0,
      isEditMode: false,
      showYumeshinLoader: false,
      questionsArray: [],
      employeesArray: [],
      answerMethodName: '',
      answerMethodId: 0,
      selectedRecipient: [],
      lastId: 0,
      totalItems: 0,
      pageSize: 10,
      lastIdRegular: 0,
      totalItemsRegular:0 ,
      pageSizeRegular: 0,
      tableArray: []
    })
  }

  setChunkedRecipients() {
    const chunked_arr = [];
    const size = 1000
    let copied = [...this.state.employeesArray];
    const numOfChild = Math.ceil(copied.length / size);


    for (let i = 0; i < numOfChild; i++) {
        chunked_arr.push(copied.splice(0, size));
    }

    const chunkedArrTemp = []
    chunked_arr.map((resp, key) =>
      resp.map((resp2, key2) =>
        {
          let recipient = {
            email: resp2.email,
            firstName: resp2.firstName,
            firstNameKana: resp2.firstNameKana,
            groupId: resp2.groupId,
            lastName: resp2.lastName,
            lastNameKana: resp2.lastNameKana,
            loginId: resp2.loginId,
            month: 0,
            operationStatus: '',
            year: ''
          }
          chunkedArrTemp.push({ recipient })
        }
      )
    )

    this.recipientArrayMapper(chunkedArrTemp)
  }

  setQuestionnaireActiveStatus(questionnaireId) {
    this.presenter.putActiveQuestionnaire(questionnaireId)
  }

  setQuestionnaireDeactiveStatus(questionnaireId) {
    this.presenter.putDeactiveQuestionnaire(questionnaireId)
  }

  setRegularQuestionnaireActiveStatus(questionnaireId) {
    this.presenter.putRegularActiveQuestionnaire(questionnaireId)
  }

  setRegularQuestionnaireDeactiveStatus(questionnaireId) {
    this.presenter.putRegularDeactiveQuestionnaire(questionnaireId)
  }

  setQuestionType(resp) {
    this.setState({ questionTypeArray: resp })
  }

  putRegularQuestionnaire(operationStatusId, targetNoOfMonths) {
    this.presenter.putRegularQuestionnaireSettings(
      operationStatusId,
      this.state.questionnaireIdTemp,
      this.state.questionnaireSettingsIdTemp,
      targetNoOfMonths)
  }

  render () {
    const {
      isEditMode,
      title,
      titleErrorMessage,
      completeMessage,
      completeMessageErrorMessage,
      reminderRemainingDays,
      reminderRemainingDaysErrorMessage,
      activeStatus,
      startDay,
      startDayErrorMessage,
      startMonth,
      startMonthErrorMessage,
      startYear,
      startYearErrorMessage,
      endDay,
      endDayErrorMessage,
      endMonth,
      endMonthErrorMessage,
      endYear,
      endYearErrorMessage,
      urlLink,
      emailTitle,
      emailTitleErrorMessage,
      sendBy,
      sendByErrorMessage,
      subject,
      subjectErrorMessage,
      content,
      contentErrorMessage,
      targetPersonValue,
      targetPersonValueErrorMessage,
      targetNoOfMonths,
      operationStatusId,
      operationStatusName,
      operationStatusModal,
      isActiveTab,
      tableArray,
      operationStat,
      optionalArray,
      showLoader,
      showYumeshinLoader,
      showError,
      errorMessage,
      questionnaireId,
      openTargetPersonModal,
      isMoreThanTargetNoOfMonths,
      isMonthlyRecurring,
      questionTypeModal,
      answerTypeModal,
      qTypeTemplate,
      ansTemplate,
      isQuestionRequired,
      questionsArray,
      employeesArray,
      chunkedArray,
      answerMethodArray,
      answerGroupArray,
      selectedRecipient,
      questionTypeArray,
      totalItems,
      lastId,
      activeLoader,
      questionHeaderSettings,
      questionSetting,
      lastIdRegular,
      totalItemsRegular,
      pageSizeRegular,
      questionnaireIdTemp,
      questionnaireSettingsIdTemp
    } = this.state

    const tabs = [
      {
        id: 1,
        title: 'Regular Questionnaire',
      },
      {
        id: 2,
        title: 'Optional Questionnaire',
      }
    ]

    return (
      <div>
        {
          showError &&
          <Modal>
            <div className = { 'text-align-center' }>
              <span>{ errorMessage }</span>
              <br/>
              <br/>
              <GenericButton
                text = { 'OK' }
                onClick = { () => this.showErrorModal(false, '') }
              />
            </div>
          </Modal>
        }
        {
          showLoader ?
            <Modal width = { 10 }>
              <GenericLoader show = { showLoader } />
            </Modal>
          :
          <div>
            <div>
              <GenericButton
                text = { 'Regular Questionnaire' }
                type = { 'button' }
                className = { `tabs-button margin-right-8px ${ isActiveTab === 1 ?' active' : ''}` }
                onClick = { () => {
                    this.setActiveStatus(1)
                  }
                }
              />
              <GenericButton
                text = { 'Optional Questionnaire' }
                type = { 'button' }
                className = { `tabs-button margin-right-8px ${ isActiveTab === 2 ?' active' : ''}` }
                onClick = { () => {
                    this.setActiveStatus(2)
                  }
                }
              />
            </div>
            <br/>
            {
              isActiveTab === 1 ?
                <RegularComponent
                  totalItemsRegular = { totalItemsRegular }
                  lastIdRegular = { lastIdRegular }
                  isEditMode = { isEditMode }
                  questionnaireId = { questionnaireId }
                  title = { title }
                  titleErrorMessage = { titleErrorMessage }
                  completeMessage = { completeMessage }
                  completeMessageErrorMessage = { completeMessageErrorMessage }
                  reminderRemainingDays = { reminderRemainingDays }
                  reminderRemainingDaysErrorMessage = { reminderRemainingDaysErrorMessage }
                  isMonthlyRecurring = { isMonthlyRecurring }
                  activeStatus = { activeStatus }
                  startDay = { startDay }
                  startDayErrorMessage = { startDayErrorMessage }
                  endDay = { endDay }
                  endDayErrorMessage = { endDayErrorMessage }
                  urlLink = { urlLink }
                  emailTitle = { emailTitle }
                  sendBy = { sendBy }
                  subject = { subject }
                  content = { content }
                  targetNoOfMonths = { targetNoOfMonths }
                  openTargetPersonModal = { openTargetPersonModal }
                  targetPersonValue = { targetPersonValue }
                  isMoreThanTargetNoOfMonths = { isMoreThanTargetNoOfMonths }
                  operationStatusId = { operationStatusId }
                  operationStatusName = { operationStatusName }
                  operationStatusModal = { operationStatusModal }
                  operationStat = { operationStat }
                  questionTypeModal = { questionTypeModal }
                  answerTypeModal = { answerTypeModal }
                  ansTemplate = { ansTemplate }
                  qTypeTemplate = { qTypeTemplate }
                  questionsArray = { questionsArray }
                  employeesArray = { employeesArray }
                  isQuestionRequired = { isQuestionRequired }
                  editRegularButton = { (bool, id) => this.editRegularButton(bool, id) }
                  setTitle = { (resp) => this.setTitle(resp) }
                  setCompleteMessage = { (resp) => this.setCompleteMessage(resp) }
                  setReminderRemainingDays = { (resp) => this.setReminderRemainingDays(resp) }
                  setStatus = { (resp) => this.setStatus(resp) }
                  setStartDay = { (resp) => this.setStartDay(resp) }
                  setEndDay = { (resp) => this.setEndDay(resp) }
                  setUrlLink = { (resp) => this.setUrlLink(resp) }
                  setEmailTitle = { (resp) => this.setEmailTitle(resp) }
                  setSendBy = { (resp) => this.setSendBy(resp) }
                  setSubject = { (resp) => this.setSubject(resp) }
                  setContent = { (resp) => this.setContent(resp) }
                  questionTypeArray = { questionTypeArray }
                  toggleMonthlyRecurring = { (resp) => this.toggleMonthlyRecurring(resp) }
                  toggle12Months = { (resp) => this.toggle12Months(resp) }
                  showOperationStatus = { (resp) => this.showOperationStatus(resp) }
                  showQuestionSetup = { (resp) => this.showQuestionSetup(resp) }
                  setTargetNoOfMonths = { (resp) => this.setTargetNoOfMonths(resp) }
                  selectedOperationStatus = { (resp, respId) => this.selectedOperationStatus(resp, respId) }
                  toggleQuestionRequired = { (resp) => this.toggleQuestionRequired(resp) }
                  showQuestionType = { (resp) => this.showQuestionType(resp) }
                  selectedQuestionType = { (resp) => this.selectedQuestionType(resp) }
                  showAnswerType = { (resp) => this.showAnswerType(resp) }
                  selectedAnswerTemplate = { (resp) => this.selectedAnswerTemplate(resp) }
                  setQuestionArray = { (resp) => this.setQuestionArray(resp) }
                  toggleQuestionRequired = { (resp) => this.toggleQuestionRequired(resp) }
                  showTargetPerson = { (resp) => this.showTargetPerson(resp) }
                  selectedTargetPersonValue = { (resp) => this.selectedTargetPersonValue(resp) }
                  showErrorModal = { (resp) => this.showErrorModal(resp) }
                  showError = { showError }
                  errorMessage = { errorMessage }
                  tableArray = { tableArray }
                  questionSetting = { questionSetting }
                  resetFields = { () => this.resetFields() }
                  setQuestionnaireId = { (resp) => this.setQuestionnaireId(resp) }
                  getYumeshinEmployee = { (operationStatusId, targetNoOfMonths) => this.getYumeshinEmployee(operationStatusId, targetNoOfMonths) }
                  submitRegularQuestionnaire = { () => this.submitRegularQuestionnaire() }
                  updateRegularQuestionnaire = { () => this.updateRegularQuestionnaire() }
                  getAnswerTemplate = { () => this.presenter.getAnswerGroupTemplate() }
                  answerMethodArray = { answerMethodArray }
                  answerGroupArray = { answerGroupArray }
                  setRegularQuestionnaireActiveStatus = { (resp) => this.setRegularQuestionnaireActiveStatus(resp) }
                  setRegularQuestionnaireDeactiveStatus = { (resp) => this.setRegularQuestionnaireDeactiveStatus(resp) }
                  questionHeaderSettings = { questionHeaderSettings }
                  getRegularQuestionnaire = { (pageNo, pageSize) => this.presenter.getRegularQuestionnaire(pageNo, pageSize) }
                  putRegularQuestionnaire = { (operationStatusId, targetNoOfMonths) => this.putRegularQuestionnaire(operationStatusId, targetNoOfMonths) }
                  />
              :
                isActiveTab === 2 &&
                <OptionalComponent
                  totalItems = { totalItems }
                  lastId = { lastId }
                  isEditMode = { isEditMode }
                  questionnaireId = { questionnaireId }
                  title = { title }
                  titleErrorMessage = { titleErrorMessage }
                  completeMessage = { completeMessage }
                  completeMessageErrorMessage = { completeMessageErrorMessage }
                  reminderRemainingDays = { reminderRemainingDays }
                  reminderRemainingDaysErrorMessage = { reminderRemainingDaysErrorMessage }
                  activeStatus = { activeStatus }
                  startDay = { startDay }
                  startDayErrorMessage = { startDayErrorMessage }
                  startMonth = { startMonth }
                  startMonthErrorMessage = { startMonthErrorMessage }
                  startYear = { startYear }
                  startYearErrorMessage = { startYearErrorMessage }
                  endDay = { endDay }
                  endDayErrorMessage = { endDayErrorMessage }
                  endMonth = { endMonth }
                  endMonthErrorMessage = { endMonthErrorMessage }
                  endYear = { endYear }
                  endYearErrorMessage = { endYearErrorMessage }
                  urlLink = { urlLink }
                  emailTitle = { emailTitle }
                  emailTitleErrorMessage = { emailTitleErrorMessage }
                  sendBy = { sendBy }
                  sendByErrorMessage = { sendByErrorMessage }
                  subject = { subject }
                  subjectErrorMessage = { subjectErrorMessage }
                  content = { content }
                  contentErrorMessage = { contentErrorMessage }
                  targetPersonValue = { targetPersonValue }
                  targetPersonValueErrorMessage = { targetPersonValueErrorMessage }
                  openTargetPersonModal = { openTargetPersonModal }
                  targetNoOfMonths = { targetNoOfMonths }
                  isMoreThanTargetNoOfMonths = { isMoreThanTargetNoOfMonths }
                  operationStatusId = { operationStatusId }
                  operationStatusName = { operationStatusName }
                  operationStatusModal = { operationStatusModal }
                  questionTypeModal = { questionTypeModal }
                  answerTypeModal = { answerTypeModal }
                  ansTemplate = { ansTemplate }
                  qTypeTemplate = { qTypeTemplate }
                  questionsArray = { questionsArray }
                  employeesArray = { employeesArray }
                  showYumeshinLoader = { showYumeshinLoader }
                  selectedRecipient = { selectedRecipient }
                  showError = { showError }
                  errorMessage = { errorMessage }
                  tableArray = { optionalArray }
                  activeLoader = { activeLoader }
                  editButton = { (bool, id) => this.editButton(bool, id) }
                  setTitle = { (resp) => this.setTitle(resp) }
                  setCompleteMessage = { (resp) => this.setCompleteMessage(resp) }
                  setReminderRemainingDays = { (resp) => this.setReminderRemainingDays(resp) }
                  setStatus = { (resp) => this.setStatus(resp) }
                  setStartDay = { (resp) => this.setStartDay(resp) }
                  setStartMonth = { (resp) => this.setStartMonth(resp) }
                  setStartYear = { (resp) => this.setStartYear(resp) }
                  setEndDay = { (resp) => this.setEndDay(resp) }
                  setEndMonth = { (resp) => this.setEndMonth(resp) }
                  setEndYear = { (resp) => this.setEndYear(resp) }
                  setUrlLink = { (resp) => this.setUrlLink(resp) }
                  setEmailTitle = { (resp) => this.setEmailTitle(resp) }
                  setSendBy = { (resp) => this.setSendBy(resp) }
                  setSubject = { (resp) => this.setSubject(resp) }
                  setContent = { (resp) => this.setContent(resp) }

                  toggle12Months = { (resp) => this.toggle12Months(resp) }
                  showOperationStatus = { (resp) => this.showOperationStatus(resp) }
                  showQuestionSetup = { (resp) => this.showQuestionSetup(resp) }
                  setTargetNoOfMonths = { (resp) => this.setTargetNoOfMonths(resp) }
                  selectedOperationStatus = { (resp, respId) => this.selectedOperationStatus(resp, respId) }
                  toggleQuestionRequired = { (resp) => this.toggleQuestionRequired(resp) }
                  showQuestionType = { (resp) => this.showQuestionType(resp) }
                  selectedQuestionType = { (resp) => this.selectedQuestionType(resp) }
                  showAnswerType = { (resp) => this.showAnswerType(resp) }
                  selectedAnswerTemplate = { (resp) => this.selectedAnswerTemplate(resp) }
                  setQuestionArray = { (resp) => this.setQuestionArray(resp) }

                  showTargetPerson = { (resp) => this.showTargetPerson(resp) }
                  selectedTargetPersonValue = { (resp) => this.selectedTargetPersonValue(resp) }
                  showErrorModal = { (resp) => this.showErrorModal(resp) }
                  resetFields = { () => this.resetFields() }
                  addCheckedRecipient = { (resp) => this.addCheckedRecipient(resp) }
                  removeCheckedRecipient = { (resp) => this.removeCheckedRecipient(resp) }
                  updateOptionalQuestionnaire = { () => this.updateOptionalQuestionnaire() }
                  submitOptionalQuestionnaire = { () => this.submitOptionalQuestionnaire()}
                  getYumeshinEmployee = { (operationStatusId, targetNoOfMonths) => this.getYumeshinEmployee(operationStatusId, targetNoOfMonths) }
                  showYumeshinLoaderModal = { (resp) => this.showYumeshinLoaderModal(resp) }
                  selectRecipient = { (resp, key) => this.selectRecipient(resp, key) }
                  unSelectRecipient = { (resp, key) => this.unSelectRecipient(resp, key) }
                  setQuestionnaireActiveStatus = { (resp) => this.setQuestionnaireActiveStatus(resp) }
                  setQuestionnaireDeactiveStatus = { (resp) => this.setQuestionnaireDeactiveStatus(resp) }
                  getAnswerTemplate = { () => this.presenter.getAnswerGroupTemplate() }
                  getOptionalQuestionnaires = { (pageNo, pageSize) => this.presenter.getOptionalQuestionnaires(pageNo, pageSize) }
                />
            }
          </div>
        }
      </div>
    )
  }
}

export default ConnectView(QuestionnaireManagementFragment, Presenter)
