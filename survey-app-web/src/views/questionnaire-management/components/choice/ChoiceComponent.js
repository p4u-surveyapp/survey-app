import React, { Component } from 'react'

import GenericInput from '../../../../components/TextBox/GenericInput'

class ChoiceComponent extends Component {

  constructor (props) {
    super (props)
  }

  render () {
    const {
      label,
      value,
      onChangeChoices
    } = this.props

    return (
            <div
              className = { 'grid-global-question align-items-center'} >
              <div className = { 'font-size-12px' } >{ label }</div>
              <GenericInput
                defaultValue = { '' }
                type = { 'text' }
                hint = { 'Enter Choice' }
                value = { value }
                onChange = { (e) => onChangeChoices(e.target.value) }
              />
            </div>
    )
  }

}

export default ChoiceComponent
