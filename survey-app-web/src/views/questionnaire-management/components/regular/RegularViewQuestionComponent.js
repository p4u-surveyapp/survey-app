import React, { Component } from 'react'

import GenericCard from '../../../../components/Card/GenericCard'
import GenericInput from '../../../../components/TextBox/GenericInput'
import GenericButton from '../../../../components/Button/GenericButton'
import Modal from '../../../../components/Modal/Modal'
import Line from '../../../../components/Line/Line'
import { RadioGroup, RadioButton, ReversedRadioButton } from 'react-radio-buttons'

class RegularViewQuestionComponent extends Component {
  constructor (props) {
    super (props)
    this.state = {
      isActive: false,
      opStat: ''
    }
  }

  setActiveSettings(isActive) {
    this.setState({ isActive })
  }

  render () {

    const {
      isActive,
      opStat
    } = this.state

    const {
      isEditMode,
      targetNoOfMonths,
      setTargetNoOfMonths,
      questionHeaderSettings,
      questionSetting,
      operationStat,
      operationStatusId,
      operationStatusName,
      operationStatusModal,
      showOperationStatus,
      selectedOperationStatus,
      updateQuestionSettings,
      putRegularQuestionnaire,
      questionTypeArray
    } = this.props

    return (
      <div>
      {
        isEditMode ?
        'No Data'
        :
        <div>
          {
            operationStatusModal &&
              <Modal
                onClose = { () => showOperationStatus(false) } >
                <h3 className = { 'text-align-center' }>Target Operation Status</h3>
                <Line/>
                {
                  operationStat &&
                  operationStat.map((resp, key) =>
                      <label
                        className = { `method-rows text-align-center cursor-pointer
                          ${ operationStatusName === resp.operationStatusId && 'active' }` }
                        key = { key }
                        onClick = { () => selectedOperationStatus(resp.operationStatus, resp.operationStatusId) }>
                        <span className = { 'padding' }>
                          { resp.operationStatus }
                        </span>
                        <Line/>
                      </label>
                  )
                }
              </Modal>
          }
          {
            isActive ?
              <div className = { 'grid-global-columns-x3-header' } >
                <GenericInput
                  type = { 'number' }
                  hint = { 'Target No. of Months' }
                  text = { 'Target No. of Months' }
                  value = { targetNoOfMonths }
                  onChange = { (e) => setTargetNoOfMonths(e.target.value) } />

                <GenericInput
                  defaultValue = { '' }
                  onChange = { (e) => { operationStatusName = e.target.value } }
                  onClick = { () => showOperationStatus(true) }
                  value = { operationStatusName }
                  text = { 'Target Operation Status' } />

                <GenericButton
                  className = { 'tabs-button active' }
                  type = { 'button' }
                  onClick = { () =>
                      {
                        this.setActiveSettings(false)
                        putRegularQuestionnaire(
                          operationStatusId,
                          targetNoOfMonths
                        )
                    }
                  }
                  text = { 'Save' } />
              </div>
            :
              <div className = { 'grid-global-columns-x3-header' } >
                <div>
                  <label className = { 'font-size-14px' }>Target No. of Months:</label>
                  <br/>
                  <label className = { 'font-size-12px label-color' }>
                    { targetNoOfMonths ? targetNoOfMonths : '0' }
                  </label>
                </div>
                <div>
                  <label className = { 'font-size-14px' }>Target Operation Status:</label>
                  <br/>
                  <label className = { 'font-size-12px label-color' }>
                    { operationStatusName ? operationStatusName : 'Status' }
                  </label>
                </div>
                <GenericButton
                  className = { 'tabs-button active' }
                  type = { 'button' }
                  onClick = { () =>
                      this.setActiveSettings(true)
                  }
                  text = { 'Edit' } />
              </div>
          }

          <Line className = { 'margin-tb-8px' } />
          {
            questionSetting &&
            questionSetting.map((question, keyQuestion) =>
              <GenericCard
                className = { 'margin-bottom-8px' }
                key = { keyQuestion } >
                <div>
                  {
                    questionTypeArray.map((questionItem, keyItem) =>
                      questionItem.questionTypeId === question.questionTypeId &&
                      <label
                        className = { 'semi-round-btn font-size-12px' } >
                        { questionItem.description }
                      </label>
                    )
                  }
                </div>
                <div
                  className = { 'margin-tb-8px grid-question-view' } >
                  <div
                    className = { 'numberCircle' } >
                    { question.sequenceNo ? question.sequenceNo : '' }
                  </div>

                  <label
                    className = { 'margin-8px' } >
                    { question.question ? question.question : 'No Data' }
                  </label>

                  <div className = { 'action-edit-circle' } >
                    <span className = { 'padding-18px' } />
                  </div>

                </div>
              </GenericCard>
            )
          }

        </div>
      }
      </div>
    )
  }
}

export default RegularViewQuestionComponent
