import React, { Component } from 'react'

import GenericCard from '../../../../components/Card/GenericCard'
import GenericInput from '../../../../components/TextBox/GenericInput'
import GenericButton from '../../../../components/Button/GenericButton'
import GenericLoader from '../../../../components/Loader/GenericLoader'
import Modal from '../../../../components/Modal/Modal'
import Line from '../../../../components/Line/Line'

class RegularViewComponent extends Component {

  constructor (props) {
    super (props)

  }

  render () {
    const {
      isEditMode,
      setAction,
      title,
      setTitle,
      completeMessage,
      setCompleteMessage,
      reminderRemainingDays,
      setReminderRemainingDays,
      setDescription,
      isMonthlyRecurring,
      toggleMonthlyRecurring,
      setStatus,
      activeStatus,
      startDay,
      setStartDay,
      endDay,
      setEndDay,
      emailTitle,
      setEmailTitle,
      sendBy,
      setSendBy,
      subject,
      setSubject,
      content,
      setContent,
      questionSetting,
      updateRegularQuestionnaire,
      questionnaireId,
      setRegularQuestionnaireActiveStatus,
      setRegularQuestionnaireDeactiveStatus,
      activeLoader,
      cancelButton
    } = this.props

    return (
      <div>
        {
        isEditMode ?
        <div>
          <div>
            <div>
              <span className = { 'questionnaire-heading-label' }>
                Questionnaire Settings
              </span>
            </div>
            <Line className = { 'margin-tb-8px' } />
            <div className = { 'grid-global' }>
              <div>
                  <GenericInput
                    type = { 'text' }
                    hint = { 'Top Message' }
                    text = { 'Top Message' }
                    value = { title }
                    onChange = { (e) => setTitle(e.target.value) }
                  />
              </div>
              <div>
                <GenericInput
                  type = { 'number' }
                  hint = { 'Reminder Remaining Days' }
                  text = { 'Reminder Remaining Days' }
                  value = { reminderRemainingDays }
                  onChange = { (e) => setReminderRemainingDays(e.target.value) }
                />
              </div>
            </div>

            <div>
              <GenericInput
                type = { 'textarea' }
                hint = { 'Complete Message' }
                text = { 'Complete Message' }
                value = { completeMessage }
                onChange = { (e) => setCompleteMessage(e.target.value) }
              />
            </div>

            <div className = { 'grid-global' } >
              <GenericInput
                type = { 'checkbox' }
                checked = { isMonthlyRecurring ? true : false }
                onChange = { (e) => toggleMonthlyRecurring(e.target.checked) }
                hint = { 'Monthly Recurring' }
                text = { 'Monthly Recurring' }
              />

              <div />
            </div>

            <div className = { 'grid-global' }>
              <GenericInput
                type = { 'number' }
                hint = { 'Start Day' }
                text = { 'Start Day' }
                value = { startDay }
                onChange = { (e) => setStartDay(e.target.value) }
              />
              <GenericInput
                type = { 'number' }
                hint = { 'End Day' }
                text = { 'End Day' }
                value = { endDay }
                onChange = { (e) => setEndDay(e.target.value) }
              />
            </div>
          </div>
          <div>
            <div>
              <a className = { 'questionnaire-heading-label' }>
                Email Settings
              </a>
            </div>
            <Line className = { 'margin-tb-8px' } />
            <div className = { 'grid-global' } >

                <GenericInput
                  type = { 'text' }
                  hint = { 'Email Title' }
                  text = { 'Email Title' }
                  value = { emailTitle }
                  onChange = { (e) => setEmailTitle(e.target.value) }
                />
                <GenericInput
                  type = { 'text' }
                  hint = { 'Send By' }
                  text = { 'Send By' }
                  value = { sendBy }
                  onChange = { (e) => setSendBy(e.target.value) }
                />
            </div>
            <div>
                <GenericInput
                  type = { 'text' }
                  hint = { 'Subject' }
                  text = { 'Subject' }
                  value = { subject }
                  onChange = { (e) => setSubject(e.target.value) }
                />
            </div>

            <div>
              <GenericInput
                type = { 'textarea' }
                hint = { 'Content' }
                text = { 'Content' }
                value = { content }
                onChange = { (e) => setContent(e.target.value) }
              />
            </div>
          </div>
          <div className = { 'grid-global' } >
            <GenericButton
              className = { 'tabs-button' }
              text = { 'Cancel' }
              onClick = { () => cancelButton() }/>

            <GenericButton
              className = { 'tabs-button active' }
              text = { 'Save' }
              onClick = { () => updateRegularQuestionnaire() }/>
          </div>
        </div>
        :
        <div>
          <div className = { 'grid-global' } >
            <span className = { 'questionnaire-heading-label' }>
              Questionnaire Settings
            </span>
            <div className = { 'rtl'} >
            {
              activeLoader ?
                <GenericLoader show = { activeLoader } />
              :
                activeStatus ?
                  <GenericButton
                    className = { 'tabs-button active' }
                    onClick = { () => setRegularQuestionnaireDeactiveStatus(questionnaireId) }
                    text = { 'Deactivate' } />
                :
                  <GenericButton
                    className = { 'tabs-button' }
                    onClick = { () => setRegularQuestionnaireActiveStatus(questionnaireId) }
                    text = { 'Activate' } />
            }
            </div>
          </div>
          <Line className = { 'margin-tb-8px' } />
          <div className = { 'grid-global' }>
              <label className = { 'font-size-16px margin-bottom-8px' }>{ title ? title : 'Top Message' }</label>
              <br/>
              <label className = { 'font-size-12px label-color margin-bottom-8px' }>Reminder Remaining Days: { reminderRemainingDays ? reminderRemainingDays : '0' } Day(s)</label>
              <label className = { 'font-size-12px label-color margin-bottom-8px' }>Monthly Recurring: { isMonthlyRecurring ? 'Yes' : 'No' }</label>
              <label className = { 'font-size-12px label-color margin-bottom-8px' }>Active: { activeStatus ? 'Yes' : 'No' }</label>
            <label className = { 'font-size-12px label-color margin-bottom-8px' }>Start Day: { startDay ? startDay : 'dd' } - { endDay ? endDay : 'dd' }</label>
          </div>
          <br/>
          <div>
            <span className = { 'questionnaire-heading-label' }>
              Email Settings
            </span>
            <Line className = { 'margin-top-12px' } />
            <br/>
          </div>
          <div className = { 'grid-global' }>
            <label className = { 'font-size-16px margin-bottom-8px' }>{ emailTitle ? emailTitle : 'Title' }</label>
            <br/>
            <label className = { 'font-size-12px label-color margin-bottom-8px' }>Sender: { sendBy ? sendBy : 'Send By' }</label>
            <label className = { 'font-size-12px label-color margin-bottom-8px' }>Subject: { subject ? subject : 'Subject' }</label>
            <label className = { 'font-size-12px label-color margin-bottom-8px' }>Content: { content ? content : 'Message...' }</label>
          </div>
        </div>
      }
      </div>
    )
  }
}

export default RegularViewComponent
