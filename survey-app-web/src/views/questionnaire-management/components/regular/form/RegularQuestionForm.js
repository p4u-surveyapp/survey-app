import React, { Component } from 'react'
import GenericCard from '../../../../../components/Card/GenericCard'
import GenericInput from '../../../../../components/TextBox/GenericInput'
import GenericDropdown from '../../../../../components/Dropdown/GenericDropdown'
import GenericButton from '../../../../../components/Button/GenericButton'
import Modal from '../../../../../components/Modal/Modal'
import Line from '../../../../../components/Line/Line'
import GenericLoader from '../../../../../components/Loader/GenericLoader'
import Table from '../../../../../components/Table/Table'
import '../styles/regular.css'

import { RadioGroup, RadioButton, ReversedRadioButton } from 'react-radio-buttons'

class RegularQuestionForm extends Component {

  constructor (props) {
    super(props)

    this.state = {
      isActive: false,
      answerGroupModal: false,
      questionItem: [],
      qSeqNo: 0,
      qItem: '',
      qTypeState: '',
      qAnsItem: 0,
      options: []
    }
  }

  componentDidMount() {
    this.answerOptions()
  }

  setActiveSettings(isActive) {
    this.setState({ isActive })
  }

  removeChild(id, item) {
    let tempItem = [...item]
    tempItem.map((val, key) => {
      if(key === id) {
        tempItem.splice(id, 1)
      }
    })
    this.setState({ questionItem: tempItem})
  }

  appendChild(itemId, item) {
    let tempItem = [...item]
    tempItem.push({
      id: itemId + tempItem.length
    })
    this.setState({ questionItem: tempItem})
  }

  showAnswerGroupModal(answerGroupModal) {
    this.setState({ answerGroupModal })
  }

  setSequenceNo(resp) {
    return this.setState({ qSeqNo: resp })
  }

  setQuestion(resp){
    return this.setState({ qItem: resp })
  }

  setQuestionType(resp) {
    return this.setState({ qTypeState: resp })
  }

  setAnswerItem(resp) {
    return this.setState({ qAnsItem: resp })
  }

  fun(seqNo, question, isRequired, questionType, answerItem, qTypeFlag, key) {
    let questionTemp = [...this.state.questionItem]

    let sequenceItem = this.setSequenceNo(seqNo)
    let questionItem = this.setQuestion(question)
    let questionTypeItem = this.setQuestionType(questionType)
    let ansItem = this.setAnswerItem(answerItem)

    questionTemp.length != 0 &&
    questionTemp.map((qItem, keyItem) =>
        {
          if (key === keyItem) {
              questionTemp.splice(keyItem, 1,
                qTypeFlag ?
                {
                  id: key,
                  sequenceNo: sequenceItem,
                  question: questionItem,
                  isRequired: isRequired,
                  questionTypeId: questionTypeItem,
                  answerGroupId: ansItem
                }
                :
                {
                  id: key,
                  sequenceNo: sequenceItem,
                  question: questionItem,
                  isRequired: isRequired,
                  questionTypeId: questionTypeItem,
                  ans: 0,
                  answerGroupId: 0,
                  groupChoice: []
                }
            )
          }
      }
    )
  }

  setAnswers(answers, answerGroupId, resp) {
    let opt = []

    for(var i = 0; i < answers.length; i++) {
        answers[i].map((ans, key) =>
          {
            if(ans.answerGroupId == answerGroupId) {
              opt.push(
                  <ReversedRadioButton
                    key = { key }
                    rootColor = { '#94979e' }
                    value = { String(ans.answer) } >
                    { ans.answer }
                  </ReversedRadioButton>
              )
            }
          }
        )
    }
    resp.groupChoice = opt
  }

  answerGroupTemplateOptions(group, id) {
    let answerGroupTemp = []
    group.map((resp, key) =>
      {
        if(resp.answerMethodId == id)
          answerGroupTemp.push({
            id: resp.answerMethodId,
            name: resp.method
          })
      }
    )
    return answerGroupTemp
  }

  answerMethodOptions(method) {
    let answerMethodTemp = []
    method.map((resp, key) =>
      answerMethodTemp.push({
        id: resp.answerMethodId,
        name: resp.method
      })
    )
    return answerMethodTemp
  }

  questionTypeOptions(question) {
    let questionTypeTemp = []
    question.map((resp, key) =>
      questionTypeTemp.push({
        id: resp.questionTypeId,
        name: resp.description
      })
    )

    return questionTypeTemp
  }

  answerOptions() {
    let answerGroupArray = [...this.props.answerGroupArray];
    let optionTemp = []

    answerGroupArray.map((resp, key) =>
      optionTemp.push(
        resp.answers
      )
    )
    this.setState({ options: optionTemp })
  }

  render () {
    const {
      isActive,
      answerGroupModal,
      questionItem,
      setActiveSettings,
      qSeqNo,
      qItem,
      qTypeState,
      qAnsItem,
      options
    } = this.state

    const {
      isMoreThanTargetNoOfMonths,
      toggle12Months,
      showOperationStatus,
      operationStatusModal,
      selectedOperationStatus,
      operationStatusId,
      operationStatusName,
      operationStatus,
      showQuestionType,
      questionTypeModal,
      qType,
      selectedQuestionType,
      showQuestionSetup,
      answerTypeModal,
      showAnswerType,
      selectedAnswerTemplate,
      qTypeTemplate,
      ansTemplate,
      toggleQuestionRequired,
      isQuestionRequired,
      targetNoOfMonths,
      setTargetNoOfMonths,
      questionsArray,
      setQuestionArray,
      questionTypeVal,
      setQuestionTypeVal,
      operationStat,
      getYumeshinEmployee,
      answerMethodArray,
      answerGroupArray,
      questionTypeArray
    } = this.props

    return (
      <div>
        <GenericCard>
          <div>
            {
              operationStatusModal &&
                <Modal
                  onClose = { () => showOperationStatus(false) } >
                  <h3 className = { 'text-align-center' }>Target Operation Status</h3>
                  <Line/>
                  {
                    operationStat &&
                    operationStat.map((resp, key) =>
                        <label
                          className = { `method-rows text-align-center cursor-pointer
                            ${ operationStatusName === resp.operationStatus && 'active' }` }
                          key = { key }
                          onClick = { () => selectedOperationStatus(resp.operationStatus, resp.operationStatusId) }>
                          <span className = { 'padding' }>{ resp.operationStatus }</span>
                          <Line/>
                        </label>
                    )
                  }
                </Modal>
            }
            <div className = { 'card-header' } >
              <div className = { 'grid-global' } >
                <div className = { 'table-name' } >
                  <label>
                    Question Settings
                  </label>
                </div>
              </div>
            </div>

            <div className = { 'card-body' } >
            {
              isActive ?
              <div>

                <div className = { 'grid-global-columns-x3-header' } >
                  <div>
                    <label className = { 'font-size-14px' }>Target No. of Months:</label>
                    <br/>
                    <label className = { 'font-size-12px label-color' }>
                      { targetNoOfMonths ? targetNoOfMonths : '0' }
                    </label>
                  </div>
                  <div>
                    <label className = { 'font-size-14px' }>Target Operation Status:</label>
                    <br/>
                    <label className = { 'font-size-12px label-color' }>
                      { operationStatusName ? operationStatusName : 'Status' }
                    </label>
                  </div>
                  <GenericButton
                    className = { 'tabs-button active' }
                    type = { 'button' }
                    onClick = { () => this.setActiveSettings(false)
                    }
                    text = { 'Edit' } />
                </div>
                <Line className = { 'margin-tb-8px' } />
                <div>
                  <GenericButton
                    className = { 'tabs-button active' }
                    type = { 'button' }
                    onClick = { () => this.appendChild(1, questionItem) }
                    text = { 'Create Question' } />
                </div>

                <div className = { 'margin-tb-8px' } />
                {
                  questionItem.map((resp, key) => {
                      return (
                        <div
                           key = { key } >
                           <div
                              className = { 'grid-global-question' } >
                              <div>
                                <GenericInput
                                  defaultValue = { '' }
                                  value = { resp.sequenceNo = key+ 1 }
                                  type = { 'text' }
                                  hint = { 'Seq No' }
                                  text = { 'Seq No' }
                                  disabled = { true }
                                  onChange = { (e) => {
                                     this.fun(e.target.value,
                                              resp.question,
                                              resp.isRequired,
                                              resp.questionTypeId,
                                              resp.answerGroupId,
                                              resp.qTypeFlag,
                                              key) } } />
                              </div>

                              <div>
                                <GenericInput
                                  defaultValue = { resp.question = resp.question ? resp.question : '' }
                                  value = { resp.question }
                                  type = { 'text' }
                                  hint = { 'Question' }
                                  text = { 'Question' }
                                  onChange = { (e) => {
                                     resp.question = e.target.value
                                     this.fun(resp.sequenceNo,
                                              resp.question,
                                              resp.isRequired,
                                              resp.questionTypeId,
                                              resp.answerGroupId,
                                              resp.qTypeFlag,
                                              key) } } />
                              </div>

                              <div>
                                <GenericInput
                                  type = { 'checkbox' }
                                  defaultValue = { resp.isRequired = resp.isRequired ? resp.isRequired : false }
                                  checked = { resp.isRequired ? true : false }
                                  text = { 'Required' }
                                  onClick = { (e) => resp.isRequired = e.target.checked }
                                  onChange = { (e) => {
                                    toggleQuestionRequired(isQuestionRequired)
                                     this.fun(resp.sequenceNo,
                                              resp.question,
                                              e.target.checked,
                                              resp.questionTypeId,
                                              resp.answerGroupId,
                                              resp.qTypeFlag,
                                              key) } } />
                              </div>

                              <div>
                                <GenericDropdown
                                  text = { 'Question Type' }
                                  value = { resp.questionTypeId }
                                  onChange = { (e) => {
                                    resp.questionTypeId = e.target.value
                                    this.fun(resp.sequenceNo,
                                            resp.question,
                                            resp.isRequired,
                                            resp.questionTypeId,
                                            resp.answerGroupId,
                                            resp.qTypeFlag,
                                            key) }
                                    }
                                  options = { this.questionTypeOptions(questionTypeArray) }
                                />
                              </div>

                              <div>
                                {
                                  resp.questionTypeId == 1 ?
                                  <GenericDropdown
                                    text = { 'Answer Method' }
                                    value = { resp.answerGroupId }
                                    onChange = { (e) => {
                                          resp.answerGroupId = e.target.value
                                          resp.qTypeFlag = true
                                          this.fun(resp.sequenceNo,
                                                  resp.question,
                                                  resp.isRequired,
                                                  resp.questionTypeId,
                                                  resp.answerGroupId,
                                                  resp.qTypeFlag,
                                                  key)
                                        }
                                      }
                                    options = { this.answerMethodOptions(answerMethodArray) }
                                    />
                                  :
                                  resp.questionTypeId == 2 &&
                                  <div>
                                    { resp.qTypeFlag = false }
                                  </div>
                                }
                              </div>

                            <div className = { 'padding-top-24' } >
                              <div className = { 'icon-edit' }
                                   onClick = { () => this.removeChild(key, questionItem) } />
                            </div>

                          </div>

                          {
                            <div>
                              {
                                resp.answerGroupId &&
                                resp.qTypeFlag &&
                                  <div className = { 'grid-global-answer' } >
                                    <GenericDropdown
                                      key = { key }
                                      text = { 'Answer Group' }
                                      value = { resp.ans }
                                      onChange = { (e) => {
                                            resp.ans = e.target.value
                                            this.setAnswers(options, resp.ans, resp)
                                            this.fun(resp.sequenceNo,
                                                    resp.question,
                                                    resp.isRequired,
                                                    resp.questionTypeId,
                                                    resp.answerGroupId,
                                                    resp.qTypeFlag,
                                                    key)
                                          }
                                      }
                                      options = { this.answerGroupTemplateOptions(answerGroupArray, resp.answerGroupId) }
                                    />

                                    <div
                                      className = { 'margin-top-24px' } >
                                      {
                                        resp.groupChoice &&
                                        resp.groupChoice
                                      }
                                    </div>

                                    <div />
                                  </div>
                              }

                            </div>
                          }
                        </div>
                      )
                  })
                }

                {
                  questionItem &&
                  <div>
                    <div className = { 'rtl' } >
                      <GenericButton
                        className = { 'tabs-button active' }
                        type = { 'button' }
                        onClick = { () =>
                            setQuestionArray(questionItem)
                          }
                        text = { 'Save' } />
                    </div>
                  </div>
                }

              </div>
              :
                <div className = { 'grid-global-columns-x4-header' } >
                  <GenericInput
                    type = { 'number' }
                    hint = { 'Target No. of Months' }
                    text = { 'Target No. of Months' }
                    value = { targetNoOfMonths }
                    onChange = { (e) => setTargetNoOfMonths(e.target.value) } />

                  <GenericInput
                    defaultValue = { '' }
                    onChange = { (e) => { operationStatusName = e.target.value } }
                    onClick = { () => showOperationStatus(true) }
                    value = { operationStatusName }
                    text = { 'Target Operation Status' } />

                  <GenericButton
                    className = { 'tabs-button active' }
                    type = { 'button' }
                    onClick = { () =>
                      {
                        this.setActiveSettings(true)
                        getYumeshinEmployee(operationStatusId, targetNoOfMonths)
                      }
                    }
                    text = { 'Submit' } />
                </div>
            }
          </div>

          </div>
        </GenericCard>
        <br/>
      </div>
    )
  }
}

export default RegularQuestionForm
