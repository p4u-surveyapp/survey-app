import React, { Component } from 'react'
import GenericInput from '../../../../../components/TextBox/GenericInput'
import GenericButton from '../../../../../components/Button/GenericButton'
import Modal from '../../../../../components/Modal/Modal'
import Line from '../../../../../components/Line/Line'
import '../styles/regular.css'

class RegularQuestionnaireSettingsForm extends Component {

    render () {
      const {
        title,
        titleErrorMessage,
        setTitle,
        completeMessage,
        completeMessageErrorMessage,
        setCompleteMessage,
        reminderRemainingDays,
        reminderRemainingDaysErrorMessage,
        setReminderRemainingDays,
        isMonthlyRecurring,
        toggleMonthlyRecurring,
        setStatus,
        activeStatus,
        startDay,
        startDayErrorMessage,
        setStartDay,
        endDay,
        endDayErrorMessage,
        setEndDay,
        emailTitle,
        emailTitleErrorMessage,
        setEmailTitle,
        sendBy,
        sendByErrorMessage,
        setSendBy,
        subject,
        subjectErrorMessage,
        setSubject,
        content,
        contentErrorMessage,
        setContent,
      } = this.props
      
      return (
        <div className = { 'questionnaire-div grid-global' }>
          <div>
            <div>
              <span className = { 'questionnaire-heading-label' }>
                Questionnaire Settings
              </span>
            </div>
            <Line className = { 'margin-tb-8px' } />
            <div className = { 'grid-global' }>
              <div>
                  <GenericInput
                    type = { 'text' }
                    hint = { 'Top Message' }
                    text = { 'Top Message' }
                    value = { title }
                    onChange = { (e) => setTitle(e.target.value) }
                    errorMessage = { titleErrorMessage }
                  />
              </div>

              <div>
                <GenericInput
                  type = { 'number' }
                  hint = { 'Reminder Remaining Days' }
                  text = { 'Reminder Remaining Days' }
                  value = { reminderRemainingDays }
                  onChange = { (e) => setReminderRemainingDays(e.target.value) }
                  errorMessage = { reminderRemainingDaysErrorMessage }
                />
              </div>
            </div>

            <div>
              <GenericInput
                type = { 'textarea' }
                hint = { 'Complete Message' }
                text = { 'Complete Message' }
                value = { completeMessage }
                onChange = { (e) => setCompleteMessage(e.target.value) }
                errorMessage = { completeMessageErrorMessage }
              />
            </div>

            <div className = { 'grid-global' } >
              <GenericInput
                type = { 'checkbox' }
                checked = { activeStatus ? true : false }
                onChange = { (e) => setStatus(e.target.checked) }
                hint = { 'Active Status' }
                text = { 'Active Status' }
              />

              <GenericInput
                type = { 'checkbox' }
                checked = { isMonthlyRecurring ? true : false }
                onChange = { (e) => toggleMonthlyRecurring(e.target.checked) }
                hint = { 'Monthly Recurring' }
                text = { 'Monthly Recurring' }
              />
            </div>

            <div className = { 'grid-global' }>
              <GenericInput
                type = { 'number' }
                hint = { 'Start Day' }
                text = { 'Start Day' }
                value = { startDay }
                onChange = { (e) => setStartDay(e.target.value) }
                errorMessage = { startDayErrorMessage }
              />
              <GenericInput
                type = { 'number' }
                hint = { 'End Day' }
                text = { 'End Day' }
                value = { endDay }
                onChange = { (e) => setEndDay(e.target.value) }
                errorMessage = { endDayErrorMessage }
              />
            </div>
          </div>
          <div>
            <div>
              <a className = { 'questionnaire-heading-label' }>
                Email Settings
              </a>
            </div>
            <Line className = { 'margin-tb-8px' } />
            <div className = { 'grid-global' } >

                <GenericInput
                  type = { 'text' }
                  hint = { 'Email Title' }
                  text = { 'Email Title' }
                  value = { emailTitle }
                  onChange = { (e) => setEmailTitle(e.target.value) }
                  errorMessage = { emailTitleErrorMessage }
                />
                <GenericInput
                  type = { 'text' }
                  hint = { 'Send By' }
                  text = { 'Send By' }
                  value = { sendBy }
                  onChange = { (e) => setSendBy(e.target.value) }
                  errorMessage = { sendByErrorMessage }
                />
            </div>
            <div>
                <GenericInput
                  type = { 'text' }
                  hint = { 'Subject' }
                  text = { 'Subject' }
                  value = { subject }
                  onChange = { (e) => setSubject(e.target.value) }
                  errorMessage = { subjectErrorMessage }
                />
            </div>

            <div>
              <GenericInput
                type = { 'textarea' }
                hint = { 'Content' }
                text = { 'Content' }
                value = { content }
                onChange = { (e) => setContent(e.target.value) }
                errorMessage = { contentErrorMessage }
              />
            </div>
          </div>
        </div>
    )
  }
}

export default RegularQuestionnaireSettingsForm
