import React, { Component } from 'react'
import GenericCard from '../../../../../components/Card/GenericCard'
import GenericInput from '../../../../../components/TextBox/GenericInput'
import GenericButton from '../../../../../components/Button/GenericButton'
import Modal from '../../../../../components/Modal/Modal'
import Line from '../../../../../components/Line/Line'
import Pagination from '../../../../../components/Pagination/Pagination'
import '../styles/regular.css'

class RegularEmployeeSettingsForm extends Component {
  constructor (props) {
    super (props)
    this.state = {
      cardsArray: this.props.employeesArray,
      pageItemsArray: this.props.employeesArray
    }
    this.onChangePage = this.onChangePage.bind(this)
  }

  onChangePage(pageOfItems) {
    this.setState({ pageItemsArray: pageOfItems })
  }

  render () {
    const {
      cardsArray,
      pageItemsArray
    } = this.state

    const {
      employeesArray
    } = this.props

    return (
      <div className = { 'questionnaire-div' }>
        <div>
          <div>
            <span className = { 'questionnaire-heading-label' }>
              Employee Settings
            </span>
          </div>
          <Line className = { 'margin-top-12px' } />
          <br/>

          <GenericCard className = {'card-body'}>
            <div className = { 'text-align-center' } >
              <label className = { 'font-size-16px' }>
                Available Recipient(s)
              </label>
            </div>
            <br/>
            {
              pageItemsArray.length != 0 ?
              pageItemsArray.map((resp, key) =>
                <div
                  key = { key }
                  className = { 'cursor-pointer' }
                  onClick = { () => {} }>
                  <div className = { 'grid-global' }>
                    <div>
                      <label className = { 'font-size-16px' } >
                        {resp.firstName} {resp.lastName}
                      </label>
                      <br/>
                      <label className = { 'font-size-10px label-color' } >
                        ({resp.firstNameKana} {resp.lastNameKana})
                      </label>
                    </div>

                    <div>
                      <label className = { 'font-size-14px label-color' } >
                        {resp.email}
                      </label>
                    </div>

                  </div>
                  <Line/>
                </div>
              )
              :
              <div className = { 'margin-bottom-8px text-align-center' }>
                <label>No record(s)</label>
              </div>
            }
            {
              pageItemsArray.length != 0 &&
              <Pagination items={cardsArray} onChangePage={this.onChangePage} />
            }
          </GenericCard>

          <br/>
        </div>
      </div>
    )
  }
}

export default RegularEmployeeSettingsForm
