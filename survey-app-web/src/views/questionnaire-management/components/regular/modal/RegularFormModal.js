import React, { Component } from 'react'
import GenericButton from '../../../../../components/Button/GenericButton'
import Modal from '../../../../../components/Modal/Modal'

import RegularQuestionnaireSettingsForm from '../form/RegularQuestionnaireSettingsForm'
import RegularQuestionForm from '../form/RegularQuestionForm'
import RegularEmployeeSettingsForm from '../form/RegularEmployeeSettingsForm'

import '../styles/regular.css'

class RegularFormModal extends Component {
  constructor (props) {
    super(props)

    this.state = {
      tabId: 1
    }
  }

  setNextTab(id) {
    this.setState({
      tabId: id >= 3 ? 3 : id + 1
    })
  }

  setPrevTab(id) {
    const {
      showAddForm
    } = this.props

    if (id === 1) {
      showAddForm(false)
    } else {
      this.setState({
        tabId: id <= 1 ? 1 : id - 1
      })
    }
  }

  checkRequiredFields(tabId,
    title,
    completeMessage,
    reminderRemainingDays,
    startDay,
    endDay,
    emailTitle,
    sendBy,
    subject,
    content,
    questionsArray
  ) {
    if(tabId === 1) {
      if(title &&
        completeMessage &&
        reminderRemainingDays &&
        startDay &&
        endDay &&
        emailTitle &&
        sendBy &&
        subject &&
        content
      ) {
        return true
      } else {
        return false
      }
    } else if(tabId === 2) {
      if(questionsArray.length != 0) {
        return true
      } else {
        return false
      }
    }
  }

  render () {
    const {
      tabId
    } = this.state

    const {
      showAddForm,
      isMonthlyRecurring,
      toggleMonthlyRecurring,
      setStatus,
      activeStatus,
      title,
      titleErrorMessage,
      completeMessage,
      completeMessageErrorMessage,
      reminderRemainingDays,
      reminderRemainingDaysErrorMessage,
      startDay,
      startDayErrorMessage,
      endDay,
      endDayErrorMessage,
      emailTitle,
      emailTitleErrorMessage,
      sendBy,
      sendByErrorMessage,
      subject,
      subjectErrorMessage,
      content,
      contentErrorMessage,
      setTitle,
      setCompleteMessage,
      description,
      setDescription,
      setReminderRemainingDays,
      setStartDay,
      setEndDay,
      setEmailTitle,
      setSendBy,
      setSubject,
      setContent,
      //2nd Tab Wizard
      isMoreThanTargetNoOfMonths,
      toggle12Months,
      showOperationStatus,
      operationStatusModal,
      operationStatusId,
      operationStatus,
      selectedOperationStatus,
      operationStatusName,
      showQuestionType,
      questionTypeModal,
      selectedQuestionType,
      showQuestionSetup,
      answerTypeModal,
      showAnswerType,
      selectedAnswerTemplate,
      qTypeTemplate,
      ansTemplate,
      toggleQuestionRequired,
      isQuestionRequired,
      targetNoOfMonths,
      setTargetNoOfMonths,
      questionsArray,
      employeesArray,
      setQuestionArray,
      getYumeshinEmployee,
      submitRegularQuestionnaire,
      operationStat,
      answerMethodArray,
      answerGroupArray,
      questionTypeArray
    } = this.props

    return (
        <Modal
          width = { 100 }
          fullHeight = { false } >
          {
            tabId === 1 ?
            <RegularQuestionnaireSettingsForm
              title = { title }
              titleErrorMessage = { titleErrorMessage }
              completeMessage = { completeMessage }
              completeMessageErrorMessage = { completeMessageErrorMessage }
              reminderRemainingDays = { reminderRemainingDays }
              reminderRemainingDaysErrorMessage = { reminderRemainingDaysErrorMessage }
              startDay = { startDay }
              startDayErrorMessage = { startDayErrorMessage }
              endDay = { endDay }
              endDayErrorMessage = { endDayErrorMessage }
              emailTitleErrorMessage = { emailTitleErrorMessage }
              sendBy = { sendBy }
              sendByErrorMessage = { sendByErrorMessage }
              subject = { subject }
              subjectErrorMessage = { subjectErrorMessage }
              content = { content }
              contentErrorMessage = { contentErrorMessage }
              setTitle = { (resp) => setTitle(resp) }
              setCompleteMessage = { (resp) => setCompleteMessage(resp) }
              setReminderRemainingDays = { (resp) => setReminderRemainingDays(resp) }
              toggleMonthlyRecurring = { (resp) => toggleMonthlyRecurring(resp) }
              isMonthlyRecurring = { isMonthlyRecurring }
              setStatus = { (resp) => setStatus(resp) }
              activeStatus = { activeStatus }
              setStartDay = { (resp) => setStartDay(resp) }
              setEndDay = { (resp) => setEndDay(resp) }
              emailTitle = { emailTitle }
              setEmailTitle = { (resp) => setEmailTitle(resp) }
              setSendBy = { (resp) => setSendBy(resp) }
              setSubject = { (resp) => setSubject(resp) }
              setContent = { (resp) => setContent(resp) }
            />
            :
              tabId === 2 ?
              <RegularQuestionForm
                toggle12Months = { (resp) => toggle12Months(resp) }
                isMoreThanTargetNoOfMonths = { isMoreThanTargetNoOfMonths }
                operationStatusId = { operationStatusId }
                showOperationStatus = { (resp) => showOperationStatus(resp) }
                operationStatusModal = { operationStatusModal }
                selectedOperationStatus = { (resp, respId) => selectedOperationStatus(resp, respId) }
                operationStatusName = { operationStatusName }
                showQuestionType = { (resp) => showQuestionType(resp) }
                questionTypeModal = { questionTypeModal }
                selectedQuestionType = { (resp) => selectedQuestionType(resp) }
                showQuestionSetup = { (resp) => showQuestionSetup(resp) }
                answerTypeModal = { answerTypeModal }
                showAnswerType = { (resp) => showAnswerType(resp) }
                selectedAnswerTemplate = { (resp) => selectedAnswerTemplate(resp) }
                toggleQuestionRequired = { (resp) => toggleQuestionRequired(resp) }
                isQuestionRequired = { isQuestionRequired }
                targetNoOfMonths = { targetNoOfMonths }
                qTypeTemplate = { qTypeTemplate }
                ansTemplate = { ansTemplate }
                setTargetNoOfMonths = { (resp) => setTargetNoOfMonths(resp) }
                questionsArray = { questionsArray }
                setQuestionArray = { (resp) => setQuestionArray(resp) }
                operationStat = { operationStat }
                getYumeshinEmployee = { (operationStatusId, targetNoOfMonths) => getYumeshinEmployee(operationStatusId, targetNoOfMonths) }
                answerMethodArray = { answerMethodArray }
                answerGroupArray = { answerGroupArray }
                questionTypeArray = { questionTypeArray }
              />
              :
              tabId === 3 &&
              <RegularEmployeeSettingsForm
                employeesArray = { employeesArray }
              />

          }
          <div className = { 'grid-global' } >
            <div className = { 'ltr' } >
                <GenericButton
                  text = { tabId === 1 ? 'Cancel' : 'Previous' }
                  onClick = { () => this.setPrevTab(tabId) }/>
            </div>
            <div className = { 'rtl' } >
            {
              tabId === 3 ?
              <GenericButton
                text = { 'Save' }
                onClick = { () => submitRegularQuestionnaire() }/>
                :
                this.checkRequiredFields(tabId,
                  title,
                  completeMessage,
                  reminderRemainingDays,
                  startDay,
                  endDay,
                  emailTitle,
                  sendBy,
                  subject,
                  content,
                  questionsArray
                ) &&
                <GenericButton
                  text = { 'Next' }
                  onClick = { () => this.setNextTab(tabId) }/>
            }
            </div>
          </div>
        </Modal>
    )
  }
}

export default RegularFormModal
