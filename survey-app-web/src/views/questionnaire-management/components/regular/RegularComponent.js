import React, { Component } from 'react'

import Table from './../../../../components/Table/Table'
import Pagination from './../../../../components/Pagination/Pagination'
import Line from '../../../../components/Line/Line'
import GenericButton from '../../../../components/Button/GenericButton'
import GenericInput from '../../../../components/TextBox/GenericInput'
import GenericCard from '../../../../components/Card/GenericCard'
import GenericLoader from '../../../../components/Loader/GenericLoader'
import Card from '../../../../components/Card/Card'

import './styles/regular.css'

import RegularFormModal from './modal/RegularFormModal'
import RegularQuestionnaireSettingsForm from './form/RegularQuestionnaireSettingsForm'
import RegularViewComponent from './RegularViewComponent'
import RegularViewQuestionComponent from './RegularViewQuestionComponent'

class RegularComponent extends Component {

  constructor (props) {
    super (props)
      this.state = {
        showForm: false,
        tabId: 1,
        pageNo: 1,
        pageSize: 10,
        pageOfItems: this.props.tableArray
      }

      this.onChangePage = this.onChangePage.bind(this)
      this.getPage = this.getPage.bind(this)

  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.pageOfItems !== prevProps.tableArray) {
      this.setState({ pageOfItems: this.props.tableArray })
    }
  }

  onChangePage(pageOfItems, currentPage) {
    this.setState({ pageOfItems })
  }

  getPage(lastId) {
    this.props.getRegularQuestionnaire(lastId, this.state.pageSize)
  }

  showAddForm(showForm) {
    this.props.resetFields()
    this.setState({ showForm })
    this.props.getAnswerTemplate()
  }

  setNextTab(id) {
    this.setState({
      tabId: id >= 2 ? 2 : id + 1
    })
  }

  setPrevTab(id) {
    if (id === 1) {

    } else {
      this.setState({
        tabId: id <= 1 ? 1 : id - 1
      })
    }
  }

  render () {
    const {
      tabId,
      showForm,
      pageNo,
      pageSize,
      pageOfItems
    } = this.state

    const {
      totalItemsRegular,
      lastIdRegular,
      operationStat,
      isEditMode,
      editRegularButton,
      questionnaireId,
      title,
      titleErrorMessage,
      completeMessage,
      completeMessageErrorMessage,
      reminderRemainingDays,
      reminderRemainingDaysErrorMessage,
      activeStatus,
      startDay,
      startDayErrorMessage,
      endDay,
      endDayErrorMessage,
      urlLink,
      emailTitle,
      emailTitleErrorMessage,
      sendBy,
      sendByErrorMessage,
      subject,
      subjectErrorMessage,
      content,
      contentErrorMessage,
      setTitle,
      setCompleteMessage,
      setReminderRemainingDays,
      setStatus,
      showTargetPerson,
      setStartDay,
      setEndDay,
      setEmailTitle,
      setSendBy,
      setSubject,
      setContent,
      operationStatusModal,
      operationStatusId,
      operationStatus,
      operationStatusName,
      showErrorModal,
      showError,
      errorMessage,
      targetNoOfMonths,
      tableArray,
      submitRegularQuestionnaire,
      updateRegularQuestionnaire,
      openTargetPersonModal,
      selectedTargetPersonValue,
      targetPersonValue,
      toggle12Months,
      showOperationStatus,
      showQuestionSetup,
      setTargetNoOfMonths,
      selectedOperationStatus,
      toggleQuestionRequired,
      showQuestionType,
      selectedQuestionType,
      showAnswerType,
      selectedAnswerTemplate,
      isMoreThanTargetNoOfMonths,
      setQuestionArray,
      questionTypeModal,
      answerTypeModal,
      qTypeTemplate,
      questionsArray,
      employeesArray,
      setQuestionnaireId,
      questionSetting,
      getYumeshinEmployee,
      toggleMonthlyRecurring,
      isMonthlyRecurring,
      ansTemplate,
      isQuestionRequired,
      resetFields,
      answerMethodArray,
      answerMethodName,
      answerMethodId,
      answerGroupArray,
      setRegularQuestionnaireActiveStatus,
      setRegularQuestionnaireDeactiveStatus,
      questionTypeArray,
      questionHeaderSettings,
      activeLoader,
      getRegularQuestionnaire,
      putRegularQuestionnaire
    } = this.props

    return (
      <div className = { 'grid-global'} >
        <div className = { 'regular-scroll-hidden' }>
          {
            showForm &&
              <RegularFormModal
                showAddForm = { () => this.showAddForm() }
                title = { title }
                titleErrorMessage = { titleErrorMessage }
                setTitle = { (resp) => setTitle(resp) }
                completeMessage = { completeMessage }
                completeMessageErrorMessage = { completeMessageErrorMessage }
                setCompleteMessage = { (resp) => setCompleteMessage(resp) }
                reminderRemainingDays = { reminderRemainingDays }
                reminderRemainingDaysErrorMessage = { reminderRemainingDaysErrorMessage }
                setReminderRemainingDays = { (resp) => setReminderRemainingDays(resp) }
                activeStatus = { activeStatus }
                startDay = { startDay }
                startDayErrorMessage = { startDayErrorMessage }
                setStartDay = { (resp) => setStartDay(resp) }
                endDay = { endDay }
                endDayErrorMessage = { endDayErrorMessage }
                setEndDay = { (resp) => setEndDay(resp) }
                emailTitle = { emailTitle }
                emailTitleErrorMessage = { emailTitleErrorMessage }
                setEmailTitle = { (resp) => setEmailTitle(resp) }
                sendBy = { sendBy }
                sendByErrorMessage = { sendByErrorMessage }
                setSendBy = { (resp) => setSendBy(resp) }
                subject = { subject }
                subjectErrorMessage = { subjectErrorMessage }
                setSubject = { (resp) => setSubject(resp) }
                content = { content }
                contentErrorMessage = { contentErrorMessage }
                setContent = { (resp) => setContent(resp) }
                toggle12Months = { (resp) => toggle12Months(resp) }
                isMoreThanTargetNoOfMonths = { isMoreThanTargetNoOfMonths }
                operationStatusId = { operationStatusId }
                operationStatusModal = { operationStatusModal }
                showOperationStatus = { (resp) => showOperationStatus(resp) }
                selectedOperationStatus = { (resp, respId) => selectedOperationStatus(resp, respId) }
                operationStatusName = { operationStatusName }
                toggleMonthlyRecurring = { (resp) => toggleMonthlyRecurring(resp) }
                isMonthlyRecurring = { isMonthlyRecurring }
                setStatus = { (resp) => setStatus(resp) }
                showQuestionType = { (resp) => showQuestionType(resp) }
                selectedQuestionType = { (resp) => selectedQuestionType(resp) }
                questionTypeModal = { questionTypeModal }
                showQuestionSetup = { (resp) => showQuestionSetup(resp) }
                answerTypeModal = { answerTypeModal }
                showAnswerType = { (resp) => showAnswerType(resp) }
                selectedAnswerTemplate = { (resp) => selectedAnswerTemplate(resp) }
                qTypeTemplate = { qTypeTemplate }
                ansTemplate = { ansTemplate }
                toggleQuestionRequired = { (resp) => toggleQuestionRequired(resp) }
                isQuestionRequired = { isQuestionRequired }
                setTargetNoOfMonths = { (resp) => setTargetNoOfMonths(resp) }
                targetNoOfMonths = { targetNoOfMonths }
                questionsArray = { questionsArray }
                employeesArray = { employeesArray }
                setQuestionArray = { (resp) => setQuestionArray(resp) }
                operationStat = { operationStat }
                getYumeshinEmployee = { (operationStatusId, targetNoOfMonths) => getYumeshinEmployee(operationStatusId, targetNoOfMonths) }
                submitRegularQuestionnaire = { () => submitRegularQuestionnaire() }
                answerMethodArray = { answerMethodArray }
                answerMethodName = { answerMethodName }
                answerMethodId = { answerMethodId }
                answerGroupArray = { answerGroupArray }
                questionTypeArray = { questionTypeArray }
              />
          }
          <div className = { 'grid-global' } >
            <div className = { 'table-name' } >
              <label className = { 'font-size-20px' }>
                Regular Questionnaire
              </label>
            </div>
            <div>
              <div className = { 'grid-global' } >
                <div></div>
                <div className = { 'rtl' } >
                      <GenericButton
                        className = { 'tabs-button active' }
                        type = { 'button' }
                        onClick = { () => this.showAddForm(true) }
                        text = { 'Create' } />
                </div>
              </div>
            </div>
          </div>
          <br/>
          <Line/>
          <div className = { 'regular-scroll' }>
            {
              pageOfItems ?
                pageOfItems.map((data, id) =>
                  <Card className = { `grid-card margin-bottom-8px ${ questionnaireId == data.questionnaireId && 'card-active' }` } key = { id }>
                    <div className = { 'cursor-pointer' } onClick = { () =>
                      {
                        data.questionnaireId != questionnaireId ?
                        editRegularButton(false, data.questionnaireId)
                        :
                        editRegularButton(false)
                      }
                    }>
                      <label className = { 'font-size-16px' }>{data.topMessage}</label>
                      <br/>
                      <label className = { 'font-size-11px label-color' }>Start day: {data.startDay}</label>
                      <br/>
                      <label className = { 'font-size-11px label-color' }>End day: {data.endDay}</label>
                    </div>
                    <div className = { 'text-align-end' }>
                      <span className = { 'action-button action-edit' }
                        onClick = { () => {
                          editRegularButton(true, data.questionnaireId)
                        }
                      }/>
                    </div>
                  </Card>
                )
            :
            <Card className = { 'grid-card margin-bottom-8px' }>
              No record(s)
            </Card>
          }
          <Pagination
            items = { tableArray }
            onClick = { this.getPage }
            onChangePage = { this.onChangePage }
            pageSize = { pageSize }
            totalItems = { totalItemsRegular }
            currentPage = { lastIdRegular }/>
          </div>
        </div>

        <div>
          <GenericCard className = { 'card-body' }>
            <div className = { 'grid-global margin-bottom-8px' } >
              <div className = { 'ltr' } >
              {
                tabId != 1 &&
                <div
                  className = { 'nav-left' }
                  onClick = { () => this.setPrevTab(tabId) } >
                </div>
              }
              </div>

              <div className = { 'rtl' } >
              {
                tabId != 2 &&
                <div
                  className = { 'nav-right' }
                  onClick = { () => this.setNextTab(tabId) } >
                </div>
              }
              </div>
            </div>
            {
              activeLoader ?
                <GenericLoader show = { activeLoader } />
              :
                tabId === 1 ?
                tableArray.length != 0 &&
                  tableArray &&
                  <RegularViewComponent
                    isEditMode = { isEditMode }
                    questionSetting = { questionSetting }
                    title = { title }
                    completeMessage = { completeMessage }
                    reminderRemainingDays = { reminderRemainingDays }
                    activeStatus = { activeStatus }
                    startDay = { startDay }
                    endDay = { endDay }
                    sendBy = { sendBy }
                    subject = { subject }
                    emailTitle = { emailTitle }
                    content = { content }
                    isMonthlyRecurring = { isMonthlyRecurring }
                    questionnaireId = { questionnaireId }
                    activeLoader = { activeLoader }
                    setCompleteMessage = { (resp) => setCompleteMessage(resp) }
                    setTitle = { (resp) => setTitle(resp) }
                    setReminderRemainingDays = { (resp) => setReminderRemainingDays(resp) }
                    setStartDay = { (resp) => setStartDay(resp) }
                    setEndDay = { (resp) => setEndDay(resp) }
                    setEmailTitle = { (resp) => setEmailTitle(resp) }
                    setSendBy = { (resp) => setSendBy(resp) }
                    setSubject = { (resp) => setSubject(resp) }
                    setContent = { (resp) => setContent(resp) }
                    toggleMonthlyRecurring = { (resp) => toggleMonthlyRecurring(resp) }
                    setStatus = { (resp) => setStatus(resp) }
                    updateRegularQuestionnaire = { () => updateRegularQuestionnaire() }
                    setRegularQuestionnaireActiveStatus = { (resp) => setRegularQuestionnaireActiveStatus(resp) }
                    setRegularQuestionnaireDeactiveStatus = { (resp) => setRegularQuestionnaireDeactiveStatus(resp) }
                    cancelButton = { () => editRegularButton(false) }
                  />
                  :
                  tabId === 2 &&
                    <RegularViewQuestionComponent
                      isEditMode = { isEditMode }
                      targetNoOfMonths = { targetNoOfMonths }
                      questionHeaderSettings = { questionHeaderSettings }
                      setTargetNoOfMonths = { (resp) => setTargetNoOfMonths(resp) }
                      questionSetting = { questionSetting }
                      operationStat = { operationStat }
                      operationStatusId = { operationStatusId }
                      operationStatusName = { operationStatusName }
                      operationStatusModal = { operationStatusModal }
                      showOperationStatus = { (resp) => showOperationStatus(resp) }
                      selectedOperationStatus = { (resp, respId) => selectedOperationStatus(resp, respId) }
                      putRegularQuestionnaire =  {
                        (operationStatusId, targetNoOfMonths) => putRegularQuestionnaire(operationStatusId, targetNoOfMonths)
                      }
                      questionTypeArray = { questionTypeArray }
                    />
            }

          </GenericCard>
        </div>

      </div>
    )
  }

}

export default RegularComponent
