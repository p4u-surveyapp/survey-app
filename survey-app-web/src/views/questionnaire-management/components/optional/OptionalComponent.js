import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Table from './../../../../components/Table/Table'
import Pagination from './../../../../components/Pagination/Pagination'
import Line from '../../../../components/Line/Line'
import GenericButton from '../../../../components/Button/GenericButton'
import GenericInput from '../../../../components/TextBox/GenericInput'
import GenericCard from '../../../../components/Card/GenericCard'
import GenericLoader from '../../../../components/Loader/GenericLoader'
import Card from '../../../../components/Card/Card'
import Modal from '../../../../components/Modal/Modal'

import './styles/optional.css'

import OptionalViewComponent from './OptionalViewComponent'
import OptionalFormModal from './modal/OptionalFormModal'
import OptionalViewQuestionComponent from './OptionalViewQuestionComponent'

class OptionalComponent extends Component {

  constructor (props) {
    super (props)
      this.state = {
        showForm: false,
        tabId: 1,
        pageNo: 1,
        pageSize: 10,
        targetPersonValue: '',
        optionalQuestionHeader: '',
        pageOfItems: this.props.tableArray
      }
      this.onChangePage = this.onChangePage.bind(this)
      this.getPage = this.getPage.bind(this)

  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.pageOfItems !== prevProps.tableArray) {
      this.setState({ pageOfItems: this.props.tableArray })
    }
  }

  onChangePage(pageOfItems, currentPage) {
    this.setState({ pageOfItems })
    // this.props.getOptionalQuestionnaires(currentPage, this.state.pageSize)
  }

  getPage(lastId) {
    this.props.getOptionalQuestionnaires(lastId, this.state.pageSize)
  }

  showAddForm(showForm) {
    this.setState({ showForm })
    this.props.resetFields()
    this.props.getAnswerTemplate()
  }

  setNextTab(id) {
    this.setState({
      tabId: id >= 3 ? 3 : id + 1
    })
  }

  setPrevTab(id) {
    if (id === 1) {

    } else {
      this.setState({
        tabId: id <= 1 ? 1 : id - 1
      })
    }
  }

  render () {
    const targetPerson =  [
      {
        id:1,
        name: 'SPECIFIED'
      },
      {
        id: 2,
        name: 'NOT SPECIFIED'
      }
    ]

    const {
      showForm,
      pageNo,
      tabId,
      pageSize,
      optionalQuestionHeader,
      pageOfItems
    } = this.state

    const {
      totalItems,
      lastId,
      isInvalid,
      isEditMode,
      editButton,
      questionnaireId,
      title,
      titleErrorMessage,
      completeMessage,
      completeMessageErrorMessage,
      reminderRemainingDays,
      reminderRemainingDaysErrorMessage,
      activeStatus,
      startDay,
      startDayErrorMessage,
      startMonth,
      startMonthErrorMessage,
      startYear,
      startYearErrorMessage,
      endDay,
      endDayErrorMessage,
      endMonth,
      endMonthErrorMessage,
      endYear,
      endYearErrorMessage,
      urlLink,
      emailTitle,
      emailTitleErrorMessage,
      sendBy,
      sendByErrorMessage,
      subject,
      subjectErrorMessage,
      content,
      contentErrorMessage,
      targetPersonValue,
      targetPersonValueErrorMessage,
      setTitle,
      setCompleteMessage,
      setReminderRemainingDays,
      setStatus,
      showTargetPerson,
      setStartDay,
      setStartMonth,
      setStartYear,
      setEndDay,
      setEndMonth,
      setEndYear,
      setEmailTitle,
      setSendBy,
      setSubject,
      setContent,
      operationStatusModal,
      operationStatusId,
      operationStatusName,
      showErrorModal,
      showError,
      errorMessage,
      targetNoOfMonths,
      tableArray,
      submitOptionalQuestionnaire,
      updateOptionalQuestionnaire,
      openTargetPersonModal,
      selectedTargetPersonValue,
      toggle12Months,
      showOperationStatus,
      showQuestionSetup,
      setTargetNoOfMonths,
      selectedOperationStatus,
      toggleQuestionRequired,
      showQuestionType,
      selectedQuestionType,
      showAnswerType,
      selectedAnswerTemplate,
      isMoreThanTargetNoOfMonths,
      setQuestionArray,
      questionTypeModal,
      answerTypeModal,
      qTypeTemplate,
      questionsArray,
      isQuestionRequired,
      getYumeshinEmployee,
      employeesArray,
      ansTemplate,
      showYumeshinLoaderModal,
      showYumeshinLoader,
      selectRecipient,
      selectedRecipient,
      unSelectRecipient,
      addCheckedRecipient,
      removeCheckedRecipient,
      setQuestionnaireActiveStatus,
      setQuestionnaireDeactiveStatus,
      resetFields,
      activeLoader
    } = this.props

    return (
      <div className = { 'grid-global' }>
        {
          showError &&
          <Modal>
            <div className = { 'text-align-center' }>
              <span>{ errorMessage }</span>
              <br/>
              <br/>
              <GenericButton
                text = { 'OK' }
                onClick = { () => showErrorModal(false, '') }
              />
            </div>
          </Modal>
        }
        <div className = { 'optional-scroll-hidden' }>
          {
            showForm &&
              <OptionalFormModal
                openTargetPersonModal = { openTargetPersonModal }
                showTargetPerson = { (resp) => showTargetPerson(resp) }
                targetPerson = { targetPerson }
                title = { title }
                titleErrorMessage = { titleErrorMessage }
                completeMessage = { completeMessage }
                completeMessageErrorMessage = { completeMessageErrorMessage }
                reminderRemainingDays = { reminderRemainingDays }
                reminderRemainingDaysErrorMessage = { reminderRemainingDaysErrorMessage }
                activeStatus = { activeStatus }
                startDay = { startDay }
                startDayErrorMessage = { startDayErrorMessage }
                startMonth = { startMonth }
                startMonthErrorMessage = { startMonthErrorMessage }
                startYear = { startYear }
                startYearErrorMessage = { startYearErrorMessage }
                endDay = { endDay }
                endDayErrorMessage = { endDayErrorMessage }
                endMonth = { endMonth }
                endMonthErrorMessage = { endMonthErrorMessage }
                endYear = { endYear }
                endYearErrorMessage = { endYearErrorMessage }
                urlLink = { urlLink }
                emailTitle = { emailTitle }
                emailTitleErrorMessage = { emailTitleErrorMessage }
                sendBy = { sendBy }
                sendByErrorMessage = { sendByErrorMessage }
                subject = { subject }
                subjectErrorMessage = { subjectErrorMessage }
                content = { content }
                contentErrorMessage = { contentErrorMessage }
                targetPersonValue = { targetPersonValue }
                targetPersonValueErrorMessage = { targetPersonValueErrorMessage }
                targetNoOfMonths = { targetNoOfMonths }
                optionalQuestionHeader = { optionalQuestionHeader }
                questionsArray = { questionsArray }
                employeesArray = { employeesArray }
                isMoreThanTargetNoOfMonths = { isMoreThanTargetNoOfMonths }
                operationStatusModal = { operationStatusModal }
                operationStatusId = { operationStatusId }
                operationStatusName = { operationStatusName }
                questionTypeModal = { questionTypeModal }
                qTypeTemplate = { qTypeTemplate }
                answerTypeModal = { answerTypeModal }
                ansTemplate = { ansTemplate }
                isQuestionRequired = { isQuestionRequired }
                showYumeshinLoader = { showYumeshinLoader }
                selectedRecipient = { selectedRecipient }
                addCheckedRecipient = { (resp) => addCheckedRecipient(resp) }
                removeCheckedRecipient = { (resp) => removeCheckedRecipient(resp) }
                showYumeshinLoaderModal = { (resp) => showYumeshinLoaderModal(resp) }
                setTitle = { (resp) => setTitle(resp) }
                setCompleteMessage = { (resp) => setCompleteMessage(resp) }
                setReminderRemainingDays = { (resp) => setReminderRemainingDays(resp) }
                setStatus = { (resp) => setStatus(resp) }
                setStartDay = { (resp) => setStartDay(resp) }
                setStartMonth = { (resp) => setStartMonth(resp) }
                setStartYear = { (resp) => setStartYear(resp) }
                setEndDay = { (resp) => setEndDay(resp) }
                setEndMonth = { (resp) => setEndMonth(resp) }
                setEndYear = { (resp) => setEndYear(resp) }
                setEmailTitle = { (resp) => setEmailTitle(resp) }
                setSendBy = { (resp) => setSendBy(resp) }
                setSubject = { (resp) => setSubject(resp) }
                setContent = { (resp) => setContent(resp) }
                selectedTargetPersonValue = { (resp) => selectedTargetPersonValue(resp) }
                showAddForm = { (resp) => this.showAddForm(resp) }
                toggle12Months = { (resp) => toggle12Months(resp) }
                showOperationStatus = { (resp) => showOperationStatus(resp) }
                setTargetNoOfMonths = { (resp) => setTargetNoOfMonths(resp) }
                selectedOperationStatus = { (resp, respId) => selectedOperationStatus(resp, respId) }
                toggleQuestionRequired = { (resp) => toggleQuestionRequired(resp) }
                showQuestionType = { (resp) => showQuestionType(resp) }
                selectedQuestionType = { (resp) => selectedQuestionType(resp) }
                showAnswerType = { (resp) => showAnswerType(resp) }
                selectedAnswerTemplate = { (resp) => selectedAnswerTemplate(resp) }
                setQuestionArray = { (resp) => setQuestionArray(resp) }
                submitOptionalQuestionnaire = { () =>
                {
                  submitOptionalQuestionnaire()
                  this.showAddForm(false)
                }
                }
                getYumeshinEmployee = { (operationStatusId, targetNoOfMonths) => getYumeshinEmployee(operationStatusId, targetNoOfMonths) }
                selectRecipient = { (resp, key) => selectRecipient(resp, key) }
                unSelectRecipient = { (resp, key) => unSelectRecipient(resp, key) }/>
          }
          <div className = { 'grid-global' } >
            <div className = { 'table-name' } >
              <label className = { 'font-size-20px' }>
                Optional Questionnaire
              </label>
            </div>
            <div className = { 'grid-global' } >
              <div></div>
              <div className = { 'rtl' } >
                <GenericButton
                  className = { 'tabs-button active' }
                  type = { 'button' }
                  onClick = { () => this.showAddForm(true) }
                  text = { 'Create' }
                />
              </div>
            </div>
          </div>
          <br/>
          <Line/>
          <div className = { 'optional-scroll' }>

              {
                pageOfItems.length != 0 ?
                pageOfItems.map((data, id) =>
                  <Card className = { `grid-card margin-bottom-8px ${ questionnaireId == data.questionnaireId && 'card-active' }` } key = { id }>
                    <div className = { 'cursor-pointer' } onClick = { () =>
                      {
                        data.questionnaireId != questionnaireId ?
                        editButton(false, data.questionnaireId)
                        :
                        editButton(false)
                      }
                    }>
                      <label className = { 'font-size-16px' }>{data.topMessage}</label>
                      <br/>
                      <label className = { 'font-size-11px label-color' }>Start date: {data.startDate}</label>
                      <br/>
                      <label className = { 'font-size-11px label-color' }>End date: {data.endDate}</label>
                    </div>
                    <div className = { 'text-align-end' }>
                      <span className = { 'action-button action-edit' }
                        onClick = { () => {
                          editButton(true, data.questionnaireId)
                        }
                      }/>
                    </div>
                  </Card>
                )
                :
                <div className = { 'margin-bottom-8px text-align-center' }>
                  <label>No record(s)</label>
                </div>
              }
              <Pagination
                items = { tableArray }
                onClick = { this.getPage }
                onChangePage = { this.onChangePage }
                pageSize = { pageSize }
                totalItems = { totalItems }
                currentPage = { lastId }/>
          </div>
        </div>
        <div>
          <GenericCard className = {'card-body'} >
            <div >
              <div className = { 'grid-global margin-bottom-8px' } >
                <div className = { 'ltr' } >
                {
                  tabId > 1 &&
                  <div className = { 'nav-left' } onClick = { () => this.setPrevTab(tabId) } ></div>
                }
                </div>
                <div className = { 'rtl' } >
                  <div className = { 'nav-right' } onClick = { () => this.setNextTab(tabId) } ></div>
                </div>
              </div>
            </div>
            <div>
              {
                activeLoader ?
                  <GenericLoader show = { activeLoader } />
                :
                tabId === 1 ?
                tableArray.length !=0 &&
                <OptionalViewComponent
                  questionnaireId = { questionnaireId }
                  isEditMode = { isEditMode }
                  openTargetPersonModal = { openTargetPersonModal }
                  targetPerson = { targetPerson }
                  targetPersonValue = { targetPersonValue }
                  title = { title }
                  reminderRemainingDays = { reminderRemainingDays }
                  activeStatus = { activeStatus }
                  startDay = { startDay }
                  startMonth = { startMonth }
                  startYear = { startYear }
                  endDay = { endDay }
                  endMonth = { endMonth }
                  endYear = { endYear }
                  emailTitle = { emailTitle }
                  sendBy = { sendBy }
                  subject = { subject }
                  content = { content }
                  setTitle = { (resp) => setTitle(resp) }
                  setReminderRemainingDays = { (resp) => setReminderRemainingDays(resp) }
                  setStatus = { (resp) => setStatus(resp) }
                  setEmailTitle = { (resp) => setEmailTitle(resp) }
                  setSendBy = { (resp) => setSendBy(resp) }
                  setSubject = { (resp) => setSubject(resp) }
                  setContent = { (resp) => setContent(resp) }
                  selectedTargetPersonValue = { (resp) => selectedTargetPersonValue(resp) }
                  showTargetPerson = { (resp) => showTargetPerson(resp) }
                  setStartDay = { (resp) => setStartDay(resp) }
                  setStartMonth = { (resp) => setStartMonth(resp) }
                  setStartYear = { (resp) => setStartYear(resp) }
                  setEndDay = { (resp) => setEndDay(resp) }
                  setEndMonth = { (resp) => setEndMonth(resp) }
                  setEndYear = { (resp) => setEndYear(resp) }
                  updateOptionalQuestionnaire = { () => updateOptionalQuestionnaire() }
                  setQuestionnaireActiveStatus = { (resp) => setQuestionnaireActiveStatus(resp) }
                  setQuestionnaireDeactiveStatus = { (resp) => setQuestionnaireDeactiveStatus(resp) }
                  cancelButton = { () => editButton(false) }
                  activeLoader = { activeLoader }
                />
                :
                tabId === 2 ?
                  <OptionalViewQuestionComponent />
                :
                tabId === 3 &&
                  alert('hey 3')
              }
            </div>
          </GenericCard>
        </div>
      </div>
    )
  }

}
OptionalComponent.propTypes = {
  titleErrorMessage : PropTypes.string
}

export default OptionalComponent
