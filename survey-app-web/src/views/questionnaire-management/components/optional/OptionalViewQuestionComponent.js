import React, { Component } from 'react'

import GenericCard from '../../../../components/Card/GenericCard'
import GenericInput from '../../../../components/TextBox/GenericInput'
import GenericButton from '../../../../components/Button/GenericButton'
import Modal from '../../../../components/Modal/Modal'
import Line from '../../../../components/Line/Line'

class OptionalViewQuestionComponent extends Component {
  constructor (props) {
    super (props)
    this.state = {

    }
  }

  render () {

    const {
    } = this.props

    return (
        <div className = { 'card-body font-size-12px' } >
          <div className = { 'grid-global' } >

            <label>
              Target No. of Months: 12
            </label>

            <label>
              Greater than No. of Months: Yes
            </label>
          </div>

          <label>
            Operation Status: Regular
          </label>


          <div className = { 'grid-global-question' } >
            <label>
              1.
            </label>

            <label>
              What is this? :)
            </label>

          </div>

        </div>
    )
  }
}

export default OptionalViewQuestionComponent
