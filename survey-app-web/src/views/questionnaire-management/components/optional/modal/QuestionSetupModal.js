import React, { Component } from 'react'
import Card from '../../../../../components/Card/Card'
import GenericInput from '../../../../../components/TextBox/GenericInput'
import GenericButton from '../../../../../components/Button/GenericButton'
import GenericLoader from '../../../../../components/Loader/GenericLoader'
import GenericDropdown from '../../../../../components/Dropdown/GenericDropdown'
import Modal from '../../../../../components/Modal/Modal'
import Line from '../../../../../components/Line/Line'

class QuestionSetupModal extends Component {

  constructor(props) {
    super(props)
      this.state = {
        questionItem: [],
        questionTypeState: ''
      }
      this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.setState({ questionTypeState: event.target.value })
  }

  appendChild(itemId, item) {
    let tempItem = [...item]
    tempItem.push({
      id: itemId + tempItem.length
    })
    this.setState({ questionItem: tempItem})
  }

  removeChild(id, item) {
    let tempItem = [...item]
    tempItem.map((val, key) => {
      if(key === id) {
        tempItem.splice(id, 1)
      }
    })
    this.setState({ questionItem: tempItem})
  }

  setQuestionType(qTypeTemplate) {
    let methodCount = 0
    if(qTypeTemplate) {
      if(qTypeTemplate === '2-OPTIONS') {
        methodCount = 2
      } else if (qTypeTemplate === '3-OPTIONS') {
        methodCount = 3
      } else if (qTypeTemplate === '5-OPTIONS') {
        methodCount = 5
      }
    }
    return methodCount
  }

  setQuestionChoices(choice) {
      let choicesArray = []
      for(let i = 1; i<= choice; i++) {
        choicesArray.push(
            <div className = { 'grid-global-question align-items-center'} >
              <div className = { 'font-size-12px' } >Item { i } </div>
              <GenericInput
                value = { '' }
                onChange = { (e) => e.target.value }
              />
            </div>
        )
      }
    return choicesArray
  }

  render () {
    const {
      questionItem,
      questionTypeState
    } = this.state

    const {
      toggle12Months,
      isMoreThanTargetNoOfMonths,
      operationStatusModal,
      showOperationStatus,
      selectedOperationStatus,
      operationStatusName,
      showQuestionType,
      questionTypeModal,
      selectedQuestionType,
      answerTypeModal,
      showAnswerType,
      selectedAnswerTemplate,
      qTypeTemplate,
      toggleQuestionRequired,
      isQuestionRequired,
      targetNoOfMonths,
      setTargetNoOfMonths,
      setQuestionArray
    } = this.props

    const answerTemplate = [
      {
        id: 1,
        groupName: 'FREE-INPUT',
        answers: [
          {
            id: 1,
            answerItem: 'Free Input'
          }
        ]
      },
      {
        id: 2,
        groupName: 'MULTIPLE-OPTIONS',
        answers: [
          {
            id: 1,
            answerItem: '2-OPTIONS'
          },
          {
            id: 2,
            answerItem: '3-OPTIONS'
          },
          {
            id: 3,
            answerItem: '5-OPTIONS'
          }
        ]
      }
    ]

    const questionType = [
      {
        id: 1,
        name: 'FREE-INPUT'
      },
      {
        id: 2,
        name: 'MULTIPLE-OPTIONS'
      }
    ]

    return (
      <div className = { 'questionnaire-div' }>
        <div>
          <Line className = { 'margin-top-12px' } />
          <br/>
          <div className = {'card-body grid-global text-align-center'} >
            <div>
              <label className = { 'font-size-14px' }>Target No. of Months:</label>
              <br/>
              <label className = { 'font-size-12px label-color' }>{ targetNoOfMonths ? targetNoOfMonths : '0' }</label>
            </div>
            <div>
              <label className = { 'font-size-14px' }>Target Operation Status:</label>
              <br/>
              <label className = { 'font-size-12px label-color' }>{ operationStatusName ? operationStatusName : 'N/A' }</label>
            </div>
          </div>
          <Line className = { 'margin-top-12px' } />
          <br/>
          <div>
          {
            questionItem.length < 20 &&
            <GenericButton
              className = { 'tabs-button active' }
              type = { 'button' }
              onClick = { () => this.appendChild(1, questionItem) }
              text = { questionItem.length > 0 ? 'Add Question' : 'Create Question' } />
          }
          </div>
          <br/>
          {
            questionTypeModal &&
            <Modal
              isDismisable = { true }
              onClose = { () => showQuestionType(false) } >
              <h3 className = { 'text-align-center' }>Question Type</h3>
              <Line/>
              {
                questionType.map((type, key) =>
                  <label
                    key = { key }
                    className = { `method-rows text-align-center cursor-pointer
                      ${ questionTypeState === type.name && 'active' }` }
                    onClick = { () => {
                        selectedQuestionType(type.id)
                        // resp.questionType = type.name
                        // resp.questionTypeId = type.id
                        // resp.answerItem = ''
                        this.setState({ questionTypeState: type.name })
                      }
                    }>
                    <span className = { 'padding' }>{ type.name }</span>
                    <Line/>
                  </label>
                )
              }
            </Modal>
          }
          {
            questionItem.map((resp, key) => {
                return (
                  <div
                     key = { key } >
                     <div
                        className = { 'grid-global-question' } >
                        <div>
                          <GenericInput
                            defaultValue = { '' }
                            value = { resp.sequenceNo }
                            type = { 'text' }
                            hint = { 'Seq No' }
                            text = { 'Seq No' }
                            onChange = { (e) => { resp.sequenceNo = e.target.value} } />
                        </div>

                        <div>
                          <GenericInput
                            defaultValue = { '' }
                            value = { resp.question }
                            type = { 'text' }
                            hint = { 'Question' }
                            text = { 'Question' }
                            onChange = { (e) => { resp.question = e.target.value} } />
                        </div>

                        <div>
                          <GenericInput
                            type = { 'checkbox' }
                            checked = { resp.isRequired ? true : false }
                            text = { 'Required' }
                            onChange = { () => toggleQuestionRequired(isQuestionRequired) }
                            onClick = { (e) => { resp.isRequired = e.target.checked } } />
                        </div>

                        <div>

                          <GenericDropdown
                            text = { 'Question Type' }
                            hint = { 'Select Question Type' }
                            value = { qTypeTemplate }
                            options = { questionType }
                            onChange = { (e) => {
                              console.log(e.target.value)
                              resp.questionTypeId = e.target.value
                              selectedQuestionType(e.target.value)
                              this.setState({ questionTypeState: e.target.value })
                            } }
                            />

                        </div>
                        {
                          answerTypeModal &&
                          <Modal
                            isDismisable = { true }
                            onClose = { () => showAnswerType(false) } >
                            <h3 className = { 'text-align-center' }>Choose Answer Template</h3>
                            <Line/>
                            {
                              answerTemplate.map((template, key) =>
                                template.id === parseInt(qTypeTemplate, 10) &&
                                  template.answers.map((ans, keyAns) =>
                                    <label
                                      key = { keyAns }
                                      className = { `method-rows text-align-center cursor-pointer
                                        ${ resp.answerItem === ans.answerItem && 'active' }` }
                                      onClick = { () => {
                                         selectedAnswerTemplate(ans.id)
                                         resp.answerItem = ans.answerItem
                                         resp.answerGroupId = ans.id
                                       }
                                     }>
                                     <span className = { 'padding' }>{ ans.answerItem }</span>
                                     <Line/>
                                     </label>
                                  )
                                )
                            }
                          </Modal>
                        }

                        <div>
                          <GenericInput
                            text = { 'Answer Template' }
                            hint = { 'Answer Template' }
                            value = { resp.answerItem }
                            onClick = { () => showAnswerType(true) }
                            />
                        </div>

                        <div className = { 'padding-top-24' } >
                          <div className = { 'icon-edit' }
                               onClick = { () => this.removeChild(key, questionItem) } />
                        </div>
                    </div>
                    {
                      qTypeTemplate &&
                      this.setQuestionChoices(this.setQuestionType(resp.answerItem))
                    }
                  </div>
                )
            })
          }
        </div>
          <div className = { 'rtl'} >
            <GenericButton
              className = { 'tabs-button active' }
              type = { 'button' }
              onClick = { () => {
                questionItem.length > 0 && setQuestionArray(questionItem)
              }}
              text = { questionItem.length > 0 ? 'Submit': 'Close' } />
          </div>
      </div>
      )
    }
}

export default QuestionSetupModal
