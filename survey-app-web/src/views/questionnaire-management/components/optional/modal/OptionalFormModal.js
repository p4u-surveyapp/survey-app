import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Card from '../../../../../components/Card/Card'
import GenericInput from '../../../../../components/TextBox/GenericInput'
import GenericButton from '../../../../../components/Button/GenericButton'
import Modal from '../../../../../components/Modal/Modal'

import QuestionnaireSettingsForm from '../form/QuestionnaireSettingsForm'
import OptionalQuestionSettingForm from '../form/OptionalQuestionSettingForm'
import OptionalEmployeeSetting from '../form/OptionalEmployeeSetting'

import '../styles/optional.css'

class OptionalFormModal extends Component {
  constructor (props) {
    super(props)

    this.state = {
      tabId: 1,
      fullHeight: false
    }
  }

  setNextTab(id) {
    const {
      targetPersonValue
    } = this.props

    let tabLimit;
    if(targetPersonValue === 'NOT SPECIFIED') {
      tabLimit = 2
    } else {
      tabLimit = 3
    }

    this.setState({
        tabId: id >= tabLimit ? tabLimit : id + 1
    })
  }

  setPrevTab(id) {
    const {
      showAddForm
    } = this.props

    if (id === 1) {
      showAddForm(false)
    } else {
      this.setState({
        tabId: id <= 1 ? 1 : id - 1
      })
    }
  }

  checkRequiredFields(tabId,
    title,
    completeMessage,
    reminderRemainingDays,
    startDay,
    startMonth,
    startYear,
    endDay,
    endMonth,
    endYear,
    emailTitle,
    sendBy,
    subject,
    content,
    questionsArray
  ) {
    if(tabId === 1) {
      if(title &&
        completeMessage &&
        reminderRemainingDays &&
        startDay &&
        startMonth &&
        startYear &&
        endDay &&
        endMonth &&
        endYear &&
        emailTitle &&
        sendBy &&
        subject &&
        content
      ) {
        return true
      } else {
        return false
      }
    } else if(tabId === 2) {
      if(questionsArray.length != 0) {
        return true
      } else {
        return false
      }
    }
  }

  render () {
    const {
      tabId,
      fullHeight,
    } = this.state

    const {
      showAddForm,
      targetPerson,
      showTargetPerson,
      openTargetPersonModal,
      isMoreThanTargetNoOfMonths,
      operationStatusModal,
      selectedTargetPersonValue,
      title,
      titleErrorMessage,
      completeMessage,
      completeMessageErrorMessage,
      reminderRemainingDays,
      reminderRemainingDaysErrorMessage,
      activeStatus,
      startDay,
      startDayErrorMessage,
      startMonth,
      startMonthErrorMessage,
      startYear,
      startYearErrorMessage,
      endDay,
      endDayErrorMessage,
      endMonth,
      endMonthErrorMessage,
      endYear,
      endYearErrorMessage,
      urlLink,
      emailTitle,
      emailTitleErrorMessage,
      sendBy,
      sendByErrorMessage,
      subject,
      subjectErrorMessage,
      content,
      contentErrorMessage,
      targetPersonValue,
      targetPersonValueErrorMessage,
      setTitle,
      setReminderRemainingDays,
      setCompleteMessage,
      setStatus,
      setStartDay,
      setStartMonth,
      setStartYear,
      setEndDay,
      setEndMonth,
      setEndYear,
      setUrlLink,
      setEmailTitle,
      setSendBy,
      setSubject,
      setContent,
      optionalQuestionHeader,
      questionArray,
      toggle12Months,
      showOperationStatus,
      selectedOperationStatus,
      operationStatusId,
      operationStatusName,
      showQuestionType,
      questionTypeModal,
      selectedQuestionType,
      qType,
      answerTypeModal,
      showAnswerType,
      selectedAnswerTemplate,
      toggleQuestionRequired,
      isQuestionRequired,
      targetNoOfMonths,
      qTypeTemplate,
      ansTemplate,
      setTargetNoOfMonths,
      questionsArray,
      setQuestionArray,
      submitOptionalQuestionnaire,
      getYumeshinEmployee,
      showYumeshinLoaderModal,
      showYumeshinLoader,
      employeesArray,
      selectRecipient,
      selectedRecipient,
      unSelectRecipient,
      addCheckedRecipient,
      removeCheckedRecipient
    } = this.props

    return (
      <Modal
        width = { 100 }
        fullHeight = { fullHeight }
        onClose = { () => showAddForm(false) }>
        {
          tabId === 1 ?
          <QuestionnaireSettingsForm
            showTargetPerson = { (resp) => showTargetPerson(resp) }
            openTargetPersonModal = { openTargetPersonModal }
            targetPerson = { targetPerson }
            title = { title }
            titleErrorMessage = { titleErrorMessage }
            completeMessage = { completeMessage }
            completeMessageErrorMessage = { completeMessageErrorMessage }
            reminderRemainingDays = { reminderRemainingDays }
            reminderRemainingDaysErrorMessage = { reminderRemainingDaysErrorMessage }
            activeStatus = { activeStatus }
            startDay = { startDay }
            startDayErrorMessage = { startDayErrorMessage }
            startMonth = { startMonth }
            startMonthErrorMessage = { startMonthErrorMessage }
            startYear = { startYear }
            startYearErrorMessage = { startYearErrorMessage }
            endDay = { endDay }
            endDayErrorMessage = { endDayErrorMessage }
            endMonth = { endMonth }
            endMonthErrorMessage = { endMonthErrorMessage }
            endYear = { endYear }
            endYearErrorMessage = { endYearErrorMessage }
            urlLink = { urlLink }
            emailTitle = { emailTitle }
            emailTitleErrorMessage = { emailTitleErrorMessage }
            sendBy = { sendBy }
            sendByErrorMessage = { sendByErrorMessage }
            subject = { subject }
            subjectErrorMessage = { subjectErrorMessage }
            content = { content }
            contentErrorMessage = { contentErrorMessage }
            targetPersonValue = { targetPersonValue }
            targetPersonValueErrorMessage = { targetPersonValueErrorMessage }
            optionalQuestionHeader = { optionalQuestionHeader }
            questionsArray = { questionsArray }
            setTitle = { (resp) => setTitle(resp) }
            setReminderRemainingDays = { (resp) => setReminderRemainingDays(resp) }
            setCompleteMessage = { (resp) => setCompleteMessage(resp) }
            setStatus = { (resp) => setStatus(resp) }
            setStartDay = { (resp) => setStartDay(resp) }
            setStartMonth = { (resp) => setStartMonth(resp) }
            setStartYear = { (resp) => setStartYear(resp) }
            setEndDay = { (resp) => setEndDay(resp) }
            setEndMonth = { (resp) => setEndMonth(resp) }
            setEndYear = { (resp) => setEndYear(resp) }
            setUrlLink = { (resp) => setUrlLink(resp) }
            setEmailTitle = { (resp) => setEmailTitle(resp) }
            setSendBy = { (resp) => setSendBy(resp) }
            setSubject = { (resp) => setSubject(resp) }
            setContent = { (resp) => setContent(resp) }
            selectedTargetPersonValue = { (resp) => selectedTargetPersonValue(resp) }
          />
          :
            tabId === 2 ?
            <OptionalQuestionSettingForm
              toggle12Months = { (resp) => toggle12Months(resp) }
              isMoreThanTargetNoOfMonths = { isMoreThanTargetNoOfMonths }
              showOperationStatus = { (resp) => showOperationStatus(resp) }
              operationStatusModal = { operationStatusModal }
              selectedOperationStatus = { (resp, respId) => selectedOperationStatus(resp, respId) }
              operationStatusId = { operationStatusId }
              operationStatusName = { operationStatusName }
              showQuestionType = { (resp) => showQuestionType(resp) }
              questionTypeModal = { questionTypeModal }
              selectedQuestionType = { (resp) => selectedQuestionType(resp) }
              qType = { qType }
              answerTypeModal = { answerTypeModal }
              showAnswerType = { (resp) => showAnswerType(resp) }
              selectedAnswerTemplate = { (resp) => selectedAnswerTemplate(resp) }
              toggleQuestionRequired = { (resp) => toggleQuestionRequired(resp) }
              isQuestionRequired = { isQuestionRequired }
              targetNoOfMonths = { targetNoOfMonths }
              qTypeTemplate = { qTypeTemplate }
              ansTemplate = { ansTemplate }
              setTargetNoOfMonths = { (resp) => setTargetNoOfMonths(resp) }
              questionsArray = { questionsArray }
              setQuestionArray = { (resp) => setQuestionArray(resp) }
            />
            :
            tabId === 3 &&
            <OptionalEmployeeSetting
              employeesArray = { employeesArray }
              selectedRecipient = { selectedRecipient }
              showYumeshinLoader = { showYumeshinLoader }
              targetNoOfMonths = { targetNoOfMonths }
              operationStatusId = { operationStatusId }
              operationStatusName = { operationStatusName }
              selectRecipient = { (resp, key) => selectRecipient(resp, key) }
              unSelectRecipient = { (resp, key) => unSelectRecipient(resp, key) }
              addCheckedRecipient = { (resp) => addCheckedRecipient(resp) }
              removeCheckedRecipient = { (resp) => removeCheckedRecipient(resp) }
              getYumeshinEmployee = { (operationStatusId, targetNoOfMonths) => getYumeshinEmployee(operationStatusId, targetNoOfMonths) }
              showYumeshinLoaderModal = { (resp) => showYumeshinLoaderModal(resp) }
              setTargetNoOfMonths = { (resp) => setTargetNoOfMonths(resp) }
              selectedOperationStatus = { (resp, respId) => selectedOperationStatus(resp, respId) }
            />
        }
        <div className = { 'grid-global' } >
          <div className = { 'ltr' } >
              <GenericButton
                text = { tabId === 1 ? 'Cancel' : 'Previous' }
                onClick = { () => {
                  this.setPrevTab(tabId)
                  this.setState({ fullHeight: false })
                }}/>
          </div>
          <div className = { 'rtl' } >
          {
            targetPersonValue === 'NOT SPECIFIED' ?
              tabId === 2 ?
                <GenericButton
                  text = { 'Save' }
                  onClick = { () => submitOptionalQuestionnaire()
                }/>
              :
                this.checkRequiredFields(tabId,
                  title,
                  completeMessage,
                  reminderRemainingDays,
                  startDay,
                  startMonth,
                  startYear,
                  endDay,
                  endMonth,
                  endYear,
                  emailTitle,
                  sendBy,
                  subject,
                  content,
                  questionsArray
                ) &&
                <GenericButton
                  text = { 'Next' }
                  onClick = { () => {
                    this.setNextTab(tabId)
                  }
                }/>
            :
              tabId === 3 ?
                <GenericButton
                  text = { 'Save' }
                  onClick = { () => submitOptionalQuestionnaire()
                }/>
              :
                this.checkRequiredFields(tabId,
                  title,
                  completeMessage,
                  reminderRemainingDays,
                  startDay,
                  startMonth,
                  startYear,
                  endDay,
                  endMonth,
                  endYear,
                  emailTitle,
                  sendBy,
                  subject,
                  content,
                  questionsArray
                ) &&
                <GenericButton
                  text = { 'Next' }
                  onClick = { () => {
                    this.setNextTab(tabId)
                    this.setState({ fullHeight: true })
                  }
                }/>
          }
          </div>
        </div>
      </Modal>
    )
  }
}

OptionalFormModal.propTypes = {
  titleErrorMessage : PropTypes.string
}

export default OptionalFormModal
