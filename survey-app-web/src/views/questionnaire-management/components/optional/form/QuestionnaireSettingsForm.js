import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Card from '../../../../../components/Card/Card'
import GenericInput from '../../../../../components/TextBox/GenericInput'
import GenericButton from '../../../../../components/Button/GenericButton'
import Modal from '../../../../../components/Modal/Modal'
import Line from '../../../../../components/Line/Line'
import '../styles/optional.css'

  class QuestionnaireSettingsForm extends Component {

    render () {
      const {
        targetPerson,
        showTargetPerson,
        openTargetPersonModal,
        selectedTargetPersonValue,
        title,
        titleErrorMessage,
        completeMessage,
        completeMessageErrorMessage,
        reminderRemainingDays,
        reminderRemainingDaysErrorMessage,
        activeStatus,
        startDay,
        startDayErrorMessage,
        startMonth,
        startMonthErrorMessage,
        startYear,
        startYearErrorMessage,
        endDay,
        endDayErrorMessage,
        endMonth,
        endMonthErrorMessage,
        endYear,
        endYearErrorMessage,
        urlLink,
        emailTitle,
        emailTitleErrorMessage,
        sendBy,
        sendByErrorMessage,
        subject,
        subjectErrorMessage,
        content,
        contentErrorMessage,
        targetPersonValue,
        targetPersonValueErrorMessage,
        setTitle,
        setReminderRemainingDays,
        setCompleteMessage,
        setStatus,
        setStartDay,
        setStartMonth,
        setStartYear,
        setEndDay,
        setEndMonth,
        setEndYear,
        setUrlLink,
        setEmailTitle,
        setSendBy,
        setSubject,
        setContent,
      } = this.props

      return (
      <div>
        <div className = { 'questionnaire-div grid-global' }>
          {
            openTargetPersonModal &&
            <Modal
              isDismissable = { true }
              onClose = { () => showTargetPerson(false) } >
              <h3 className = { 'text-align-center' }>Target Person</h3>
              {
                targetPerson.map((item, key) =>
                  <label
                    className = { `method-rows text-align-center cursor-pointer
                      ${ targetPersonValue === item.name && 'active' }` }
                    key = { key }
                    onClick = { () => selectedTargetPersonValue(item.name) }>
                    <span className = { 'padding' }>{ item.name }</span>
                    <Line/>
                  </label>
                )
              }
            </Modal>
          }
          <div>
            <div>
              <span className = { 'questionnaire-heading-label' }>
                Optional - Questionnaire Settings
              </span>
              <Line className = { 'margin-top-12px' } />
              <br/>
            </div>
            <div className = { 'grid-global' }>
              <div>
                  <GenericInput
                    type = { 'text' }
                    hint = { 'Top Message' }
                    text = { 'Top Message' }
                    value = { title }
                    onChange = { (e) => setTitle(e.target.value) }
                    errorMessage = { titleErrorMessage }
                  />
                  <GenericInput
                    type = { 'textarea' }
                    hint = { 'Complete Message' }
                    text = { 'Complete Message' }
                    value = { completeMessage }
                    onChange = { (e) => setCompleteMessage(e.target.value) }
                    errorMessage = { completeMessageErrorMessage }
                  />
              </div>
              <div>
                <GenericInput
                  type = { 'number' }
                  hint = { 'Reminder Remaining Days' }
                  text = { 'Reminder Remaining Days' }
                  value = { reminderRemainingDays }
                  onChange = { (e) => setReminderRemainingDays(e.target.value) }
                  errorMessage = { reminderRemainingDaysErrorMessage }
                />
                {// <GenericInput
                //   type = { 'checkbox' }
                //   hint = { 'Active Status' }
                //   text = { 'Active Status' }
                //   value = { activeStatus }
                //   onChange = { (e) => setStatus(e.target.checked) }
                // />
                }
                <GenericInput
                type = { 'text' }
                hint = { 'Target Person' }
                text = { 'Target Person' }
                readOnly = { true }
                value = { targetPersonValue }
                onClick = { () => showTargetPerson(true) }
                errorMessage = { targetPersonValueErrorMessage }
                />
              </div>
            </div>
            <div className = { 'grid-global' }>
              <div>
                <label className = { 'font-size-14px text-color' }>Start date</label>
                <div className = { 'grid-global-columns-x3' }>
                    <GenericInput
                      type = { 'number' }
                      hint = { 'dd' }
                      value = { startDay }
                      onChange = { (e) => setStartDay(e.target.value) }
                      errorMessage = { startDayErrorMessage }
                    />
                    <GenericInput
                      type = { 'number' }
                      hint = { 'mm' }
                      value = { startMonth }
                      onChange = { (e) => setStartMonth(e.target.value) }
                      errorMessage = { startMonthErrorMessage }
                    />
                    <GenericInput
                      type = { 'number' }
                      hint = { 'yyyy' }
                      value = { startYear }
                      onChange = { (e) => setStartYear(e.target.value) }
                      errorMessage = { startYearErrorMessage }
                    />
                </div>
              </div>
              <div>
                <label className = { 'font-size-14px text-color' }>End date</label>
                <div className = { 'grid-global-columns-x3' }>
                  <GenericInput
                    type = { 'number' }
                    hint = { 'dd' }
                    value = { endDay }
                    onChange = { (e) => setEndDay(e.target.value) }
                    errorMessage = { endDayErrorMessage }
                  />
                  <GenericInput
                    type = { 'number' }
                    hint = { 'mm' }
                    value = { endMonth }
                    onChange = { (e) => setEndMonth(e.target.value) }
                    errorMessage = { endMonthErrorMessage }
                  />
                  <GenericInput
                    type = { 'number' }
                    hint = { 'yyyy' }
                    value = { endYear }
                    onChange = { (e) => setEndYear(e.target.value) }
                    errorMessage = { endYearErrorMessage }
                  />
                </div>
              </div>
            </div>
          </div>

          <div>
            <div>
              <span className = { 'questionnaire-heading-label' }>
                Email Settings
              </span>
              <Line className = { 'margin-top-12px' } />
              <br/>
            </div>
            <div className = { 'grid-global' }>
              <GenericInput
                type = { 'text' }
                hint = { 'Title' }
                text = { 'Title' }
                value = { emailTitle }
                onChange = { (e) => setEmailTitle(e.target.value) }
                errorMessage = { emailTitleErrorMessage }
              />
              <GenericInput
                type = { 'text' }
                hint = { 'Send By' }
                text = { 'Send By' }
                value = { sendBy }
                onChange = { (e) => setSendBy(e.target.value) }
                errorMessage = { sendByErrorMessage }
              />
            </div>

            <div>
              <GenericInput
                type = { 'text' }
                hint = { 'Subject' }
                text = { 'Subject' }
                value = { subject }
                onChange = { (e) => setSubject(e.target.value) }
                errorMessage = { subjectErrorMessage }
              />
            </div>

            <div>
              <GenericInput
                type = { 'textarea' }
                hint = { 'Content' }
                text = { 'Content' }
                value = { content }
                onChange = { (e) => setContent(e.target.value) }
                errorMessage = { contentErrorMessage }
              />
            </div>
          </div>

        </div>
        {
          // <GenericInput
          //   type = { 'text' }
          //   hint = { 'Generated URL Link' }
          //   text = { 'Generated URL Link' }
          //   value = { urlLink }
          //   onChange = { (e) => setUrlLink(e.target.value) }
          //   />
        }
      </div>
    )
  }
}

QuestionnaireSettingsForm.propTypes = {
  titleErrorMessage : PropTypes.string
}

export default QuestionnaireSettingsForm
