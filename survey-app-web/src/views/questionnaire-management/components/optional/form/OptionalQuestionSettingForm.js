import React, { Component } from 'react'
import GenericCard from '../../../../../components/Card/GenericCard'
import GenericInput from '../../../../../components/TextBox/GenericInput'
import GenericButton from '../../../../../components/Button/GenericButton'
import Modal from '../../../../../components/Modal/Modal'
import Line from '../../../../../components/Line/Line'
import Table from '../../../../../components/Table/Table'
import '../styles/optional.css'

import QuestionSetupModal from '../modal/QuestionSetupModal'

class OptionalQuestionSettingForm extends Component {

    constructor (props) {
      super(props)
    }

    render () {
      const {
        isMoreThanTargetNoOfMonths,
        toggle12Months,
        showOperationStatus,
        operationStatusModal,
        selectedOperationStatus,
        operationStatusName,
        operationStatusId,
        showQuestionType,
        questionTypeModal,
        qType,
        selectedQuestionType,
        answerTypeModal,
        showAnswerType,
        selectedAnswerTemplate,
        qTypeTemplate,
        ansTemplate,
        toggleQuestionRequired,
        isQuestionRequired,
        targetNoOfMonths,
        setTargetNoOfMonths,
        setQuestionArray,
        questionTypeVal,
        setQuestionTypeVal,
        questionsArray
      } = this.props

      return (
        <div>
          <GenericCard>
            <div className = { 'card-header' } >
              <div className = { 'table-name' } >
                <label>Question Settings</label>
              </div>
              <QuestionSetupModal
                toggle12Months = { (resp) => toggle12Months(resp) }
                isMoreThanTargetNoOfMonths = { isMoreThanTargetNoOfMonths }
                showOperationStatus = { (resp) => showOperationStatus(resp) }
                operationStatusModal = { operationStatusModal }
                selectedOperationStatus = { (resp, respId) => selectedOperationStatus(resp, respId) }
                operationStatusName = { operationStatusName }
                showQuestionType = { (resp) => showQuestionType(resp) }
                questionTypeModal = { questionTypeModal }
                selectedQuestionType = { (resp) => selectedQuestionType(resp) }
                qType = { qType }
                answerTypeModal = { answerTypeModal }
                showAnswerType = { (resp) => showAnswerType(resp) }
                selectedAnswerTemplate = { (resp) => selectedAnswerTemplate(resp) }
                toggleQuestionRequired = { (resp) => toggleQuestionRequired(resp) }
                isQuestionRequired = { isQuestionRequired }
                targetNoOfMonths = { targetNoOfMonths }
                qTypeTemplate = { qTypeTemplate }
                ansTemplate = { ansTemplate }
                setTargetNoOfMonths = { (resp) => setTargetNoOfMonths(resp) }
                targetNoOfMonths = { targetNoOfMonths }
                questionsArray = { questionsArray }
                setQuestionArray = { (resp) => setQuestionArray(resp) }
              />
            </div>
          </GenericCard>
          <br/>
        </div>
      )
  }
}

export default OptionalQuestionSettingForm
