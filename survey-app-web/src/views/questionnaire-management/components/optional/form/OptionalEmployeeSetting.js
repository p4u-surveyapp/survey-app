import React, { Component } from 'react'
import Line from '../../../../../components/Line/Line'
import Table from '../../../../../components/Table/Table'
import Card from '../../../../../components/Card/Card'
import GenericPagination from '../../../../../components/Pagination/GenericPagination'
import GenericCard from '../../../../../components/Card/GenericCard'
import GenericInput from '../../../../../components/TextBox/GenericInput'
import GenericButton from '../../../../../components/Button/GenericButton'
import GenericLoader from '../../../../../components/Loader/GenericLoader'
import GenericDropdown from '../../../../../components/Dropdown/GenericDropdown'

import '../styles/optional.css'

class OptionalEmployeeSetting extends Component {
  constructor (props) {
    super (props)
    this.state = {
      pageItemsArray: this.props.employeesArray,
      selectedItemsArray: this.props.selectedRecipient,
      checkedItemsArray: [],
      unCheckedItemsArray: [],
      loaderRecipient: this.props.showYumeshinLoader
    }
    this.onChangePage = this.onChangePage.bind(this)
    this.onChangeItems = this.onChangeItems.bind(this)
  }

  componentDidUpdate(prevProps) {
    if (this.props.employeesArray !== prevProps.employeesArray) {
      this.setState({ pageItemsArray: this.props.employeesArray });
    } else if (this.props.showYumeshinLoader !== prevProps.showYumeshinLoader) {
      this.setState({ loaderRecipient: this.props.showYumeshinLoader})
    }
  }

  onChangePage(pageOfItems) {
    this.setState({ pageItemsArray: pageOfItems })
  }

  onChangeItems(item) {
    this.setState({ selectedItemsArray: item })
  }

  unCheckedRecipient(data, key) {
    const { unCheckedItemsArray } = this.state
    let tempArray = [...unCheckedItemsArray]

    if(tempArray.find( element => { return element.id === data.id })) {
      tempArray.map((resp, index) =>
        resp.id === data.id &&
        tempArray.splice(index, 1)
      )
    } else {
      tempArray.push({
        division: data.division,
        email: data.email,
        firstName: data.firstName,
        firstNameKana: data.firstNameKana,
        groupId: data.groupId,
        id: data.id,
        lastName: data.lastName,
        lastNameKana: data.lastNameKana,
        loginId: data.loginId,
        key: key
      })
    }
    this.setState({ unCheckedItemsArray: tempArray })
  }

  checkedRecipient(data, key) {
    const { checkedItemsArray } = this.state
    let tempArray = [...checkedItemsArray]

    if(tempArray.find( element => { return element.id === data.id })) {
      tempArray.map((resp, index) =>
        resp.id === data.id &&
        tempArray.splice(index, 1)
      )
    } else {
      tempArray.push({
        division: data.division,
        email: data.email,
        firstName: data.firstName,
        firstNameKana: data.firstNameKana,
        groupId: data.groupId,
        id: data.id,
        lastName: data.lastName,
        lastNameKana: data.lastNameKana,
        loginId: data.loginId,
        key: key
      })
    }
    this.setState({ checkedItemsArray: tempArray })
  }
  render () {
    const {
      pageItemsArray,
      selectedItemsArray,
      checkedItemsArray,
      unCheckedItemsArray,
      loaderRecipient
    } = this.state

    const {
      selectedRecipient,
      selectRecipient,
      employeesArray,
      unSelectRecipient,
      addCheckedRecipient,
      removeCheckedRecipient,
      getYumeshinEmployee,
      showYumeshinLoaderModal,
      showYumeshinLoader,
      targetNoOfMonths,
      setTargetNoOfMonths,
      operationStatusId,
      operationStatusName,
      selectedOperationStatus
    } = this.props

    const status = [
      {
        id: 1,
        name: 'REGULAR'
      },
      {
        id: 2,
        name: 'CONTRACTOR'
      },
      {
        id: 3,
        name: 'ALL'
      }
    ]

      return (
        <div className = { 'questionnaire-div' }>
          <div>
            <div>
              <span className = { 'questionnaire-heading-label' }>
                Optional - Employee Settings
              </span>
            </div>
            <Line className = { 'margin-top-12px' } />
            <br/>
            <div className = { 'grid-columns-x5' }>
              <div></div>
              <div className = { 'grid-global' }>
                <GenericInput
                  text = { 'Target Number of Months' }
                  type = { 'number' }
                  value = { targetNoOfMonths }
                  onChange = { (e) => {
                      setTargetNoOfMonths(e.target.value)
                      getYumeshinEmployee(operationStatusId, e.target.value)
                    }
                  }
                />
                <GenericDropdown
                  text = { 'Target Operator status' }
                  options = { status }
                  value = { operationStatusName }
                  onChange = { (e) => {
                      var index = e.target.selectedIndex
                      var optionElement = e.target.childNodes[index]
                      var id =  optionElement.getAttribute('id')
                      selectedOperationStatus(e.target.value, id)
                      getYumeshinEmployee(id, targetNoOfMonths)
                    }
                  }
                />
              </div>
              <div></div>
              <div></div>
            </div>
            <div className = { 'grid-columns-x5' }>
              <div></div>
              <GenericCard className = {'card-body'}>
                <div className = { 'grid-columns-x3' } >
                  <div>
                    <span
                      className = { 'action-button action-add' }
                      onClick = { () => {
                        checkedItemsArray.length !=0 &&
                        addCheckedRecipient(checkedItemsArray)
                        this.setState({ checkedItemsArray: [] })
                      }}
                    />
                    <span>{ checkedItemsArray.length } Selected</span>
                  </div>
                  <div>
                    <label className = { 'font-size-16px' }>
                    Available Recipient(s)
                    </label>
                  </div>
                  <div></div>
                </div>
                <br/>
                <Line/>
                <br/>
                {
                  pageItemsArray.length != 0 ?
                  pageItemsArray.map((resp, key) =>
                    <div className = { 'grid-list-checkbox' } key = { key }>
                      <div>
                        <input
                          defaultValue = { false }
                          className = { 'text-align-right' }
                          type = { 'checkbox' }
                          onClick = { () => this.checkedRecipient(resp, key) }
                          checked = { checkedItemsArray.find( element => { return element.id === resp.id }) ? true : false }
                        />
                      </div>
                      <div>
                        <label
                          className = { 'font-size-14px label-color cursor-pointer' }
                          onClick = { () => {
                            selectRecipient(resp, key)
                        } }>
                            <b>{resp.loginId}</b> - {resp.firstName} {resp.lastName}
                        </label>
                      </div>
                    </div>
                  )
                  :
                  loaderRecipient ?
                    <div className = { 'margin-bottom-8px text-align-center' }>
                      <GenericLoader show = { loaderRecipient }/>
                    </div>
                    :
                    <div className = { 'margin-bottom-8px text-align-center' }>
                      <label>No record(s)</label>
                    </div>
                }
                {
                  pageItemsArray.length != 0 &&
                  <GenericPagination
                    items = { employeesArray }
                    onChangePage = { (e) => this.onChangePage(e) }
                    pageSize={ 25 }
                  />
                }
              </GenericCard>
              <div></div>
              <GenericCard className = {'card-body'}>
              <div className = { 'grid-columns-x3' } >
                <div>
                  <span
                    className = { 'action-button action-remove' }
                    onClick = { () => {
                      unCheckedItemsArray.length !=0 &&
                      removeCheckedRecipient(unCheckedItemsArray)
                      unCheckedItemsArray.length !=0 &&
                      this.setState({ unCheckedItemsArray: [], selectedItemsArray: [] })
                    }}
                  />
                  <span>{ unCheckedItemsArray.length } Selected</span>
                </div>
                <div>
                  <label className = { 'font-size-16px' }>
                  Selected Recipient(s)
                  </label>
                </div>
                <div></div>
              </div>
              <br/>
              <Line/>
              <br/>
                {
                  selectedItemsArray.length != 0 ?
                  selectedItemsArray.map((resp, key) =>
                  <div className = { 'grid-list-checkbox' }>
                    <div>
                      <input
                        defaultValue = { false }
                        className = { 'text-align-right' }
                        type = { 'checkbox' }
                        onClick = { () => this.unCheckedRecipient(resp, key) }
                        checked = { unCheckedItemsArray.find( element => { return element.id === resp.id }) ? true : false }
                      />
                    </div>
                    <div>
                      <label className = { 'cursor-pointer font-size-14px label-color' }
                        onClick = { () =>
                          {
                            unSelectRecipient(resp, key)
                            selectedRecipient.length === 1 &&
                            this.setState({ selectedItemsArray: [] })
                          }
                        }><b>{resp.loginId}</b> - {resp.firstName} {resp.lastName}</label>
                    </div>
                  </div>
                  )
                  :
                  // loaderRecipient ?
                  //   <div className = { 'margin-bottom-8px text-align-center' }>
                  //     <GenericLoader show = { loaderRecipient }/>
                  //   </div>
                  //   :
                    <div className = { 'margin-bottom-8px text-align-center' }>
                      <label>No record(s)</label>
                    </div>
                }
                {
                  selectedRecipient.length != 0 &&
                  <GenericPagination
                    items = { selectedRecipient }
                    onChangePage = { this.onChangeItems }
                    pageSize = { 25 }
                  />

                }
              </GenericCard>
              <div></div>
            </div>

            <br/>
          </div>
        </div>
    )
  }
}

export default OptionalEmployeeSetting
