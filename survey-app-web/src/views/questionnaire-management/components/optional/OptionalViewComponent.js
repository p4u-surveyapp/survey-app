import React, { Component } from 'react'

import Table from './../../../../components/Table/Table'
import Pagination from './../../../../components/Pagination/Pagination'
import Line from '../../../../components/Line/Line'
import GenericButton from '../../../../components/Button/GenericButton'
import GenericInput from '../../../../components/TextBox/GenericInput'
import GenericCard from '../../../../components/Card/GenericCard'
import GenericLoader from '../../../../components/Loader/GenericLoader'
import Modal from '../../../../components/Modal/Modal'

import './styles/optional.css'

import OptionalFormModal from './modal/OptionalFormModal'

class OptionalViewComponent extends Component {

  constructor (props) {
    super (props)
  }

  render () {
    const targetPerson =  [
      {
        id:1,
        name: 'SPECIFIED'
      },
      {
        id: 2,
        name: 'NOT SPECIFIED'
      }
    ]

    const {
      isEditMode,
      showForm,
      pageNo,
      pageSize,
      editableFields,
      columns,
      openTargetPersonModal,
      isMoreThanTargetNoOfMonths,
      operationStatusModal,
      questionSetupModal,
      targetNoOfMonths,
      targetPersonValue,
      title,
      reminderRemainingDays,
      activeStatus,
      startDay,
      startMonth,
      startYear,
      endDay,
      endMonth,
      endYear,
      urlLink,
      emailTitle,
      sendBy,
      subject,
      content,
      setTitle,
      setReminderRemainingDays,
      setStatus,
      showTargetPerson,
      setStartDay,
      setStartMonth,
      setStartYear,
      setEndDay,
      setEndMonth,
      setEndYear,
      setEmailTitle,
      setSendBy,
      setSubject,
      setContent,
      selectedTargetPersonValue,
      updateOptionalQuestionnaire,
      setQuestionnaireActiveStatus,
      setQuestionnaireDeactiveStatus,
      questionnaireId,
      cancelButton,
      activeLoader
    } = this.props

    return (
        <div>
          {
            openTargetPersonModal &&
            <Modal
              isDismissable = { true }
              onClose = { () => showTargetPerson(false) } >
              <h3 className = { 'text-align-center' }>Target Person</h3>
              {
                targetPerson.map((item, key) =>
                  <label
                    className = { `method-rows text-align-center cursor-pointer
                      ${ targetPersonValue === item.name && 'active' }` }
                    key = { key }
                    onClick = { () => selectedTargetPersonValue(item.name) }>
                    <span className = { 'padding' }>{ item.name }</span>
                    <Line/>
                  </label>
                )
              }
            </Modal>
          }
          {
            isEditMode ?
            <div>
              <div>
                <div>
                  <span className = { 'questionnaire-heading-label' }>
                    Optional - Questionnaire Settings
                  </span>
                  <Line className = { 'margin-top-12px' } />
                  <br/>
                </div>
                <div className = { 'grid-global' }>
                  <div>
                      <GenericInput
                        type = { 'text' }
                        hint = { 'Top Message' }
                        text = { 'Top Message' }
                        value = { title }
                        onChange = { (e) => setTitle(e.target.value) }
                      />
                      <GenericInput
                        type = { 'text' }
                        hint = { 'Target Person' }
                        text = { 'Target Person' }
                        readOnly = { true }
                        value = { targetPersonValue }
                        onClick = { () => showTargetPerson(true) }
                      />
                  </div>
                  <div>
                    <GenericInput
                      type = { 'number' }
                      hint = { 'Reminder Remaining Days' }
                      text = { 'Reminder Remaining Days' }
                      value = { reminderRemainingDays }
                      onChange = { (e) => setReminderRemainingDays(e.target.value) }
                    />
                    {
                      // <GenericInput
                      //   type = { 'checkbox' }
                      //   hint = { 'Active Status' }
                      //   text = { 'Active Status' }
                      //   checked = { activeStatus ? true : false }
                      //   onChange = { (e) => setStatus(e.target.checked) }
                      // />
                    }
                  </div>
                </div>
                <div className = { 'grid-global' }>
                  <div>
                    <label className = { 'font-size-14px text-color' }>Start date</label>
                    <div className = { 'grid-global-columns-x3' }>
                        <GenericInput
                          type = { 'number' }
                          hint = { 'dd' }
                          value = { startDay }
                          onChange = { (e) => setStartDay(e.target.value) }
                        />
                        <GenericInput
                          type = { 'number' }
                          hint = { 'mm' }
                          value = { startMonth }
                          onChange = { (e) => setStartMonth(e.target.value) }
                        />
                        <GenericInput
                          type = { 'number' }
                          hint = { 'yyyy' }
                          value = { startYear }
                          onChange = { (e) => setStartYear(e.target.value) }
                        />
                    </div>
                  </div>
                  <div>
                    <label className = { 'font-size-14px text-color' }>End date</label>
                    <div className = { 'grid-global-columns-x3' }>
                      <GenericInput
                        type = { 'number' }
                        hint = { 'dd' }
                        value = { endDay }
                        onChange = { (e) => setEndDay(e.target.value) }
                      />
                      <GenericInput
                        type = { 'number' }
                        hint = { 'mm' }
                        value = { endMonth }
                        onChange = { (e) => setEndMonth(e.target.value) }
                      />
                      <GenericInput
                        type = { 'number' }
                        hint = { 'yyyy' }
                        value = { endYear }
                        onChange = { (e) => setEndYear(e.target.value) }
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <div>
                  <span className = { 'questionnaire-heading-label' }>
                    Email Settings
                  </span>
                  <Line className = { 'margin-top-12px' } />
                  <br/>
                </div>
                <div className = { 'grid-global' }>
                  <GenericInput
                    type = { 'text' }
                    hint = { 'Title' }
                    text = { 'Title' }
                    value = { emailTitle }
                    onChange = { (e) => setEmailTitle(e.target.value) }
                  />
                  <GenericInput
                    type = { 'text' }
                    hint = { 'Send By' }
                    text = { 'Send By' }
                    value = { sendBy }
                    onChange = { (e) => setSendBy(e.target.value) }
                  />
                </div>

                <div>
                  <GenericInput
                    type = { 'text' }
                    hint = { 'Subject' }
                    text = { 'Subject' }
                    value = { subject }
                    onChange = { (e) => setSubject(e.target.value) }
                  />
                </div>

                <div>
                  <GenericInput
                    type = { 'textarea' }
                    hint = { 'Content' }
                    text = { 'Content' }
                    value = { content }
                    onChange = { (e) => setContent(e.target.value) }
                  />
                </div>
              </div>
              <div className = { 'grid-global' }>
                <GenericButton
                  className = { 'tabs-button'}
                  text = { 'Cancel' }
                  onClick = { () => cancelButton() }/>
                <GenericButton
                  className = { 'tabs-button active'}
                  text = { 'Save' }
                  onClick = { () => updateOptionalQuestionnaire() }/>
              </div>
            </div>
            :
            <div>
              <div>
                <div className = { 'grid-global' }>
                  <span className = { 'questionnaire-heading-label' }>
                    Questionnaire Settings
                  </span>
                  <div className = { 'rtl'} >
                    {
                      activeLoader ?
                        <GenericLoader show = { activeLoader } />
                      :
                        activeStatus ?
                          <GenericButton
                            className = { 'tabs-button active'}
                            text = { 'Deactivate' }
                            onClick = { () => setQuestionnaireDeactiveStatus(questionnaireId) }/>
                        :
                          <GenericButton
                            className = { 'tabs-button'}
                            text = { 'Activate' }
                          onClick = { () => setQuestionnaireActiveStatus(questionnaireId) }/>
                    }
                  </div>
                </div>
                <Line className = { 'margin-top-12px' } />
                <br/>
              </div>
              <div className = { 'grid-global' }>
                  <label className = { 'font-size-16px margin-bottom-8px' }>{ title ? title : 'Top Message' }</label>
                  <br/>
                  <label className = { 'font-size-12px label-color margin-bottom-8px' }>Target Option: { targetPersonValue ? targetPersonValue : 'Target Person' }</label>
                  <label className = { 'font-size-12px label-color margin-bottom-8px' }>Reminder Remaining Days: { reminderRemainingDays ? reminderRemainingDays : '0' } Day(s)</label>
                  {// <label className = { 'font-size-12px label-color margin-bottom-8px' }>Active: { activeStatus ? 'Yes' : 'No' }</label>
                  }
                <label className = { 'font-size-12px label-color margin-bottom-8px' }>
                Start Date: {
                  startDay &&
                  startMonth &&
                  startYear ?
                  `${ startMonth }/${ startDay }/${ startYear }`
                  :
                  'mm/dd/yyyy'
                } - {
                  endDay &&
                  endMonth &&
                  endYear ?
                  `${ endMonth }/${ endDay }/${ endYear }`
                  :
                  'mm/dd/yyyy'
                }</label>
              </div>
              <br/>
              <div>
                <span className = { 'questionnaire-heading-label' }>
                  Email Settings
                </span>
                <Line className = { 'margin-top-12px' } />
                <br/>
              </div>
              <div className = { 'grid-global' }>
                <label className = { 'font-size-16px margin-bottom-8px' }>{ emailTitle ? emailTitle : 'Title' }</label>
                <br/>
                <label className = { 'font-size-12px label-color margin-bottom-8px' }>Sender: { sendBy ? sendBy : 'Send By' }</label>
                <label className = { 'font-size-12px label-color margin-bottom-8px' }>Subject: { subject ? subject : 'Subject' }</label>
                <label className = { 'font-size-12px label-color margin-bottom-8px' }>Content: { content ? content : 'Message...' }</label>
              </div>
            </div>
          }
        </div>
    )
  }

}

export default OptionalViewComponent
