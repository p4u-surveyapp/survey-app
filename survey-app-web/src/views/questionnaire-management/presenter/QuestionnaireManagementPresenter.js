import PostRegularQuestionnaireInteractor from '../../../domain/interactor/regular/PostRegularQuestionnaireInteractor'
import GetRegularQuestionnaireInteractor from '../../../domain/interactor/regular/GetRegularQuestionnaireInteractor'
import PutRegularQuestionnaireInteractor from '../../../domain/interactor/regular/PutRegularQuestionnaireInteractor'
import GetOperationStatusInteractor from '../../../domain/interactor/regular/GetOperationStatusInteractor'
import GetQuestionnaireSettingsInteractor from '../../../domain/interactor/regular/GetQuestionnaireSettingsInteractor'
import GetOptionalQuestionnairesInteractor from '../../../domain/interactor/optional/GetOptionalQuestionnairesInteractor'
import PostOptionalQuestionnaireInteractor from '../../../domain/interactor/optional/PostOptionalQuestionnaireInteractor'
import GetOptionalQuestionnaireByIdInteractor from '../../../domain/interactor/optional/GetOptionalQuestionnaireByIdInteractor'
import PutOptionalQuestionnaireInteractor from '../../../domain/interactor/optional/PutOptionalQuestionnaireInteractor'
import GetYumeshinEmployeeInteractor from '../../../domain/interactor/employee/GetYumeshinEmployeeInteractor'
import GetAnswerGroupTemplateInteractor from '../../../domain/interactor/answergroups/GetAnswerGroupTemplateInteractor'
import GetAnswerMethodsInteractor from '../../../domain/interactor/answergroups/GetAnswerMethodsInteractor'
import GetQuestionTypeInteractor from '../../../domain/interactor/questiontype/GetQuestionTypeInteractor'
import PutActiveQuestionnaireInteractor from '../../../domain/interactor/questionnairestatus/PutActiveQuestionnaireInteractor'
import PutDeactiveQuestionnaireInteractor from '../../../domain/interactor/questionnairestatus/PutDeactiveQuestionnaireInteractor'
import PutRegularQuestionnaireSettingsInteractor from '../../../domain/interactor/regular/PutRegularQuestionnaireSettingsInteractor'

import postRegularQuestionnaireParam from '../../../domain/param/regular/PostRegularQuestionnaireParam'
import putRegularQuestionnaireParam from '../../../domain/param/regular/PutRegularQuestionnaireParam'
import getRegularQuestionnaireParam from '../../../domain/param/regular/GetRegularQuestionnaireParam'
import getQuestionnaireSettingsParam from '../../../domain/param/regular/GetQuestionnaireSettingsParam'
import getOptionalQuestionnairesParam from '../../../domain/param/optional/GetOptionalQuestionnairesParam'
import postOptionalQuestionnaireParam from '../../../domain/param/optional/PostOptionalQuestionnaireParam'
import putOptionalQuestionnaireParam from '../../../domain/param/optional/PutOptionalQuestionnaireParam'
import employeeParam from '../../../domain/param/employee/GetYumeshinEmployeeParam'
import regularQuestionSettingsParam from '../../../domain/param/regular/PutRegularQuestionnaireSettingsParam'

export default class QuestionnaireManagementPresenter {
  constructor (container) {
    this.postRegularQuestionnaireInteractor = new PostRegularQuestionnaireInteractor(container.get('SurveyAppClient'))
    this.getRegularQuestionnaireInteractor = new GetRegularQuestionnaireInteractor(container.get('SurveyAppClient'))
    this.putRegularQuestionnaireInteractor = new PutRegularQuestionnaireInteractor(container.get('SurveyAppClient'))
    this.getOperationStatusInteractor = new GetOperationStatusInteractor(container.get('SurveyAppClient'))
    this.getQuestionnaireSettingsInteractor = new GetQuestionnaireSettingsInteractor(container.get('SurveyAppClient'))
    this.getAnswerGroupTemplateInteractor = new GetAnswerGroupTemplateInteractor(container.get('SurveyAppClient'))
    this.getAnswerMethodsInteractor = new GetAnswerMethodsInteractor(container.get('SurveyAppClient'))
    this.putActiveQuestionnaireInteractor = new PutActiveQuestionnaireInteractor(container.get('SurveyAppClient'))
    this.getQuestionTypeInteractor = new GetQuestionTypeInteractor(container.get('SurveyAppClient'))
    this.putDeactiveQuestionnaireInteractor = new PutDeactiveQuestionnaireInteractor(container.get('SurveyAppClient'))
    this.putRegularQuestionnaireSettingsInteractor = new PutRegularQuestionnaireSettingsInteractor(container.get('SurveyAppClient'))

    this.getOptionalQuestionnairesInteractor = new GetOptionalQuestionnairesInteractor(container.get('SurveyAppClient'))
    this.postOptionalQuestionnaireInteractor = new PostOptionalQuestionnaireInteractor(container.get('SurveyAppClient'))
    this.getOptionalQuestionnaireByIdInteractor = new GetOptionalQuestionnaireByIdInteractor(container.get('SurveyAppClient'))
    this.putOptionalQuestionnaireInteractor = new PutOptionalQuestionnaireInteractor(container.get('SurveyAppClient'))

    this.getYumeshinEmployeeInteractor = new GetYumeshinEmployeeInteractor(container.get('SurveyAppClient'))
  }

  setView (view) {
    this.view = view
  }

  getQuestionType() {
    this.getQuestionTypeInteractor.execute()
      .subscribe( data => {
        this.view.setQuestionType(data.data)
      },
      error => {

      })
  }

  getAnswerGroupTemplate() {
    this.getAnswerGroupTemplateInteractor.execute()
      .subscribe( data => {
        this.view.setAnswerGroupTemplate(data.data)
    },
    error => {

    })
  }

  getAnswerMethod() {
    this.getAnswerMethodsInteractor.execute()
      .subscribe( data => {
        this.view.setAnswerMethod(data.data.answerMethods)
      },
      error => {

      })
  }

  getRegularQuestionnaireById(questionnaireId) {
    this.view.showActiveLoader(true)
    this.getQuestionnaireSettingsInteractor.execute(getQuestionnaireSettingsParam(questionnaireId))
    .do(data =>
      data.data &&
      this.view.setData(data.data)
    )
    .subscribe(data => {
      data.data === null &&
      this.view.showErrorModal(true, data.message)
      this.view.showActiveLoader(false)
    },
    error => {
      console.error(error)
      this.view.showActiveLoader(false)
    })
  }

  getOperationStatus() {
    this.getOperationStatusInteractor.execute()
      .subscribe ( data => {
        this.view.setOperationStatus(data.data.operationStatuses)
      },
      error => {
        console.log(error)
      })
  }

  getRegularQuestionnaire(pageNo, pageSize) {
    this.getRegularQuestionnaireInteractor.execute(getRegularQuestionnaireParam(pageNo, pageSize))
    .subscribe( data => {
      this.view.setRegularQuestionnaire(data.data)
    },
    error => {
      console.error(error)
    })
  }

  submitRegularQuestionnaire(
    title,
    completeMessage,
    reminderRemainingDays,
    isMonthlyRecurring,
    isActive,
    startDay,
    endDay,
    content,
    sendBy,
    subject,
    emailTitle,
    targetNoOfMonths,
    isMoreThanTargetNoOfMonths,
    operationStatus,
    questionsArray,
    chunkedArray
    ) {

    this.view.showLoaderModal(true)
    this.postRegularQuestionnaireInteractor.execute(postRegularQuestionnaireParam(
      title,
      completeMessage,
      reminderRemainingDays,
      isMonthlyRecurring,
      isActive,
      startDay,
      endDay,
      content,
      sendBy,
      subject,
      emailTitle,
      targetNoOfMonths,
      isMoreThanTargetNoOfMonths,
      operationStatus,
      questionsArray,
      chunkedArray
    ))
    .subscribe(data => {
        data.message &&
        this.view.showErrorModal(true, data.message)
        this.getRegularQuestionnaire(0,10)
        this.view.showLoaderModal(false)
      })
    }

    updateRegularQuestionnaire (
      questionnaireId,
      title,
      completeMessage,
      reminderRemainingDays,
      isMonthlyRecurring,
      activeStatus,
      startDay,
      endDay,
      content,
      sendBy,
      subject,
      emailTitle
    ) {
      this.view.showLoaderModal(true)
      this.putRegularQuestionnaireInteractor.execute(putRegularQuestionnaireParam(
        questionnaireId,
        title,
        completeMessage,
        reminderRemainingDays,
        isMonthlyRecurring,
        activeStatus,
        startDay,
        endDay,
        content,
        sendBy,
        subject,
        emailTitle
      ))
      .subscribe(data => {
        data.message &&
        this.view.showErrorModal(true, data.message)
        this.getRegularQuestionnaire(0,10)
        this.view.showLoaderModal(false)
        this.view.editButton(false)
      })
    }

    getOptionalQuestionnaires(pageNo, pageSize) {
      this.getOptionalQuestionnairesInteractor.execute(getOptionalQuestionnairesParam(pageNo, pageSize))
        .subscribe(data => {
          data.data ?
            this.view.setOptionalQuestionnaires(data.data)
          :
          data.message &&
          this.view.showErrorModal(true, data.message)
          this.view.showLoaderModal(false)
        },
        error => {
          console.error(error)
        })
    }

    putRegularActiveQuestionnaire (questionnaireId) {
      this.view.showActiveLoader(true)
      this.putActiveQuestionnaireInteractor.execute(questionnaireId)
      .subscribe(data => {
        this.getRegularQuestionnaireById(questionnaireId)
        this.view.showActiveLoader(false)
      },
      error => {
        console.error(error)
        this.view.showActiveLoader(false)
      })
    }

    putRegularDeactiveQuestionnaire (questionnaireId) {
      this.view.showActiveLoader(true)
      this.putDeactiveQuestionnaireInteractor.execute(questionnaireId)
      .subscribe(data => {
        this.getRegularQuestionnaireById(questionnaireId)
        this.view.showActiveLoader(false)
      },
      error => {
        console.error(error)
        this.view.showActiveLoader(false)
      })
    }

    putRegularQuestionnaireSettings(
      operationStatusId,
      questionHeaderSettingId,
      questionnaireId,
      targetNoOfMonths)
    {
      this.putRegularQuestionnaireSettingsInteractor.execute(regularQuestionSettingsParam(
          operationStatusId,
          questionHeaderSettingId,
          questionnaireId,
          targetNoOfMonths)
        ).subscribe( data => {
          this.view.showErrorModal(true, data.message)
      },
      error => {
        console.log(error)
      })
    }

    submitOptionalQuestionnaire(
      content,
      sendBy,
      subject,
      emailTitle,
      activeStatus,
      endDate,
      reminderRemainingDays,
      startDate,
      targetPersonValue,
      title,
      completeMessage,
      questionsArray,
      selectedRecipient,
      lastId
    ){
      this.postOptionalQuestionnaireInteractor.execute(postOptionalQuestionnaireParam(
        content,
        sendBy,
        subject,
        emailTitle,
        activeStatus,
        endDate,
        reminderRemainingDays,
        startDate,
        targetPersonValue,
        title,
        completeMessage,
        questionsArray,
        selectedRecipient
      ))
      .subscribe(data => {
          data.message &&
          this.view.showErrorModal(true, data.message)
          this.getOptionalQuestionnaires(0,10)
          this.view.showLoaderModal(false)
        },
        error => {
          console.error(error)
          this.view.showLoaderModal(false)
        }
      )
    }

    getOptionalQuestionnaireById(id) {
      this.view.showActiveLoader(true)
      this.getOptionalQuestionnaireByIdInteractor.execute(id)
      .do(data =>
        data.data &&
        this.view.setData(data.data)
      )
      .subscribe(data => {
        data.data === null &&
        this.view.showErrorModal(true, data.message)
        this.view.showActiveLoader(false)
      },
      error => {
        console.error(error)
        this.view.showActiveLoader(false)
      })
    }

    updateOptionalQuestionnaire(
      title,
      reminderRemainingDays,
      activeStatus,
      startDate,
      endDate,
      urlLink,
      emailTitle,
      sendBy,
      subject,
      content,
      questionnaireId,
      targetPersonValue,
      completeMessage,
      lastId
    ) {
      this.putOptionalQuestionnaireInteractor.execute(putOptionalQuestionnaireParam(
        title,
        reminderRemainingDays,
        activeStatus,
        startDate,
        endDate,
        urlLink,
        emailTitle,
        sendBy,
        subject,
        content,
        questionnaireId,
        targetPersonValue,
        completeMessage
      ))
      .subscribe(data => {
          data.message &&
          this.view.showErrorModal(true, data.message)
          this.getOptionalQuestionnaires(0,10)
          this.view.showLoaderModal(false)
          this.view.editButton(false)
        },
        error => console.error(error))
    }

    getYumeshinEmployee(
      operationStatusId,
      targetNoOfMonths
    ){
      this.view.showYumeshinLoaderModal(true)
      this.getYumeshinEmployeeInteractor.execute(employeeParam(
        operationStatusId,
        targetNoOfMonths
      ))
      .subscribe(data => {
        if (data.data) {
          this.view.setYumeshinEmployee(data.data)
          this.view.setChunkedRecipients()
          this.view.showYumeshinLoaderModal(false)
        } else {
          data.message &&
          this.view.showErrorModal(true, data.message)
          this.view.showYumeshinLoaderModal(false)
        }
      },
      error => {
        console.error(error)
        this.view.showYumeshinLoaderModal(false)
      })
    }

    putActiveQuestionnaire (questionnaireId) {
      this.view.showActiveLoader(true)
      this.putActiveQuestionnaireInteractor.execute(questionnaireId)
      .subscribe(data => {
        this.getOptionalQuestionnaireById(questionnaireId)
        this.view.showActiveLoader(false)
      },
      error => {
        console.error(error)
        this.view.showActiveLoader(false)
      })
    }

    putDeactiveQuestionnaire (questionnaireId) {
      this.view.showActiveLoader(true)
      this.putDeactiveQuestionnaireInteractor.execute(questionnaireId)
      .subscribe(data => {
        this.getOptionalQuestionnaireById(questionnaireId)
        this.view.showActiveLoader(false)
      },
      error => {
        console.error(error)
        this.view.showActiveLoader(false)
      })
    }
}
