import React from 'react'
import { Switch, Route, withRouter } from 'react-router-dom'

import LoginFragment from './login/LoginFragment'
import NavigationFragment from './navigation/NavigationFragment'
import '../css/App.css'
import Presenter from './AppPresenter'

import BaseMVPView from './common/base/BaseMVPView'
import ConnectView from '../utils/ConnectView'

class App extends BaseMVPView {
  constructor (props) {
    super(props)
    this.state = {
      isLogin: true
    }
    this.checkIsLogin = this.checkIsLogin.bind(this)
  }

  componentWillReceiveProps (nextProps) {
    this.presenter.checkLogin()
  }

  componentWillMount () {
    this.presenter.checkLogin()
  }

  componentDidCatch (error, info) {
    console.log(info, error)
  }

  checkIsLogin(isLogin) {
    this.setState({ isLogin })
  }

  render () {
    const { isLogin } = this.state
    return (
      <div className = { 'App' }>
        <Switch>
          <Route path = '/' render = { props => {
              return <NavigationFragment  { ...props }/>
            // return (<LoginFragment { ...props }
            //           checkIsLogin = { (resp) => { this.checkIsLogin(resp) } }
            //         />)
            }} />
        </Switch>
      </div>
    );
  }
}

export default withRouter(ConnectView(App, Presenter))
