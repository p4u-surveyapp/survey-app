import PostSubmitAnswerInteractor from '../../../domain/interactor/user/PostSubmitAnswerInteractor'
import GetPendingQuestionnaireInteractor from '../../../domain/interactor/user/GetPendingQuestionnaireInteractor'
import GetQuestionnaireFormInteractor from '../../../domain/interactor/user/GetQuestionnaireFormInteractor'

import postAnswerParam from '../../../domain/param/user/PostAnswerParam'
import getQuestionnaireParam from '../../../domain/param/user/GetQuestionnaireParam'

export default class EmployeeUserPresenter {

  constructor (container) {
      this.postSubmitAnswerInteractor = new PostSubmitAnswerInteractor(container.get('SurveyAppClient'))
      this.getPendingQuestionnaireInteractor = new GetPendingQuestionnaireInteractor(container.get('SurveyAppClient'))
      this.getQuestionnaireFormInteractor = new GetQuestionnaireFormInteractor(container.get('SurveyAppClient'))
  }

  setView(view) {
    this.view = view
    this.view.showLoaderModal(false)
  }

  submitAnswers(resp, questionsHeaderSettings) {
    this.view.showLoaderModal(true)
    this.postSubmitAnswerInteractor
        .execute(postAnswerParam( questionsHeaderSettings.questionnaireId,
                                  questionsHeaderSettings.recipientId,
                                  resp))
        .subscribe(data => {
          this.view.showLoaderModal(false)
          this.setQuestionnaireFormModal(true)
        })
  }

  getPendingQuestionnaire() {
    this.getPendingQuestionnaireInteractor
        .execute()
        .subscribe(data => {
          this.view.setPendingQuestionnaire(data.data)
          this.view.showLoaderModal(false)
        })
  }

  getQuestionnaireForm(recipientId, action) {
    this.view.showLoaderModal(true)
    this.getQuestionnaireFormInteractor
        .execute(getQuestionnaireParam(recipientId, action))
        .subscribe(data => {
          this.view.showLoaderModal(false)
          action === 'FILL-UP' &&
            this.view.setQuestionnaireForm(data.data)
      })
  }
}
