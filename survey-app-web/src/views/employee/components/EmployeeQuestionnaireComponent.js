import React, { Component } from 'react'

import GenericCard from '../../../components/Card/GenericCard'
import GenericInput from '../../../components/TextBox/GenericInput'
import GenericButton from '../../../components/Button/GenericButton'
import { RadioGroup, RadioButton, ReversedRadioButton } from 'react-radio-buttons'

import './../styles/styles.css'

class EmployeeQuestionnaireComponent extends Component {

  constructor (props) {
    super (props)

    this.state = {
      isSubmitActive: true,
      selectedValue: '',
      answerArr: []
    }
  }

  setAnswer(answer, id, keyQuestion, templateId, recipientId) {
    let arrTemp = [...this.state.answerArr]
      if(arrTemp.length != 0) {
        arrTemp.map((val, key) =>
          {
            if(val.questionId === id) {
              arrTemp.splice(key, 1,
                  templateId === 1 ?
                  {
                    "questionId": id,
                    "answerChoiceId": answer,
                    "answer": '',
                    "recipientId": recipientId
                  }
                  :
                  templateId === 2 &&
                    {
                      "questionId": id,
                      "answerChoiceId": '',
                      "answer": answer,
                      "recipientId": recipientId
                    }
              )
            } else if(!arrTemp[keyQuestion]) {
              arrTemp.push(
              templateId === 1 ?
                {
                  "questionId": id,
                  "answerChoiceId": answer,
                  "answer": '',
                  "recipientId": recipientId
                }
                :
                templateId === 2 &&
                  {
                    "questionId": id,
                    "answerChoiceId": '',
                    "answer": answer,
                    "recipientId": recipientId
                  }
              )
            }
          }
        )
    } else {
      arrTemp.push(
      templateId === 1 ?
        {
          questionId: id,
          answerChoiceId: answer,
          answer: '',
          recipientId: recipientId
        }
        :
        templateId === 2 &&
          {
            questionId: id,
            answerChoiceId: '',
            answer: answer,
            recipientId: recipientId
          })
    }
    this.setState({ answerArr: arrTemp })
  }

  render () {
    const {
      isSubmitActive,
      selectedValue,
      answerArr
    } = this.state

    const {
      setAnswerArr,
      questionsHeaderSettings,
      questions
    } = this.props

    return (
      <GenericCard>
        <div className = { 'grid-global-questionnaire' } >
          <div>
          </div>

            <div className = { 'card-body' } >

              <div>
                <div className = { 'text-align-center'} >
                  <span className = { 'font-size-18px' } >
                    Questionnaire for engineers
                  </span>
                  <br/>
                  <span className = { 'font-size-14px' } >
                    This questionnaire is intended for newly hired engineers.
                  </span>
                </div>
                <br/>
                <br/>

                <div className = { 'grid-global font-size-12px' } >
                  <div>
                    <label>
                      Name: Yumeshin Employee 001
                    </label>
                    <br/>
                    <label>
                      Employee No: Y-R-0001
                    </label>
                  </div>
                  <div className = { 'rtl' } >
                    <label>
                      Questionnaire Type: Regular
                    </label>
                    <br/>
                    <span>
                    </span>
                  </div>
                </div>

                <br/>
                  <div>
                    {
                      questions.map((question, keyQuestion) =>
                      <div
                        key = { keyQuestion } >
                        <label
                          className = { 'margin-bottom-8px' } >
                          { question.sequenceNo }: { question.question }
                          <br/>
                          <br/>
                        </label>
                          {
                            <div>
                              {
                              question.questionTypeId === 1 ?
                              question.hasImage === 1 ?
                                <RadioGroup
                                  onChange = { (e) => this.setAnswer(e, question.questionId, keyQuestion, question.questionTypeId,
                                  questionsHeaderSettings.recipientId) }
                                  horizontal >
                                  {
                                    question.answers.map((answer, ansKey) =>
                                      <ReversedRadioButton
                                        key = { ansKey }
                                        pointColor = { '#ffb901' }
                                        rootColor = { '#94979e' }
                                        value = { String(answer.answerId) } >
                                        {
                                          <label className = { 'margin-tb-8px' } >
                                            { answer.answer }
                                            <br/>
                                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAAA+UlEQVRIidXUvUoDQRTF8d+KlY1aieBHG+zyAIK1hYVPYRP1OaxstVQDVlZp/KhUsLEUks5O7MUqwlrsCGoyyy5hCHthqjNz/nPmXoamV4aT1IA8JWAmpXmjAH0MYmI+4TpX9DLDxX89lmBY4/Z3vwxvqyS4wia+Kia4xwIW8TBGHzFfxRaOKgLykHgY0f6Yr+AFb1jGE25wjM8awBFAD+uKafgRzrCEbeyEZ/uoC8jCzfdxjY1xTQqbD/EcQDCPA8xFzqAYrbVg3irbGCCXIYUAe0QX7TJAv4J5rHrYU6TbjQEm/eze0cFrKkBpNeazmx5gFqepIc2uby59gVLzFnnDAAAAAElFTkSuQmCC" />
                                          </label>
                                        }
                                      </ReversedRadioButton>
                                    )
                                  }
                                </RadioGroup>
                                :
                                <RadioGroup
                                  onChange = { (e) => this.setAnswer(e, question.questionId, keyQuestion, question.questionTypeId, questionsHeaderSettings.recipientId) }
                                  vertical = { 'true' } >
                                  {
                                    question.answers.map((answer, ansKey) =>
                                      <ReversedRadioButton
                                        key = { ansKey }
                                        pointColor = { '#ffb901' }
                                        rootColor = { '#94979e' }
                                        value = { String(answer.answerId) } >
                                          { answer.answer }
                                      </ReversedRadioButton>
                                    )
                                  }
                                </RadioGroup>
                              : question.questionTypeId === 2  &&
                                <GenericInput
                                  type = { 'textarea' }
                                  onChange = { (e) => this.setAnswer(e.target.value, question.questionId, keyQuestion,
                                    question.questionTypeId, questionsHeaderSettings.recipientId) }
                                  />
                              }
                            </div>
                          }
                      <br/>
                      </div>
                      )
                    }
                  </div>
                  <div  className = { 'text-align-end' } >
                    <GenericButton
                      className = { 'tabs-button active' }
                      type = { 'button' }
                      onClick = { () =>
                        setAnswerArr(answerArr, questionsHeaderSettings)
                      }
                      text = { 'Submit' } />
                  </div>
              </div>
            </div>

        </div>
      </GenericCard>
    )
  }

}
export default EmployeeQuestionnaireComponent
