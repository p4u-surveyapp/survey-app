import React from 'react'
import PropTypes from 'prop-types'

import BaseMVPView from '../common/base/BaseMVPView'
import ConnectView from '../../utils/ConnectView'
import Presenter from './presenter/EmployeeUserPresenter'

import Card from '../../components/Card/Card'
import GenericButton from '../../components/Button/GenericButton'
import GenericInput from '../../components/TextBox/GenericInput'
import GenericLoader from '../../components/Loader/GenericLoader'
import Modal from '../../components/Modal/Modal'
import Line from '../../components/Line/Line'

import EmployeeQuestionnaireComponent from './components/EmployeeQuestionnaireComponent'
import EmployeeQuestionnaireViewComponent from './components/EmployeeQuestionnaireViewComponent'

import './styles/styles.css'

class EmployeeUserFragment extends BaseMVPView {

  constructor (props) {
    super (props)

    this.state = {
      questionnaireFormModal: true,
      showLoader: false,
      pendingQuestionnaire: [],
      recipientId: 0,
      action: '',
      questionsHeaderSettings: {},
      questions: []
    }

    this.showLoaderModal = this.showLoaderModal.bind(this)
  }

  componentDidMount () {
    this.presenter.getPendingQuestionnaire()
  }

  setQuestionnaireForm(resp) {
    let questionTemp = []

    resp.questions.map((val, key) =>
      questionTemp.push(val)
    )

    this.setState({ questions: questionTemp })
    this.setState({ questionsHeaderSettings: resp })
  }

  showQuestionnaireForm(resp, recipientId, action) {
    if(recipientId != 0) {
      this.presenter.getQuestionnaireForm(recipientId, action)
      this.setState({ questionnaireFormModal: resp})
      this.setState({ action: action})
    } else {
      this.setState({ questionnaireFormModal: resp})
    }
  }

  setPendingQuestionnaire(resp) {
    this.setState({ pendingQuestionnaire: resp })
  }

  setAnswerArr(resp, qhs) {
    this.setState({ questionnaireFormModal: false })
    this.presenter.submitAnswers(resp, qhs)
  }

  showLoaderModal(showLoader) {
    this.setState({ showLoader })
  }

  setQuestionnaireFormModal(resp) {
    this.setState({ questionnaireFormModal: resp })
    this.presenter.getPendingQuestionnaire()
  }

  render () {
    const FILL_UP = 'FILL-UP'
    const VIEW_ANSWERED = 'VIEW-ANSWERED'

    const {
      questionnaireFormModal,
      answerArr,
      showLoader,
      pendingQuestionnaire,
      recipientId,
      action,
      questionsHeaderSettings,
      questions
    } = this.state

    const {
      setAnswerArr,
      setSelectedNavigation
    } = this.props

    return (
      <div>
        {
          showLoader ?
          <Modal width = { 20 } >
              <GenericLoader
                message = { 'Loading Questionnaire' }
                show = { showLoader } />
          </Modal>
          :
          <div>
            <div className = { 'grid-global margin-tb-4px' } >
              <label className = { 'font-size-20px' } >
              Pending Questionnaire
              </label>
            </div>
            <Line/>
            <br/>
            {
              questionnaireFormModal && action === 'FILL_UP' &&
                <div className = { 'margin-tb-8px rtl' } >
                  <GenericButton
                    className = { 'tabs-button active' }
                    type = { 'button' }
                    onClick = { () => this.showQuestionnaireForm(true, 0) }
                    text = { 'Close' } />
                </div>
            }
            {
            questionnaireFormModal ?
            pendingQuestionnaire &&
                pendingQuestionnaire.map((resp, key) =>
                  <Card
                    key = { key }
                    className = { 'margin-bottom-8px' } >

                    <div className = { 'grid-global-4fr-3fr-2fr' } >
                      <div>
                        <label className = { 'font-size-18px' }>
                          <b>
                           { resp.topMessage }
                          </b>
                        </label>
                        <br/>
                        <label className = { 'font-size-12px' } >
                          Type: { resp.typeDesc }
                        </label>
                      </div>

                      <div>
                        <label className = { 'font-size-14px' } >
                          End Date
                        </label>
                        <br/>
                        <label className = { 'font-size-12px' } >
                          { resp.endDate }
                        </label>
                      </div>

                      <div>
                        <div className = { 'rtl' } >
                          <span
                            onClick = { () => this.showQuestionnaireForm(false, resp.recipientId, FILL_UP) }
                            className = { 'action-button icon-update' }
                          />
                        </div>
                      </div>

                    </div>
                  </Card>
                )
              :
              action === 'FILL_UP' &&
                <div>
                 <EmployeeQuestionnaireComponent
                  questionsHeaderSettings = { questionsHeaderSettings }
                  questions = { questions }
                  setAnswerArr = { (resp, qhs) => this.setAnswerArr(resp, qhs) } />
                </div>
            }
          </div>
        }
      </div>
    )
  }
}

export default ConnectView(EmployeeUserFragment, Presenter)
