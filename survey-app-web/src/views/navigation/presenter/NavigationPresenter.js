import GetAuthenticateUserInteractor from '../../../domain/interactor/user/GetAuthenticateUserInteractor'
import GetUserInteractor from '../../../domain/interactor/user/GetUserInteractor'
import LogoutInteractor from '../../../domain/interactor/login/LogoutInteractor'

export default class NavigationPresenter {
  constructor (container) {
    this.getAuthenticateUserInteractor = new GetAuthenticateUserInteractor(container.get('SurveyAppClient'))
    this.getUserInteractor = new GetUserInteractor(container.get('SurveyAppClient'))
    this.logoutInteractor = new LogoutInteractor(container.get('SurveyAppClient'))
  }

  setView (view) {
    this.view = view
  }

  getAuthenticateUser (token) {
    this.getAuthenticateUserInteractor.execute(token)
    .subscribe(
      data => {
        data.status === 1801 ?
          window.location.href = data.data
        :
        data.message === 'Success' &&
        this.getUserInfo()
      },
      error => console.error(error)
    )
  }

  getUserInfo () {
   this.getUserInteractor.execute()
   .subscribe(
     data => {
       data.data ?
         this.view.setUserInfo(data.data)
       :
        data.message &&
        this.view.showErrorModal(true, data.message)
     },
     error => {
       console.error(error)
     }
   )
 }

 logout () {
   this.logoutInteractor.execute()
    .subscribe(
      data => {
        data.message &&
        this.view.showErrorModal(true, data.message)
      },
      error => {
        console.error(error);
      }
    )
 }
}
