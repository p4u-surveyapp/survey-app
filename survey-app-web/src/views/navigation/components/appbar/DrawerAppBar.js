import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import AppBar from '../../../../components/AppBar/AppBar'

import './styles/appbar.css'

class DrawerAppBar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      showDropdown: false
    }
  }

  showDropdownFunc(value) {
    this.setState({ showDropdown: value ? false : true })
  }

  render () {
    const {
      history,
      selected
    } = this.props

    const {
      showDropdown
    } = this.state

    const navList = [
      {
        id: 1,
        title: 'Avatar',
        path: '/profile'
      }
    ]
    return (
      <div className = { 'nav-grid-1' }>
        <div></div>
        <div>
          <ul className = { 'topnav' }>
            {
              navList.map((resp, key) =>
                <div className = { 'dropdown' }>
                    <span className = { 'dropbtn' }
                          alt = { resp.title }
                          onClick = { () => this.showDropdownFunc(showDropdown) }
                          />
                   <div className = { `dropdown-content ${showDropdown ? 'dropdown-content-show' : ''}` }>
                     <Link
                       className = { resp.id === selected ? 'active' : '' }
                       to = { resp.path }>
                       Profile
                     </Link>
                   </div>
               </div>

              )
            }

          </ul>
        </div>
        <div>
        </div>
      </div>
    )
  }
}

DrawerAppBar.propTypes = {

}
export default DrawerAppBar
