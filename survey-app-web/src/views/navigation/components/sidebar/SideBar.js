import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './styles/sidebar.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import Line from '../../../../components/Line/Line'

class SideBar extends Component {
  constructor (props) {
    super(props)
  }


  render () {
    const {
      selected,
      showSideBar,
      logout,
      groupId
    } = this.props

    const sidebarList = [
      {
        id: 1,
        title: 'Home',
        path: '/home',
        icon: 'icon-home'
      },
      {
        id: 2,
        title: 'Settings',
        path: '/settings',
        icon: 'icon-settings'
      },
      {
        id: 3,
        title: 'Questionnaire Management',
        path: '/questionnaire',
        icon: 'icon-question-management'
      },
      {
        id: 4,
        title: 'User Situations Inquiries',
        path: '/inquires',
        icon: 'icon-inqueries'
      },
      {
        id: 5,
        title: 'Answered Questionnaire History',
        path: '/user_questionnaire',
        icon: 'icon-user-questionnaire'
      }
    ]

  const style = {
    show: {
      display : showSideBar ? 'none' : 'block'
    }
  }

  return (

      <div>
        <ul className="sidenav">
          {
            groupId === 5 ?
            sidebarList.map((resp, key) =>
              resp.id === 1 ?
                <li key = { key }
                    className = { resp.id === selected ? 'active' : '' } >
                    <div className = { 'tooltip' } >
                      <Link
                        className = { resp.icon }
                        to = { resp.path }>
                          <span className = { 'tooltiptext tooltip-right' } >
                            { resp.title }
                          </span>
                      </Link>
                    </div>

                    <div>
                    </div>
                    <div></div>
                </li>
              :
              resp.id === 5 &&
                <li key = { key }
                    className = { resp.id === selected ? 'active' : '' } >
                    <div className = { 'tooltip' } >
                      <Link
                        className = { resp.icon }
                        to = { resp.path }>
                          <span className = { 'tooltiptext tooltip-right' } >
                            { resp.title }
                          </span>
                      </Link>
                    </div>

                    <div>
                    </div>
                    <div></div>
                </li>
              )
              :
                groupId === 2 &&
                sidebarList.map((resp, key) =>
                <li key = { key }
                  className = { resp.id === selected ? 'active' : '' } >
                  <div className = { 'tooltip' } >
                    <Link
                    className = { resp.icon }
                    to = { resp.path }>
                      <span className = { 'tooltiptext tooltip-right' } >
                      { resp.title === 'Home' ? 'Dashboard' : resp.title }
                      </span>
                    </Link>
                  </div>
                  <div></div>
                  <div></div>
                </li>
              )
            }
        </ul>
      </div>
    )
  }
}

export default SideBar
