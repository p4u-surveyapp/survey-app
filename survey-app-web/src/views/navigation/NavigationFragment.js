import React from 'react';
import { Switch, Route } from 'react-router-dom'
import BaseMVPView from '../common/base/BaseMVPView'
import ConnectView from '../../utils/ConnectView'

import HomeFragment from './../home/HomeFragment'
import QuestionnaireManagementFragment from './../questionnaire-management/QuestionnaireManagementFragment'
import RegularComponent from './../questionnaire-management/components/regular/RegularComponent'
import OptionalComponent from './../questionnaire-management/components/optional/OptionalComponent'
import SettingsFragment from './../settings/SettingsFragment'
import AnsweredQuestionnaireFragment from './../user/AnsweredQuestionnaireFragment'

import Modal from '../../components/Modal/Modal'
import GenericButton from '../../components/Button/GenericButton'

/* Navigation Drawer Component*/
import SideBar from './components/sidebar/SideBar'
import Drawer from './components/drawer/Drawer'

import Presenter from './presenter/NavigationPresenter'

import './styles/drawerview.css'
class NavigationFragment extends BaseMVPView{
  constructor (props) {
    super(props)
    this.state = {
      userInfo: [],
      selected: 0,
      showSideBar: true,
      showError: false,
      errorMessage: '',
      groupId: 0
    }
    this.setSelectedNavigation = this.setSelectedNavigation.bind(this)
  }

  componentDidMount() {
    const token = window.location.search.split('=')
    this.presenter.getAuthenticateUser(token[1])

    if(this.state.selected === 0) {
      this.props.history.push('/home')
      this.setSelectedNavigation(1)
    }
  }

  setUserInfo(userInfo) {
    this.setState({ groupId: userInfo.groupId })
    this.setState({ userInfo })
  }

  setSelectedNavigation(selected) {
    this.setState({ selected })
  }

  showHideSidebar(value) {
    this.setState({ showSideBar: value ? false : true  })
  }

  logout() {
    this.presenter.logout()
  }
  logoutNav() {
    this.props.history.push('/')
  }

  showErrorModal(bool, message) {
    this.setState({
      errorMessage: message,
      showError: bool
    })
  }

  render () {
    const {
      userInfo,
      selected,
      showSideBar,
      showError,
      errorMessage,
      groupId
    } = this.state

    const {checkIsLogin} = this.props

    // const { history } = this.props
    const show = {
      style : { display: showSideBar ? 'block' : 'none' }
    }
    return (
      <div className = { 'navigation-body-div' }>
        { super.render() }
        {
          showError &&
          <Modal>
            <span>{ errorMessage }</span>
            <br/>
            <br/>
            <div className = { 'text-align-center' }>
              <GenericButton
              text = { 'OK' }
              onClick = { () => this.showErrorModal(false, '') }
              />
            </div>
          </Modal>
        }
        <div className = {'navigation-panels'}>
          <main className = { 'navigation-panel navigation-content' } >
            <Drawer className = { 'right-side' }>
              <Switch>
                <Route exact path = { '/home' }
                  render = {
                    props =>
                      <HomeFragment
                        { ...props }
                        groupId = { groupId }
                        setSelectedNavigation = { this.setSelectedNavigation }
                      />
                }/>
                <Route path = { '/questionnaire' }
                 render = {
                   props =>
                     <QuestionnaireManagementFragment
                       { ...props }
                       setSelectedNavigation = { this.setSelectedNavigation }
                      />
                 }/>
                 <Route path = { '/questionnaire/regular' }
                  render = {
                    props =>
                      <RegularComponent
                        { ...props }
                        setSelectedNavigation = { this.setSelectedNavigation }
                       />
                  }/>
                  <Route path = { '/questionnaire/optional' }
                   render = {
                     props =>
                       <OptionalComponent
                         { ...props }
                         setSelectedNavigation = { this.setSelectedNavigation }
                        />
                   }/>
                 <Route path = { '/settings' }
                  render = {
                    props =>
                      <SettingsFragment
                        { ...props }
                        setSelectedNavigation = { this.setSelectedNavigation }
                       />
                  }/>
                  <Route path = { '/user_questionnaire' }
                   render = {
                     props =>
                       <AnsweredQuestionnaireFragment
                         { ...props }
                         setSelectedNavigation = { this.setSelectedNavigation }
                        />
                   }/>
               </Switch>
            </Drawer>
          </main>
          <div
            className = { 'left-side' } style = { show.style }>
              <SideBar
                logout = { () => this.logout() }
                showSideBar = { showSideBar }
                selected = { selected }
                groupId = { groupId }
              />
          </div>
        </div>
      </div>
    )
  }
}
export default ConnectView(NavigationFragment, Presenter)
