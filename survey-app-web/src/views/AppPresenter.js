import CheckLoginInteractor from '../domain/interactor/login/CheckLoginInteractor'

export default class AppPresenter {
  constructor (container) {
    this.checkLoginInteractor = new CheckLoginInteractor(container.get('SurveyAppClient'))
  }

  setView (view) {
    this.view = view
  }

  checkLogin () {
    this.view.checkIsLogin(this.checkLoginInteractor.execute())
  }

}
