import { Observable } from 'rxjs'
import SessionProvider from '../provider/SessionProvider'
import Modal from '../../components/Modal/Modal'

import GenericError from '../../domain/exception/GenericError'
import ForbiddenError from '../../domain/exception/ForbiddenError'
import ServerError from '../../domain/exception/ServerError'

export default function ServiceErrorOperator () {
  return function ServiceErrorOperatorImpl (source) {
    return Observable.create(subscriber => {
      const subscription = source.subscribe(data => {
        const code = data && data.status
        const body = data && data.data

        if (code === 200) {
          subscriber.next(body)
        } else if (code === 400) {
          console.error(body.errors)
          subscriber.error(new GenericError(body))
        } else if (code === 401) {
          subscriber.error(new ForbiddenError())
        } else {
          subscriber.error(new ServerError('It seems that we\'ve encountered a problem. Error: 1'))
        }
      },
      err => subscriber.error(new ServerError('It seems that \'ve encountered a problem. Error: 2')),
      () => subscriber.complete())

      return subscription
   })
  }
}
