import ServiceErrorOperator from '../operator/ServiceErrorOperator'
import { Observable, interval } from 'rxjs'
import { mergeMap, map } from 'rxjs/operators'

export default class SurveyAppClient {
  constructor (service, sessionProvider, fileProvider) {
    this.service = service
    this.sessionProvider = sessionProvider
  }

/* User */
  setToken (token) {
    this.sessionProvider.setToken(token)
  }

  getToken () {
    return this.sessionProvider.getToken()
  }

  // HTTP status code
  authenticateUser (token) {
    return this.service.authenticateUser(token)
      .pipe(ServiceErrorOperator())
  }

  login (loginParam) {
    return this.service.login(loginParam)
      .pipe(ServiceErrorOperator())
  }

  logout (token) {
    return this.service.logout(token)
      .pipe(ServiceErrorOperator())
  }

  getUserInfo (token) {
    return this.service.getUserInfo(token)
      .pipe(ServiceErrorOperator())
  }

/* Answer Groups */
  getAnswerGroups (token, answerGroupsParam) {
    return this.service.getAnswerGroups(token, answerGroupsParam)
      .pipe(ServiceErrorOperator())
  }

  postAnswerGroup (token, postAnswerGroup) {
    return this.service.postAnswerGroup(token, postAnswerGroup)
      .pipe(ServiceErrorOperator())
  }

  getAnswerMethod (token) {
    return this.service.getAnswerMethod(token)
      .pipe(ServiceErrorOperator())
  }

  getAnswerGroupTemplate (token) {
    return this.service.getAnswerGroupTemplate(token)
      .pipe(ServiceErrorOperator())
  }

/* Answer Choices */
  getAnswerChoice (token, groupId) {
    return this.service.getAnswerChoice(token, groupId)
      .pipe(ServiceErrorOperator())
  }

  postAnswerChoice (token, postAnswerChoiceParam) {
    return this.service.postAnswerChoice(token, postAnswerChoiceParam)
      .pipe(ServiceErrorOperator())
  }

  putAnswerChoice (token, putAnswerChoiceParam) {
    return this.service.putAnswerChoice(token, putAnswerChoiceParam)
      .pipe(ServiceErrorOperator())
  }

/* Regular Questionnaires */
  postRegularQuestionnaire (token, postRegularQuestionnaireParam) {
    return this.service.postRegularQuestionnaire(token, postRegularQuestionnaireParam)
      .pipe(ServiceErrorOperator())
  }

  putRegularQuestionnaire (token, putRegularQuestionnaireParam) {
      return this.service.putRegularQuestionnaire(token, putRegularQuestionnaireParam)
        .pipe(ServiceErrorOperator())
  }

  getRegularQuestionnaire (token, regularQuestionnaireParam) {
    return this.service.getRegularQuestionnaire(token, regularQuestionnaireParam)
      .pipe(ServiceErrorOperator())
  }

  getOperationStatus (token) {
    return this.service.getOperationStatus(token)
      .pipe(ServiceErrorOperator())
  }

  getQuestionnaireSettings(token, getQuestionnaireSettingsParam) {
    return this.service.getQuestionnaireSettings(token, getQuestionnaireSettingsParam)
      .pipe(ServiceErrorOperator())
  }

  putRegularQuestionnaireSettings(token, putRegularQuestionnaireSettingsParam) {
    return this.service.putRegularQuestionnaireSettings(token, putRegularQuestionnaireSettingsParam)
      .pipe(ServiceErrorOperator())
  }

  /* Optional Questionnaires */
  getOptionalQuestionnaires(token, getOptionalQuestionnairesParam) {
    return this.service.getOptionalQuestionnaires(token, getOptionalQuestionnairesParam)
      .pipe(ServiceErrorOperator())
  }

  postOptionalQuestionnaire(token, postOptionalQuestionnaireParam) {
    return this.service.postOptionalQuestionnaire(token, postOptionalQuestionnaireParam)
      .pipe(ServiceErrorOperator())
  }

  getOptionalQuestionnaireById(token, id) {
    return this.service.getOptionalQuestionnaireById(token, id)
      .pipe(ServiceErrorOperator())
  }

  putOptionalQuestionnaire(token, putOptionalQuestionnaireParam) {
    return this.service.putOptionalQuestionnaire(token, putOptionalQuestionnaireParam)
    .pipe(ServiceErrorOperator())
  }

  /*User Answer*/
  postSubmitAnswer(token, postAnswerParam) {
    return this.service.postSubmitAnswer(token, postAnswerParam)
      .pipe(ServiceErrorOperator())
  }

  getYumeshinEmployee (token, employeeParam) {
    return this.service.getYumeshinEmployee(token, employeeParam)
      .pipe(ServiceErrorOperator())
  }

  /* User Answered Questionnaire */
  getPendingQuestionnaire(token) {
    return this.service.getPendingQuestionnaire(token)
      .pipe(ServiceErrorOperator())
  }

  getAnsweredQuestionnaire(token) {
    return this.service.getAnsweredQuestionnaire(token)
      .pipe(ServiceErrorOperator())
  }

  getQuestionnaireForm(token, getQuestionnaireParam) {
    return this.service.getQuestionnaireForm(token, getQuestionnaireParam)
      .pipe(ServiceErrorOperator())
  }

  /* Questionnaire Status */
  putActiveQuestionnaire(token, questionnaireId) {
    return this.service.putActiveQuestionnaire(token, questionnaireId)
      .pipe(ServiceErrorOperator())
  }

  putDeactiveQuestionnaire(token, questionnaireId) {
    return this.service.putDeactiveQuestionnaire(token, questionnaireId)
      .pipe(ServiceErrorOperator())
  }

  /* Question Type */
  getQuestionType(token) {
    return this.service.getQuestionType(token)
      .pipe(ServiceErrorOperator())
  }

}
