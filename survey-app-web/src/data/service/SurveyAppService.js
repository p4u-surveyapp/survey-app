export default class SurveyAppService {
  constructor (apiClient) {
    this.apiClient = apiClient
  }

  /* user */
  authenticateUser (token) {
    return this.apiClient.get(`/yumeshin-auth-user?token=${token}`)
  }

  login (loginParam) {
    return this.apiClient.post('/login', loginParam)
  }

  logout (token) {
    return this.apiClient.post('/logout', null,
    {
      headers: { token }
    })
  }

  getUserInfo (token) {
    return this.apiClient.get('/auth/user/get',
    {
      headers: { token }
    })
  }

  /* answer group */
  getAnswerGroups (token, answerGroups) {
    return this.apiClient.get(`/answer-groups/pages/get?lastId=${ answerGroups.pageNo }&pageSize=${ answerGroups.pageSize }`,
    {
      headers: { token }
    })
  }

  postAnswerGroup (token, postAnswerGroup) {
    return this.apiClient.post('/answer-group/create', {
      answerGroupApiRequest: postAnswerGroup.answerGroupApiRequest
    },
    {
      headers: { token }
    })
  }

  getAnswerMethod (token) {
    return this.apiClient.get('/answer-methods/get',
    {
      headers: { token }
    })
  }

  getAnswerGroupTemplate(token) {
    return this.apiClient.get('/answer-groups/get', {
      headers: { token }
    })
  }

  /* answer choice */
  getAnswerChoice (token, groupId) {
    return this.apiClient.get(`/answer-group/get?answerGroupId=${ groupId }`,
    {
      headers: { token }
    })
  }

  postAnswerChoice (token, postAnswerChoiceParam) {
    return this.apiClient.post('/answer/create', {
      answerApiRequest: postAnswerChoiceParam.answerApiRequest
    },
    {
      headers: { token }
    })
  }

  putAnswerChoice (token, putAnswerChoiceParam) {
    return this.apiClient.put(`/answer/update?answerId=${ putAnswerChoiceParam.answerId }`, {
      answerApiRequest: putAnswerChoiceParam.answerApiRequest
    },
    {
      headers: { token }
    })
  }

  /* regular questionnaire */
  postRegularQuestionnaire (token, postRegularQuestionnaireParam) {
    return this.apiClient.post(`/questionnaire/regular/create`, {
      emailSetting: postRegularQuestionnaireParam.emailSetting,
      questionHeaderSettings: postRegularQuestionnaireParam.questionHeaderSettings,
      regularQuestionnaire: postRegularQuestionnaireParam.regularQuestionnaire

    },
    {
      headers: { token }
    })
  }

  putRegularQuestionnaire (token, putRegularQuestionnaireParam) {
    return this.apiClient.put(`/questionnaire/regular/update`, {
      emailSetting: putRegularQuestionnaireParam.emailSetting,
      regularQuestionnaire: putRegularQuestionnaireParam.regularQuestionnaire
    },
    {
      headers: { token }
    })
  }

  getRegularQuestionnaire (token, regularQuestionnaire) {
    return this.apiClient.get(`/questionnaires/regular/pages/get?lastId= ${ regularQuestionnaire.pageNo }&pageSize=${ regularQuestionnaire.pageSize }`,
    {
      headers: { token }
    })
  }

  getOperationStatus (token) {
    return this.apiClient.get(`/operation-statuses/get`,
      {
        headers: { token }
      })
  }

  getQuestionnaireSettings (token, getQuestionnaireSettingsParam) {
    return this.apiClient.get(`/questionnaires/regular/get?questionnaireId=${ getQuestionnaireSettingsParam.questionnaireId }`,
    {
      headers: { token }
    })
  }

  putRegularQuestionnaireSettings (token, putRegularQuestionnaireSettings) {
    return this.apiClient.put(`/question-header-setting/update`,
    {
      isMoreThanTargetNoOfMonths: putRegularQuestionnaireSettings.isMoreThanTargetNoOfMonths,
      operationStatus: putRegularQuestionnaireSettings.operationStatusId,
      questionHeaderSettingId: putRegularQuestionnaireSettings.questionHeaderSettingsId,
      questionnaireId: putRegularQuestionnaireSettings.questionnaireId,
      targetNoOfMonths: putRegularQuestionnaireSettings.targetNoOfMonths
    },
    {
      headers: { token }
    })
  }

  /* Optional Questionnaires */
  getOptionalQuestionnaires(token, getOptionalQuestionnairesParam) {
    return this.apiClient.get(`/questionnaires/optional/pages/get?lastId=${ getOptionalQuestionnairesParam.pageNo }&pageSize=${ getOptionalQuestionnairesParam.pageSize }`,
    {
      headers: { token }
    })
  }

  postOptionalQuestionnaire(token, postOptionalQuestionnaireParam) {
    return this.apiClient.post('/questionnaire/optional/create',
    {
      emailSetting: postOptionalQuestionnaireParam.emailSetting,
      optionalQuestionnaire: postOptionalQuestionnaireParam.optionalQuestionnaire,
      questionHeaderSetting: postOptionalQuestionnaireParam.questionHeaderSetting
    },
    {
      headers: { token }
    })
  }

  getOptionalQuestionnaireById(token, id) {
    return this.apiClient.get(`/questionnaires/optional/get?questionnaireId=${ id }`,
      {
        headers: { token }
      })
  }

  putOptionalQuestionnaire(token, putOptionalQuestionnaireParam) {
    return this.apiClient.put('/questionnaire/optional/update', {
      emailSetting: putOptionalQuestionnaireParam.emailSetting,
      optionalQuestionnaire: putOptionalQuestionnaireParam.optionalQuestionnaire
    },
    {
      headers: { token }
    })
  }

  /* User Answer*/
  postSubmitAnswer(token, postAnswerParam) {
    return this.apiClient.post(`/questionnaire/recipient/answers/create`,
      postAnswerParam.recipientAnswersApiRequests,
      {
        headers: { token }
    })
  }

  /* Yumeshin Employee */
  getYumeshinEmployee(token, employeeParam) {
    return this.apiClient.post('/yumeshin-employees', {
      division: employeeParam.operationStatusId,
      lastId: 0,
      moreThanNoOfMonths: employeeParam.isMoreThanTargetNoOfMonths,
      targetNoOfMonths: employeeParam.targetNoOfMonths
    },
    {
      headers: { token }
    })
  }

  /* User Answered Questionnaire */
  getPendingQuestionnaire(token) {
    return this.apiClient.get('/recipient/unopened-questionnaires/pages/get',
    {
      headers: { token }
    })
  }

  getAnsweredQuestionnaire(token) {
    return this.apiClient.get('/recipient/answered-questionnaires/pages/get',
    {
      headers: { token }
    })
  }

  getQuestionnaireForm(token, getQuestionnaireParam) {
    return this.apiClient.get(`/recipient/get?recipientId=${getQuestionnaireParam.recipientId}&action=${getQuestionnaireParam.action}`,
    {
      headers: { token }
    })
  }

  /* Questionnaire Status */
  putActiveQuestionnaire(token, questionnaireId) {
    return this.apiClient.put('/questionnaire/activate/update', {
      questionnaireId
    },
    {
      headers: { token }
    })
  }

  putDeactiveQuestionnaire(token, questionnaireId) {
    return this.apiClient.put('/questionnaire/deactivate/update', {
      questionnaireId
    },
    {
      headers: { token }
    })
  }

  /* Question Type */
  getQuestionType(token) {
    return this.apiClient.get(`question-types/get`,
      {
        headers: { token }
      })
  }

}
