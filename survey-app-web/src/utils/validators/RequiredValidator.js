export default class RequiredValidator {
  isValid (s) {
    return !!s
  }
}
