export default class RequiredSymbolValidator {
  isValid (s) {
    const regex = /[\$*@!#%&()^~{}\[\]\;\'\,\.\/\<\>\?\`\-\+\_\=\\\:\"\|]+/
    return regex.test(String(s))
  }
}
