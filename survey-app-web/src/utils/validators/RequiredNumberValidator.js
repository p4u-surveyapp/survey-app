export default class RequiredNumberValidator {
  isValid (s) {
    const regex = /^[0-9]+$/
    return regex.test(String(s))
  }
}
