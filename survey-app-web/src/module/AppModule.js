import { Rxios } from 'rxios'
import { BASE_URL } from '../projectconfig'
import SurveyAppService from '../data/service/SurveyAppService'
import SurveyAppClient from '../data/service/SurveyAppClient'
import SessionProvider from '../data/provider/SessionProvider'

export default container => {

  container.singleton('SessionProvider', SessionProvider)

  container.singleton('ApiClient',
    new Rxios({
      baseURL: BASE_URL,
      headers: {},
    })
  )

  container.singleton('SurveyAppService', SurveyAppService, ['ApiClient'])
  container.singleton('SurveyAppClient', SurveyAppClient, ['SurveyAppService', 'SessionProvider'])

  return container
}
