import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './styles/dropdown.css'

class GenericDropdown extends Component {

  render () {

    const {
      text,
      style,
      value,
      onChange,
      disabled,
      errorMessage,
      options
    } = this.props

    return (
      <div
        style={ style }
        className={ 'dropdown-wrapper' }>
        <span className={ 'dropdown-text' }>{ text }</span>
        {
          <select
            className = { 'dropdown-select' }
            value = { value }
            onChange = { onChange }
            disabled = { disabled }>
            <option value = { '' }>Select an option</option>
            {
              options.map((option, id) =>
                <option
                  id = { option.id }
                  value = { option.name }>{ option.name }</option>
              )
            }
          </select>
        }
        {
          errorMessage ?
          <span className = { 'error-message' }>
            { errorMessage }
          </span>
          :
          <span className = { 'error-message-null' }></span>
        }
      </div>
    )
  }
}

GenericDropdown.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  onFocus: PropTypes.func,
  maxLength: PropTypes.number,
  readOnly: PropTypes.bool,
  onChange: PropTypes.func,
  text: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]),
  hint: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  disabled: PropTypes.bool,
  errorMessage: PropTypes.string
}

export default GenericDropdown
