import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './styles/style.css'

import Modal from '../Modal/Modal'

class GenericLoader extends Component {
  render () {
    const {
      show,
      width,
      height,
      message
    } = this.props

    return (
      <div>
        {
          show &&
          <div className = { 'generic-loader' } >
            <div  className = { 'margin-bottom-8px' } >
              <label>
                { message }
              </label>
            </div>
            <img
              width = { width ? width : 50 }
              height = { height ? height : 15}
              src= { require('../../images/ajax-loader.gif')  }/>
          </div>
        }
      </div>
    )
  }
}

GenericLoader.propTypes = {
  width : PropTypes.number,
  message : PropTypes.string,
  show : PropTypes.bool,
  validateLoading : PropTypes.bool,
}

GenericLoader.defaultProps = {
  width : 0,
  show : false,
  validateLoading : false,
  message : '',
}
export default GenericLoader
