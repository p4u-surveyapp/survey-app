import React, { Component } from 'react'
import './styles/styles.css'
import GenericPagination from '../Pagination/GenericPagination'
import GenericButton from '../Button/GenericButton'
import GenericInput from '../TextBox/GenericInput'

class Table extends Component {
   constructor(props) {
      super(props)
      this.state = {
           exampleItems: this.props.array,
           pageOfItems: this.props.array,
           isEdit: 0
      }
      this.onChangePage = this.onChangePage.bind(this)
      this.isEditMode = this.isEditMode.bind(this)
      this.populateTableData = this.populateTableData.bind(this)
      this.renderTableHeader = this.renderTableHeader.bind(this)
      // this.addTableData = this.addTableData.bind(this)
      this.renderTableData = this.renderTableData.bind(this)
   }

   onChangePage(pageOfItems) {
       this.setState({ pageOfItems: pageOfItems })
   }

   isEditMode(id) {
     this.setState({ isEdit: id })
   }


   populateTableData(array, columns) {
     let tempArray = []
     let tempArray2 = {}
       array.map((element, index) => {
           tempArray2 = {...tempArray2}
           columns.map((value, key) => {
           let column = element[columns[key]]
           tempArray2[columns[key]] = column

       })
       tempArray.push(tempArray2)
     })
     return tempArray
   }

   renderTableHeader(array) {
     let header = []
     if(array.length != 0) {
       header = Object.keys(array[0])
       return header.map((key, index) => {
          return <th key={index}>{(key.replace(/([a-z])([A-Z])/g, '$1 $2')).toUpperCase()}</th>
       })
     }

  }

  // addTableData(array) {
  //   const { isAddMode } = this.props
  //   let header = Object.keys(array[0])
  //   return <tr>
  //   {
  //     header.map((key, index) =>
  //         <td><GenericInput/></td>
  //     )
  //   }
  //     <td className = { 'text-align-center' }>
  //       <span className = { 'action-button action-save' } onClick = { () => { isAddMode(false) } }/>
  //       <span className = { 'action-button action-cancel' } onClick = { () => { isAddMode(false) } }/>
  //     </td>
  //   </tr>
  //  }

   renderTableData(columns, isEdit) {
     const {
       editButton,
       action,
       onClick,
       // isAdd,
       array
    } = this.props
    let column = []
    if(columns.length !=0) {
      return array.map((value, key) => {
        column = Object.values(columns[key])
         return (
           <tr key = {key}>
             {
               column.map((data, id) =>

                   // action &&
                   // // isEdit === value.id ?
                   // //   <td key = { id } >
                   // //     <GenericInput
                   // //      value = { data }
                   // //     />
                   // //   </td>
                   // // :
                     <td key = { id } onClick = { () => onClick(value) } >
                       { data }
                     </td>
               )
             }
             {
               // action &&
               // isEdit === value.id ?
               // <td key = { `edit-${key}` } className = { 'text-align-center' }>
               //   <span className = { 'action-button action-save' } onClick = { () => { this.isEditMode(0) } }/>
               //   <span className = { 'action-button action-cancel' } onClick = { () => { this.isEditMode(0) } }/>
               // </td>
               // :
                 action &&
                 <td key = { `edit-${key}` } className = { 'text-align-center' }>
                   <span className = { 'action-button action-edit' } onClick = { () => editButton(value) }/>
                 </td>
             }
           </tr>
         )
      })
    }
  }

  render() {
    const {
      pageOfItems,
      exampleItems,
      isEdit
    } = this.state

    const {
      editButton,
      array,
      columns,
      action,
      onClick,
      // isAdd
    } = this.props

    return (
      <div >
      {
        array &&
        array.length != 0 ?
        <table className = { 'table' }>
            <tbody>
              <tr>{ this.renderTableHeader(this.populateTableData(pageOfItems, columns)) }
                {
                  action &&
                  <th className = { 'action-header' }>ACTION</th>
                  // :
                  //   isAdd &&
                  //   <th className = { 'action-header' }>ACTION</th>
                }
              </tr>
               { //isAdd && this.addTableData(this.populateTableData(pageOfItems, columns))
               }
              { this.renderTableData(this.populateTableData(pageOfItems, columns), isEdit) }
            </tbody>
        </table>
        :
        <table className = { 'table' }>
          <tbody>
            <tr><th>Table</th></tr>
            <tr><td>No record(s)</td></tr>
          </tbody>
        </table>
      }
        <GenericPagination items={exampleItems} onChangePage={this.onChangePage} />
      </div>
    )
  }
}
export default Table
