export default (
  answerGroupCode,
  answerGroupDesc,
  answerGroupMethodId) => ({
    answerGroupApiRequest : {
      answerMethodId: answerGroupMethodId,
      groupCode: answerGroupCode,
      groupDesc: answerGroupDesc
  }
})
