export default (
  title,
  completeMessage,
  reminderRemainingDays,
  isMonthlyRecurring,
  isActive,
  startDay,
  endDay,
  content,
  sendBy,
  subject,
  emailTitle,
  targetNoOfMonths,
  isMoreThanTargetNoOfMonths,
  operationStatus,
  questionsArray,
  chunkedArray
) => ({
  emailSetting : {
    content: content,
    sender: sendBy,
    subject: subject,
    title: emailTitle
  },
  questionHeaderSettings : [{
    questionHeaderSetting: {
      isMoreThanTargetNoOfMonths: isMoreThanTargetNoOfMonths ? 1 : 0,
      operationStatus: operationStatus,
      questions:  questionsArray,
      recipients: chunkedArray,
      targetNoOfMonths: targetNoOfMonths,
    }
  }],
  regularQuestionnaire: {
    completeMessage: completeMessage,
    endDay: endDay,
    isMonthlyRecurring: isMonthlyRecurring,
    reminderRemainingDay: reminderRemainingDays,
    startDay: startDay,
    topMessage: title
  }
})
