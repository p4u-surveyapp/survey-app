export default (
  questionnaireId,
  title,
  completeMessage,
  reminderRemainingDays,
  isMonthlyRecurring,
  activeStatus,
  startDay,
  endDay,
  content,
  sendBy,
  subject,
  emailTitle
) => ({
  emailSetting : {
    content: content,
    sender: sendBy,
    subject: subject,
    title: emailTitle
  },
  regularQuestionnaire: {
    completeMessage: completeMessage,
    endDay: endDay,
    isMonthlyRecurring: isMonthlyRecurring,
    questionnaireId : questionnaireId,
    reminderRemainingDay: reminderRemainingDays,
    startDay: startDay,
    topMessage: title
  }
})
