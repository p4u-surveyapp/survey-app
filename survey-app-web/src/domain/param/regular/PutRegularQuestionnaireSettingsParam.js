export default (
  operationStatusId,
  questionHeaderSettingsId,
  questionnaireId,
  targetNoOfMonths
) => ({
  isMoreThanTargetNoOfMonths: targetNoOfMonths > 12 ? 1 : 0,
  operationStatusId,
  questionHeaderSettingsId,
  questionnaireId,
  targetNoOfMonths
})
