export default (
  answer,
  rating,
  imgUrl,
  answerGroupId) => ({
    answerApiRequest : {
      answer: answer,
      rating: rating,
      imgUrl: imgUrl,
      answerGroupId: answerGroupId
  }
})
