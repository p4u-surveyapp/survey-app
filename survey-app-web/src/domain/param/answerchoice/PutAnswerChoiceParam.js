export default (
  answer,
  rating,
  imgUrl,
  answerId,
  answerGroupId) => ({
    answerId: answerId,
    answerApiRequest : {
      answer: answer,
      rating: rating,
      imgUrl: imgUrl,
      answerGroupId: answerGroupId
  }
})
