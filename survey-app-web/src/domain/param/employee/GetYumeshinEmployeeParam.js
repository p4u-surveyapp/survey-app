export default (
  operationStatusId,
  targetNoOfMonths
) => ({
  operationStatusId,
  targetNoOfMonths,
  isMoreThanTargetNoOfMonths: targetNoOfMonths > 12 ? 1 : 0,
})
