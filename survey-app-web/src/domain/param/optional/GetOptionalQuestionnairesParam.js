export default (
  pageNo,
  pageSize
) => ({
  pageNo,
  pageSize
})
