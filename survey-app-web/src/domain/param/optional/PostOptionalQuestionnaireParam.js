export default (
  content,
  sendBy,
  subject,
  emailTitle,
  activeStatus,
  endDate,
  reminderRemainingDays,
  startDate,
  targetPersonValue,
  title,
  completeMessage,
  questionsArray,
  selectedRecipient
) => ({
  emailSetting : {
    content,
    sender: sendBy,
    subject,
    title: emailTitle
  },
  optionalQuestionnaire: {
    // activeStatus: activeStatus,
    completeMessage: completeMessage,
    endDate,
    questionnaireId: 0,
    reminderRemainingDay: reminderRemainingDays,
    startDate,
    targetOption: targetPersonValue,
    topMessage: title
  },
  questionHeaderSetting : {
      questions: questionsArray,
      recipients: selectedRecipient
  }
})
