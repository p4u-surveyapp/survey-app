export default (
  title,
  reminderRemainingDays,
  activeStatus,
  startDate,
  endDate,
  urlLink,
  emailTitle,
  sendBy,
  subject,
  content,
  questionnaireId,
  targetPersonValue,
  completeMessage
) => ({
  questionnaireId: questionnaireId,
  emailSetting: {
    content,
    sender: sendBy,
    subject,
    title: emailTitle
  },
  optionalQuestionnaire: {
    completeMessage,
    endDate,
    questionnaireId,
    reminderRemainingDay: reminderRemainingDays,
    startDate,
    targetOption: targetPersonValue,
    topMessage: title,
  }
})
