export default (
  questionnaireId,
  recipientId,
  answerArr
) => ({
  questionnaireId: questionnaireId,
  recipientId: recipientId,
  recipientAnswersApiRequests: answerArr
})
