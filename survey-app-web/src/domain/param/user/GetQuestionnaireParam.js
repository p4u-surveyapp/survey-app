export default (
  recipientId,
  action
) => ({
  recipientId: recipientId,
  action: action
})
