export default class GetYumeshinEmployeeInteractor {
  constructor (client) {
    this.client = client
  }

  execute (employeeParam) {
    return this.client.getYumeshinEmployee(this.client.getToken(), employeeParam)
  }
}
