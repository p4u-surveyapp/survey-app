export default class PostOptionalQuestionnaireInteractor {
  constructor (client) {
    this.client = client
  }

  execute (postOptionalQuestionnaireParam) {
    return this.client.postOptionalQuestionnaire(this.client.getToken(), postOptionalQuestionnaireParam)
  }
}
