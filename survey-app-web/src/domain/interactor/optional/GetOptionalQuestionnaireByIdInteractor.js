export default class GetOptionalQuestionnaireByIdInteractor {
  constructor (client) {
    this.client = client
  }

  execute (id) {
    return this.client.getOptionalQuestionnaireById(this.client.getToken(), id)
  }
}
