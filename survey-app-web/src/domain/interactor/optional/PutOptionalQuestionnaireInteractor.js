export default class PutOptionalQuestionnaireInteractor {
  constructor (client) {
    this.client = client
  }

  execute (putOptionalQuestionnaireParam) {
    return this.client.putOptionalQuestionnaire(this.client.getToken(), putOptionalQuestionnaireParam)
  }
}
