export default class GetOptionalQuestionnairesInteractor {
  constructor (client) {
    this.client = client
  }

  execute (getOptionalQuestionnairesParam) {
    return this.client.getOptionalQuestionnaires(this.client.getToken(), getOptionalQuestionnairesParam)
  }
}
