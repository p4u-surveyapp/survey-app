export default class PutActiveQuestionnaireInteractor {
  constructor (client) {
    this.client = client
  }

  execute (questionnaireId) {
    return this.client.putActiveQuestionnaire(this.client.getToken(), questionnaireId)
  }
}
