export default class PutDeactiveQuestionnaireInteractor {
  constructor (client) {
    this.client = client
  }

  execute (questionnaireId) {
    return this.client.putDeactiveQuestionnaire(this.client.getToken(), questionnaireId)
  }
}
