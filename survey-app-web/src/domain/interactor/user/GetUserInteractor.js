export default class GetUserInteractor {
  constructor (client) {
    this.client = client
  }

  execute () {
    return this.client.getUserInfo(this.client.getToken())
  }
}
