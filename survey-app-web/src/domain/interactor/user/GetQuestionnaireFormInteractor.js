export default class GetQuestionnaireFormInteractor {
  constructor (client) {
    this.client = client
  }

  execute (getQuestionnaireParam) {
    return this.client.getQuestionnaireForm(this.client.getToken(), getQuestionnaireParam)
  }
}
