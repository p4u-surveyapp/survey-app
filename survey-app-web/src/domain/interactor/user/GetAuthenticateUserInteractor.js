export default class GetAuthenticateUserInteractor {
  constructor (client) {
    this.client = client
  }

  execute (token) {
    return this.client.authenticateUser(token)
    .do(resp =>
      resp &&
      this.client.setToken(resp.data)
    )
  }
}
