export default class GetAnsweredQuestionnaireInteractor {
  constructor (client) {
    this.client = client
  }

  execute () {
    return this.client.getAnsweredQuestionnaire(this.client.getToken())
  }
}
