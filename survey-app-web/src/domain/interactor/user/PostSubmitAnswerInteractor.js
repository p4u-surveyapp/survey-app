export default class PostSubmitAnswerInteractor {
  constructor (client) {
    this.client = client
  }

  execute (postAnswerParam) {
    return this.client.postSubmitAnswer(this.client.getToken(), postAnswerParam)
  }
}
