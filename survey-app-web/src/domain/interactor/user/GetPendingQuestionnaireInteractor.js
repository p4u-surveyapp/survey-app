export default class GetPendingQuestionnaireInteractor {
  constructor (client) {
    this.client = client
  }

  execute () {
    return this.client.getPendingQuestionnaire(this.client.getToken())
  }
}
