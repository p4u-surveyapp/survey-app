export default class GetAnswerChoiceInteractor {
  constructor (client) {
    this.client = client
  }

  execute (groupId) {
    return this.client.getAnswerChoice(this.client.getToken(), groupId)
  }
}
