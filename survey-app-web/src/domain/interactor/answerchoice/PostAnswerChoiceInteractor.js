export default class PostAnswerChoiceInteractor {
  constructor (client) {
    this.client = client
  }

  execute (postAnswerChoiceParam) {
    return this.client.postAnswerChoice(this.client.getToken(), postAnswerChoiceParam)
  }
}
