export default class PutAnswerChoiceInteractor {
  constructor (client) {
    this.client = client
  }

  execute (postAnswerChoiceParam) {
    return this.client.putAnswerChoice(this.client.getToken(), postAnswerChoiceParam)
  }
}
