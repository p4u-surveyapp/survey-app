export default class GetRegularQuestionnaireInteractor {
  constructor (client) {
    this.client = client
  }

  execute (GetRegularQuestionnaireParam) {
    return this.client.getRegularQuestionnaire(this.client.getToken(), GetRegularQuestionnaireParam)
  }
}
