export default class GetOperationStatusInteractor {
  constructor (client) {
    this.client = client
  }

  execute () {
    return this.client.getOperationStatus(this.client.getToken())
  }
}
