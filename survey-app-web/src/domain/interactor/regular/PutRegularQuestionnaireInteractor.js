export default class PutRegularQuestionnaireInteractor {
  constructor (client) {
    this.client = client
  }

  execute (putRegularQuestionnaireParam) {
    return this.client.putRegularQuestionnaire(this.client.getToken(), putRegularQuestionnaireParam)
  }
}
