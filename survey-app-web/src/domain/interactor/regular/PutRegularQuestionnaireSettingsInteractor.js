export default class PutRegularQuestionnaireSettingsInteractor {
  constructor (client) {
    this.client = client
  }

  execute(putRegularQuestionnaireSettingsParam) {
    return this.client.putRegularQuestionnaireSettings(this.client.getToken(), putRegularQuestionnaireSettingsParam)
  }
}
