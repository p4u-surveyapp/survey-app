export default class GetQuestionnaireSettingsInteractor {
  constructor (client) {
    this.client = client
  }

  execute (GetQuestionnaireSettingsParam) {
    return this.client.getQuestionnaireSettings(this.client.getToken(), GetQuestionnaireSettingsParam)
  }
}
