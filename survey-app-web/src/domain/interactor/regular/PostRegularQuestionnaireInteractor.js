import { Observable } from 'rxjs'

export default class PostRegularQuestionnaireInteractor {
  constructor (client) {
    this.client = client
  }

  execute (PostRegularQuestionnaireParam) {
    return this.client.postRegularQuestionnaire(this.client.getToken(), PostRegularQuestionnaireParam)
  }
}
