export default class CheckLoginInteractor {
  constructor (client) {
    this.client = client
  }

  execute () {
    return this.client.getToken()
  }
}
