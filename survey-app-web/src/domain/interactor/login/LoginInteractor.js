export default class LoginInteractor {
  constructor (client) {
    this.client = client
  }

  execute (loginParam) {
    return this.client.login(loginParam)
    .do(resp =>
      resp &&
      resp.data.map((token, id) => 
        this.client.setToken(token)
      )
    )
  }
}
