export default class LogoutInteractor {
  constructor (client) {
    this.client = client
  }

  execute () {
    return this.client.logout(this.client.getToken())
      .do(this.client.setToken(''))
  }
}
