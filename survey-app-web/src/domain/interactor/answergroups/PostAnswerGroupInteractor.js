export default class PostAnswerGroupInteractor {
  constructor (client) {
    this.client = client
  }

  execute (postAnswerGroupParam) {
    return this.client.postAnswerGroup(this.client.getToken(), postAnswerGroupParam)
  }
}
