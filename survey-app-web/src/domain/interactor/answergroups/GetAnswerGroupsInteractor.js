export default class GetAnswerGroupsInteractor {
  constructor (client) {
    this.client = client
  }

  execute (answerGroupsParam) {
    return this.client.getAnswerGroups(this.client.getToken(), answerGroupsParam)
  }
}
