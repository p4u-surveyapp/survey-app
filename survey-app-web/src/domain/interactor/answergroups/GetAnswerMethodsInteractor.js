export default class GetAnswerMethodsInteractor {
  constructor (client) {
    this.client = client
  }

  execute () {
    return this.client.getAnswerMethod(this.client.getToken())
  }
}
