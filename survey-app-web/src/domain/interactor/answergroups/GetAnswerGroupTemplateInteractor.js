export default class GetAnswerGroupTemplateInteractor {
  constructor (client) {
    this.client = client
  }

  execute() {
    return this.client.getAnswerGroupTemplate(this.client.getToken())
  }
}
