export default class GetQuestionTypeInteractor {
  constructor (client) {
    this.client = client
  }

  execute () {
    return this.client.getQuestionType(this.client.getToken())
  }
}
